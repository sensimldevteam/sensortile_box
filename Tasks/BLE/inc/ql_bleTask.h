/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : ql_bleTask.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef __QL_BLETASK_H_
#define __QL_BLETASK_H_

#if !defined( _EnD_Of_Fw_global_config_h )
#error "Include Fw_global_config.h first"
#endif


#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "common.h"
#include "test_types.h"

#define BLE_MSGQ_WAIT_TIME	portMAX_DELAY

#define BLE_QUEUE_LENGTH  10  //msg queue size for bleTaskHandler

#define BLE_CMD_START	   11
#define BLE_CMD_TEST	   12
#define BLE_CMD_DATA_RCVD  13
#define BLE_CMD_DATA_TXMIT 14


#define SEND_BLE_IMMEDIATE     0 //type = 0
#define SEND_BLE_LARGE_BUFFER  1 //type = 1

extern signed portBASE_TYPE StartRtosTaskBLE( void);
struct xQ_Packet;
extern UINT32_t addPktToQueueFromISR_BLE(struct xQ_Packet *pxMsg);
extern int CheckBLETaskReady(void);
extern int SendToBLE( int cmd_type, int len, const uint8_t *pData );
extern void setBLERxCallback(void (*RxCallbackFunc)(uint8_t *buffer));

extern void GenerateInterruptToBLE(void);

#endif  /* __QL_BLETASK_H_ */
