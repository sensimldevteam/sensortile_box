/*==========================================================
 *
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : process_ids.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef __PROCESS_IDS__
#define __PROCESS_IDS__

typedef enum en_process_id {
  AUDIO_ISR_PID,
  AUDIO_NUANCE_VR_PID,
  AUDIO_SENSORY_VR_PID,
  AUDIO_SENSIML_AI_PID,
  IMU_ISR_PID,
  IMU_SENSIML_AI_PID,
} process_id_t ;

#endif /* __PROCESS_IDS__ */
