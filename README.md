# SensiML SensorTile.Box Project

This repository contains source code used to set up data collection and recognition in SensiML Software, using the LSM6DSOX IMU and MP23ABS1 microphone as data sources.

It also contains the ST MicroElectronics SensorTile.Box Function Pack necessary for building the full applications.

## Tools used

The instructions for this guide were tested with the following:

| Software               | Version | Link                                                                                                                                      |
|------------------------|---------|-------------------------------------------------------------------------------------------------------------------------------------------|
| IAR Embedded Workbench            | 8.32.1.18631             | [IAR Home](https://www.iar.com/)                                                                                                          |
| GNU Make For Windows              | 3.81                     | [SourceForge](https://sourceforge.net/projects/gnuwin32/files/make/3.81)                                                                  |
| Arm GCC Toolchain (arm-none-eabi) | 8.2.1 (8-2018-q4-major)  | [ARM Developer Center](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads/)                                             |
| SensorTile.Box Function Pack      | 1.1.0                    | [SensorTile.Box Function Pack](https://www.st.com/content/st_com/en/products/embedded-software/mcu-mpu-embedded-software/stm32-embedded-software/stm32-ode-function-pack-sw/fp-sns-stbox1.html) |


## Linking to the SensorTile.Box Function Pack

The SensorTile.Box Function pack is already linked for both IAR and GCC builds in this repository. The folder containing the function pack is `STM32_STBOX_V1.1.0`, and contains the libraries provided from the STMicro site.

## Building For data collection

1. Open the file `ST-Apps/sensortilebox_ai_mqttsn_app/inc/Fw_global_cfg.h`
2. Search for `#define S3AI_FIRMWARE_MODE      S3AI_FIRMWARE_MODE_COLLECTION` (Line 38)
3. Ensure this is the only mode defined in the file.
4. Build with your chosen build system.

## Building A Knowledge Pack

The slide demo is included as a Knowledge Pack for this repository.

To replace a Knowledge Pack, you can replace the `ST-APPS/sensortilebox_ai_mqttsn_app/knowledgepack` folder. Further instructions are on the [Building a Knowledge Pack Library](https://sensiml.com/documentation/knowledge-packs/building-a-knowledge-pack-library.html) section of the SensiML documentation page.

# Known Issues

- LSM6DS0X Data collection at 6.667kHz can cause intermittent freezes when live streaming to Data Capture Lab
- Transfer of files over MQTT-SN connection is currently ~ 1:1 on collection time for higher frequencies. (30 minutes  collected = 30 more minutes to transfer)

