#include "Fw_global_config.h"

#include "cli.h"
#include "FreeRTOS.h"
#include "task.h"
#include <eoss3_hal_uart.h>
#include "RtosTask.h"

/* These are PLATFORM specific functions that the CLI code requires */

struct cli CLI_common;
extern const char *SOFTWARE_VERSION_STR;

xTaskHandle xHandleTestCli;

uint32_t CLI_time_now(void)
{
  return xTaskGetTickCount();
}

intptr_t CLI_timeout_start(void)
{
  return (intptr_t)CLI_time_now();
}

void CLI_beep(void)
{
  /* ascii bell */
  CLI_putc( 0x07 );
}

int CLI_timeout_expired( intptr_t token, int timeout )
{
  int32_t delta;
  if( timeout == 0 ){
    return 1;
  }
  if( timeout < 0 ){
    /* forever */
    return 0;
  }
  uint32_t now;
  
  now = CLI_time_now();
  
  delta = ((int32_t)(now))- (int32_t)(token);
  if( delta > timeout ){
    return 1;
  } else {
    return 0;
  }
}

void CLI_putc_raw(int c)
{
  uart_tx( UART_ID_CONSOLE, c);
}

int CLI_getkey_raw( int timeout )
{
  intptr_t tstart;
  
  tstart = CLI_timeout_start();
  for(;;){
    uart_rx_wait( UART_ID_CONSOLE, timeout );
    if( uart_rx_available( UART_ID_CONSOLE ) ){
      return uart_rx( UART_ID_CONSOLE  );
    }
    /* no key */
    if( CLI_timeout_expired(tstart, timeout ) ){
      return EOF;
    }
    vTaskDelay( 10 );
  }
}


void CLI_task( void *pParameter )
{
    (void)(pParameter);
    int k;
    
    wait_ffe_fpga_load();
    /* set to 1 to have a timestamp on the side */
    vTaskDelay(100);
    
    CLI_common.timestamps = 0;
    
    CLI_printf("#*******************\n");
    CLI_printf("S3-AI-CLI-Start\n");
    CLI_printf("SW Version: %s\n", SOFTWARE_VERSION_STR );
    CLI_printf("#*******************\n");
    CLI_print_prompt();
    for(;;){
        k = CLI_getkey( 10*1000 );
        if( k == EOF ){
            continue;
        }
        CLI_rx_byte( k );
    }
}



void CLI_start_task(const struct cli_cmd_entry *pMainMenu)
{
    xTaskCreate ( CLI_task, "CLI", 4 * CLI_TASK_STACKSIZE, NULL, (UBaseType_t)(PRIORITY_NORMAL), &xHandleTestCli);
    CLI_init( pMainMenu );
    configASSERT( xHandleTestCli );
}
void CLI_sleep( int nmsecs )
{
    vTaskDelay( nmsecs );
}
