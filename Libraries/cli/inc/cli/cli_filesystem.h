#if !defined( _IN_CLI_H_ )
#error "Please include cli.h instead"
#endif

/* a submenu that supports various SD CARD actions */
extern const struct cli_cmd_entry cli_file_commands[];
