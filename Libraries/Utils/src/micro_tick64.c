/** @file micro_tick64.c */
/*==========================================================
 *
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : micro_tick64.c
 *    Purpose: 
 *                                                          
 *=========================================================*/

#include "FreeRTOS.h"
#include "task.h"

static volatile uint64_t   xRtcTickCount64_usec       = 0;
static volatile uint64_t   xRtcTickCount64_msec_start = 0;

void xTaskSet_uSecCount(uint64_t new_val)
{
    portENTER_CRITICAL();
    xRtcTickCount64_msec_start = xTaskGetTickCount();
    xRtcTickCount64_usec = new_val;
    portEXIT_CRITICAL();
}

uint64_t xTaskGet_uSecCount(void)
{
    uint64_t v;
    portENTER_CRITICAL();
    v = (xTaskGetTickCount() - xRtcTickCount64_msec_start ) * 1000;
    portEXIT_CRITICAL();
    return (v + xRtcTickCount64_usec);
}

uint64_t convert_to_uSecCount(uint32_t tickCount)
{
    uint64_t v;
    v = (tickCount - xRtcTickCount64_msec_start ) * 1000;
    return (v + xRtcTickCount64_usec);
}
