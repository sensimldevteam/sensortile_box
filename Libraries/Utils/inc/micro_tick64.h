/** @file micro_tick64.h */
/*==========================================================
 *
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : micro_tick64.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef _MICRO_TICK64_H_

#define _MICRO_TICK64_H_

/** @brief set tick count to the given value
 *
 *  @param[in] new_val new value for the tick count in micro-seconds
 *
 */
extern void xTaskSet_uSecCount(uint64_t new_val);

/** @brief return the current tick count in micro-seconds
 * 
 * @return return current tick count in micro-seconds
 */
extern uint64_t xTaskGet_uSecCount(void);


/** @brief return given tick count in micro-seconds
 * 
 * @return return given tick count in micro-seconds
 */
extern uint64_t convert_to_uSecCount(uint32_t tickCount);

#endif /* _MICRO_TICK64_H_ */