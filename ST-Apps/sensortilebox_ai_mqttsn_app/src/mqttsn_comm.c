/*==========================================================
 *
 *-  Copyright Notice  -------------------------------------
 *
 *    Licensed Materials - Property of QuickLogic Corp.
 *    Copyright (C) 2019 QuickLogic Corporation
 *    All rights reserved
 *    Use, duplication, or disclosure restricted
 *
 *    File   : mqttsn_comm.c
 *    Purpose:
 *
 *=========================================================*/

#include "Fw_global_config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if (MQTTSN_OVER_UART == 1)
#include "mqttsn_uart.h"

// These are wrapper API functions from MQTTSN_SML that have dependency on Hardware
void mqttsn_comm_setup(void)
{
  mqttsn_uart_setup();
  return;
}
void mqttsn_comm_tx(uint8_t *pBuf, uint32_t bufLen)
{
  mqttsn_uart_tx(pBuf, bufLen);
  return;
}
int mqttsn_comm_rx_available(void)
{
  return mqttsn_uart_rx_available();
}

int mqttsn_comm_rx(uint8_t *pBuf, int n)
{
  return mqttsn_uart_rx(pBuf, n);
}

#endif

#if (MQTTSN_OVER_BLE == 1)
#include "ql_bleTask.h"
#include "dbg_uart.h"

/* send this packet to the BLE */
void my_ble_send( int cmd, int len, const void *data )
{

    uint8_t databuf[20];
    int send_len = len;
    if( len > 20){
        send_len = 20;
    }
    databuf[0] = cmd;
    memcpy( (void *)(&databuf[1]), (void *)(data), send_len );
    SendToBLE( SEND_BLE_IMMEDIATE, send_len+1, databuf );
}

#endif
