/**@file
 *
 * @defgroup ble_pme SensiML PME Service
 * @{
 * @ingroup  ble_pme
 * @brief    SensiML PME Service implementation.
 *
 * @details The SensiML PME Service is a simple GATT-based service with TX and RX characteristics.
 *          Data received from the peer is passed to the application, and the data received
 *          from the application of this service is sent to the peer as Handle Value
 *          Notifications. This module demonstrates how to implement a custom GATT-based
 *          service and characteristics using the S110 SoftDevice. The service
 *          is used by the application to send and receive ASCII text strings to and from the
 *          peer.
 *
 * @note The application must propagate S110 SoftDevice events to the SensiML PME Service module
 *       by calling the ble_pme_on_ble_evt() function from the ble_stack_handler callback.
 */
#include "sensor_service.h"
#include "bluenrg1_gatt_aci.h"
#include "ble_pme.h"
#include "ble_iop_messages.h"

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

#define BLE_UUID_PME_RESULTS_CHAR 0x1101 /**< The UUID of the PME Results Characteristic. */
const char * pme_desc = "SensiML PME Service";

#if SML_ADD_FEATURE_VECTOR
#define BLE_UUID_FEAT_VEC_RESULTS_CHAR 0x1102
#endif

#if SML_ADD_SEGMENT_DATA
#define BLE_UUID_SEGMENT_DATA_CHAR 0x1103
#endif

#define BLE_UUID_MODEL_DESCRIPTOR 0x1104

#define PME_BASE_UUID                  {{0x59, 0x31, 0x72, 0x6f, 0xF2, 0x7a, 0xf7, 0x90, \
                                        0xdd, 0x46, 0x22, 0x5a, 0x00, 0x00, 0x42, 0x42}}

extern bool ble_srv_is_notification_enabled(uint8_t const * p_encoded_data);

/**@brief Function for handling the @ref BLE_GATTS_EVT_WRITE event from the S132 SoftDevice.
 *
 * @param[in] p_pme     PME Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
void ble_pme_on_write(ble_pme_t * p_pme,  uint16_t attr_handle, uint8_t data_length, uint8_t *p_data) // uint8_t * evt_gatt_attr_modified_IDB05A1_t * p_ble_evt)
{
    if ( ((attr_handle == p_pme->pme_result_handle + 2) && (data_length == 2))
         #if SML_ADD_FEATURE_VECTOR
         || ((attr_handle == p_pme->feat_vector_handle + 2) && (data_length == 2) )
         #endif
         )
    {
        bool notif_enabled;

        notif_enabled = ble_srv_is_notification_enabled(p_data);
		p_pme->pme_notif_type = RECO_CLASS_ONLY;
		#if SML_ADD_FEATURE_VECTOR
		if(attr_handle == p_pme->feat_vector_handle + 2)
		{
			p_pme->pme_notif_type = RECO_CLASS_FV;
		}
		#endif
        if (p_pme->is_pme_notif_enabled != notif_enabled)
        {
            p_pme->is_pme_notif_enabled = notif_enabled;

            if (p_pme->evt_handler != NULL)
            {
                p_pme->evt_handler(p_pme, BLE_PME_EVT_NOTIF_RESULTS, p_data, data_length);
            }
        }
    }

    else
    {
        // Do Nothing. This event is not relevant for this service.
    }
}


void ble_pme_evt_handler(ble_pme_t *p_pme, ble_pme_evt_type_t evt_type, uint8_t *p_data, uint16_t length)
{

    switch (evt_type)
    {
        case BLE_PME_EVT_NOTIF_RESULTS:
            iop_send_recognition_toggle(p_pme->is_pme_notif_enabled, p_pme->pme_notif_type);
            break;
        default:
            break;
    }
}

uint32_t ble_pme_results_set(ble_pme_t * p_pme, ble_pme_result_t * p_data)
{
    printf("In ble_pme_results_set - starts\n");
    tBleStatus retval = BLE_STATUS_SUCCESS;   
    uint16_t               length = sizeof(ble_pme_result_t);
    
    if ((length > BLE_PME_MAX_DATA_LEN) || (p_pme == NULL) || (p_data == NULL))
    {
        return BLE_STATUS_INVALID_PARAMS;
    }

    if ((p_pme->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_pme->is_pme_notif_enabled))
    {
        return BLE_STATUS_INVALID_HANDLE;
    }

    retval = aci_gatt_update_char_value(p_pme->service_handle, p_pme->pme_result_handle, 0, length, (uint8_t *)p_data);
    
    return retval;
}

uint32_t ble_pme_feature_vector_set(ble_pme_t * p_pme, ble_pme_result_w_fv_t * p_data)
{
    static uint16_t static_vals_fv_sz = (2*sizeof(uint16_t) + sizeof(uint8_t));
    uint16_t length = p_data->fv_len + static_vals_fv_sz;
    tBleStatus retval = BLE_STATUS_SUCCESS; 

    if ((p_pme->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_pme->is_pme_notif_enabled))
    {
        return BLE_STATUS_INVALID_HANDLE;
    }

    if (length > (BLE_PME_MAX_FV_FRAME_SZ + static_vals_fv_sz))
    {
        return BLE_STATUS_INVALID_PARAMS;
    }

    retval = aci_gatt_update_char_value(p_pme->service_handle, p_pme->feat_vector_handle, 0, length, (uint8_t *)p_data);
    return retval;  
}

