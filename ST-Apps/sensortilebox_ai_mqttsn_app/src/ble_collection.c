
/*==========================================================
*                                                          
*-  Copyright Notice  -------------------------------------
*                                                          
*    Licensed Materials - Property of QuickLogic Corp.     
*    Copyright (C) 2019 QuickLogic Corporation             
*    All rights reserved                                   
*    Use, duplication, or disclosure restricted            
*                                                          
*    File   : iop_messages.c
*    Purpose: 
*                                                          
*=========================================================*/

#include "Fw_global_config.h"
#include "FreeRTOS.h"
#include "task.h"
#include "sensor_service.h"
#include "ble_pme.h"
#include "ble_iop_messages.h"
#include "ble_collection.h"

#include "dcl_commands.h"

//from bluenrg1_gatt_aci.c
extern tBleStatus aci_gatt_write_resp(uint16_t Connection_Handle,
                               uint16_t Attr_Handle,
                               uint8_t Write_status,
                               uint8_t Error_Code,
                               uint8_t Attribute_Val_Length,
                               uint8_t Attribute_Val[]);

extern ble_iop_connection_t				m_iop_connection;  // Collection service and Device  Information service handles

#include "main.h"
// disabling this check for now
#if !defined(VERIFY_PARAM_NOT_NULL)
#define VERIFY_PARAM_NOT_NULL()
#endif

#define NRF_LOG_MODULE_NAME "COLL"

#define BLE_UUID_COLLECTION_CONFIG_CHAR 0x0101 /**< The UUID of the config Characteristic. */
#define BLE_UUID_COLLECTION_RAW_CHAR 0x0102        /**< The UUID of the raw data Characteristic. */
#define BLE_UUID_COLLECTION_FILE_CHAR 0x0103       /**< The UUID of the file name Characteristic. */
#define BLE_UUID_COLLECTION_DATE_CHAR 0x0104       /**< The UUID of the file name Characteristic. */

// EF68xxxx-9B35-4933-9B10-52FFA9740042
#define DCL_BASE_UUID                                          \
	{                                                          \
		{                                                      \
			0x59, 0x31, 0x72, 0x6f, 0xF2, 0x7a, 0xf7, 0x90,    \
				0xdd, 0x46, 0x22, 0x5a, 0x00, 0x00, 0x42, 0x42 \
		}                                                      \
	}
 
/* Private variables ---------------------------------------------------------*/
static char current_file_name[MAX_FILE_PREFIX_LEN];    
    
/* Global functions ----------------------------------------------------------*/
extern uint8_t *my2_ble_callback( uint8_t *pData );
    
 
//#define configASSERT2( x ) if( ( x ) == 0 ) { STLBLE_PRINTF("configASSERT2 File:%s, Line:%d\n",__FILE__, __LINE__ ); }

#define BLE_GATT_HVX_NOTIFICATION          0x01  /**< Handle Value Notification. */

bool ble_srv_is_notification_enabled(uint8_t const * p_encoded_data)
{
    //uint16_t cccd_value = uint16_decode(p_encoded_data);
    uint16_t cccd_value = (((uint16_t)((uint8_t *)p_encoded_data)[0])) |
                 (((uint16_t)((uint8_t *)p_encoded_data)[1]) << 8 );
                   
    return ((cccd_value & BLE_GATT_HVX_NOTIFICATION) != 0);
}

/* Private functions ---------------------------------------------------------*/

static uint32_t ble_config_set(ble_collection_t *p_collection, uint16_t attr_handle, uint8_t* p_data, uint8_t p_len);

uint32_t ble_capture_filename_apply(char *filename, uint16_t length)
{
    if( (filename == NULL) || (length == 0) )
    {
        STLBLE_PRINTF("\nble_capture_filename_apply(): Param error");
        return BLE_STATUS_INVALID_PARAMS;
    }
	memset(current_file_name, 0, sizeof(current_file_name));
	memcpy(current_file_name, filename, length);
#if QUICKAI_BLE_UART_ENABLE
	//Only send motion config to m4 when it comes from BLE.
	iop_send_filename(current_file_name, length);
#endif

	return 0;
}

uint32_t ble_collection_toggle(bool enable)
{
    iop_send_motion_toggle(enable);
    return 0;
}

static uint8_t *iop_translate_dcl_cmd( uint8_t majorId, uint8_t * pdata)
{
    if(pdata == NULL)
    {
        STLBLE_PRINTF("\niop_translate_dcl_cmd() : Param error");
        return NULL;
    }
    static uint8_t msg[(BLE_COLLECTION_CFG_DATA_SZ + 2)];

    memset(&msg, 0, (BLE_COLLECTION_CFG_DATA_SZ + 2));
    msg[0] = BLE_COLLECTION_CFG_DATA_SZ + 2;
    msg[1] = majorId;
	if(pdata != NULL)
	{
        memcpy(&msg[2], pdata, BLE_COLLECTION_CFG_DATA_SZ);
	}
    return (uint8_t *)(my2_ble_callback( msg ));
}
void ble_collection_data_handler(ble_collection_t *p_collection, uint16_t attr_handle, BLE_COLLECTION_EVT_type_t evt_type, uint16_t length, uint8_t * p_data)
{
    if((p_collection == NULL) || (p_data == NULL) || (length == 0))
    {
        STLBLE_PRINTF("\ble_collection_data_handler() : Param error");
        return;
    }
    
    uint32_t err_code = 0;
    static uint8_t *p_ble_resp = NULL;
    static uint8_t ble_resp_len = 20;
    static uint8_t *p_data_receive = NULL;
    bool isRespBack = FALSE;
    static uint8_t version_str[20];
    static uint8_t temp_str[MAX_BLE_PKT_SIZE];

    switch (evt_type)
    {
        case BLE_COLLECTION_EVT_NOTIF_CONFIG:
            //Nothing to do.
            STLBLE_PRINTF("\n BLE_COLLECTION_EVT_NOTIF_CONFIG");
            break;

        case BLE_COLLECTION_EVT_NOTIF_RAW:
            STLBLE_PRINTF("\n BLE_COLLECTION_EVT_NOTIF_RAW");
            err_code = ble_collection_toggle(p_collection->is_raw_notif_enabled);
            //APP_ERROR_CHECK(err_code);
            break;

        case BLE_COLLECTION_EVT_CONFIG_RECEIVED:
            err_code = 0;
            iop_collection_config_t config;
            memset(&config, 0, sizeof(iop_collection_config_t));
            memcpy(&config, p_data, length);
            //NRF_LOG_INFO("Cfg %d\n", p_config->cfg_type);

            STLBLE_PRINTF("\n\n BLE_COLLECTION_EVT_CONFIG_RECEIVED");
            switch(config.cfg_type)
            {
                //BLE should be a dumb pipe.
                case IOP_COLLECTION_CLEAR_SENSORS:
                case IOP_COLLECTION_ADD_SENSOR:
                case IOP_COLLECTION_CONFIG_DONE:
                case IOP_COLLECTION_GET_SENSOR_LIST:
                case IOP_COLLECTION_SET_UNIX_DATETIME:
                    STLBLE_PRINTF("\n BLE_COLLECTION_EVT::: cfg_type =%d\n", config.cfg_type);
                    iop_send_sensor_config_msg(config.cfg_type, config.cfg_data);
                    err_code = ble_config_set(p_collection, attr_handle, p_data, length);
                    break;

                case IOP_COLLECTION_RESERVED_DCL_SET_STORAGE:
                    iop_translate_dcl_cmd(IOP_MSG_STORAGE_CONFIG, config.cfg_data);
                    break;

                case IOP_COLLECTION_RESERVED_DCL_GET_STATUS:
                    p_data_receive = (uint8_t *)iop_translate_dcl_cmd(IOP_MSG_GET_STATUS, config.cfg_data);
                    if(p_data_receive != NULL)
                    {
                        temp_str[0] = IOP_COLLECTION_RESERVED_DCL_GET_STATUS;
                        memcpy((void*)&temp_str[1], (void*)&p_data_receive[3], (p_data_receive[0] - 2));
                        p_ble_resp = (uint8_t *)(&temp_str[0]);
                        ble_resp_len = 20;
                        isRespBack = TRUE;
                    }
                    break;

                case DCL_COMMAND_CLEAR_ERR_BIT:
                    //nrf_delay_ms(1);
                    iop_translate_dcl_cmd(IOP_MSG_CLR_STATUS, config.cfg_data);
                    //STLBLE_PRINTF("\n BLE_COLLECTION_EVT::: IOP_COLLECTION_CLEAR_ERR_BIT");
                    break;

                case IOP_COLLECTION_RESERVED_DCL_RECORD_START:
                    //nrf_delay_ms(1);
                    iop_send_record_toggle(true, config.cfg_data);
                    //STLBLE_PRINTF("\n BLE_COLLECTION_EVT::: IOP_COLLECTION_RESERVED_DCL_RECORD_START");
                    break;

                case IOP_COLLECTION_RESERVED_DCL_RECORD_STOP:
                    //nrf_delay_ms(1);
                    iop_send_record_toggle(false, config.cfg_data);
                    break;

                case DCL_COMMAND_RESERVED_DCL_GET_VERSION:
                    //nrf_delay_ms(1);
                    p_data_receive = (uint8_t *)(iop_translate_dcl_cmd(IOP_MSG_GET_VERSION, config.cfg_data));
                    if(p_data_receive != NULL)
                    {
                        version_str[0] = DCL_COMMAND_RESERVED_DCL_GET_VERSION;
                        memcpy((void*)&version_str[1], (void*)&p_data_receive[3], (p_data_receive[0] - 3) );
                        p_ble_resp = (uint8_t *)(&version_str[0]);
                        ble_resp_len = 12;
                        isRespBack = TRUE;
                    }
                    STLBLE_PRINTF("\n BLE_COLLECTION_EVT::: DCL_COMMAND_RESERVED_DCL_GET_VERSION");
                    break;

                case DCL_COMMAND_RESERVED_DCL_GET_COMP_DT:
                    //nrf_delay_ms(1);
                    p_ble_resp = (uint8_t *)iop_translate_dcl_cmd(IOP_MSG_GET_COMPDATETIME, config.cfg_data);
                    if(p_ble_resp != NULL)
                    {
                        ble_resp_len = 20;
                        isRespBack = TRUE;
                    }
                    STLBLE_PRINTF("\n BLE_COLLECTION_EVT::: DCL_COMMAND_RESERVED_DCL_GET_COMP_DT");
                    break;

                default:

                    break;
            } // end of  switch(config.cfg_type)

            //APP_ERROR_CHECK(err_code);
            break;

        case BLE_COLLECTION_EVT_FILE_NAME:
            //APP_ERROR_CHECK_BOOL(length <= 20);
            err_code = ble_capture_filename_apply((char *)p_data, length);
            ////APP_ERROR_CHECK(err_code); //@todo - replace with assert
            STLBLE_PRINTF("\n BLE_COLLECTION_EVT::: BLE_COLLECTION_EVT_FILE_NAME****************");
            isRespBack = false;
            break;

        default:
            break;
    } // end of switch (evt_type)

    if((isRespBack == true) && (p_ble_resp != NULL) && (err_code == 0))
    {
        aci_gatt_update_char_value(p_collection->service_handle, m_iop_connection.m_collection.config_handles, 0, ble_resp_len, p_ble_resp);
    }
}

/**@brief Function for handling the @ref BLE_GATTS_EVT_WRITE event from the S132 SoftDevice.
 *
 * @param[in] p_motion     Motion Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
void ble_motion_on_write(ble_collection_t * p_collection,  uint16_t attr_handle, uint8_t data_length, uint8_t *p_data)
{
    if((p_collection == NULL) || (data_length == 0) || (p_data == NULL))
    {
        STLBLE_PRINTF("\n ble_motion_on_write(): Param error");
        return;
    }
	bool notif_enabled;

    if ( (attr_handle == p_collection->raw_handles + 2) && (data_length == 2))
	{

		notif_enabled = ble_srv_is_notification_enabled(p_data);

		if (p_collection->is_raw_notif_enabled != notif_enabled)
		{
			p_collection->is_raw_notif_enabled = notif_enabled;
            
            ble_collection_data_handler(p_collection, attr_handle, BLE_COLLECTION_EVT_NOTIF_RAW, data_length, p_data);
            STLBLE_PRINTF("\nBLE_COLLECTION_EVT_NOTIF_RAW =======\n");
		}
	}
	if ((attr_handle == p_collection->config_handles + 2) &&
		(data_length == 2))
	{

        STLBLE_PRINTF("\nBLE_COLLECTION_EVT_NOTIF_CONFIG =======\n");
		notif_enabled = ble_srv_is_notification_enabled(p_data);

		if (p_collection->is_command_config_notif_en != notif_enabled)
		{
			p_collection->is_command_config_notif_en = notif_enabled;

            ble_collection_data_handler(p_collection, attr_handle, BLE_COLLECTION_EVT_NOTIF_CONFIG, data_length, p_data);
		}
	}


	else
	{
		// Do Nothing. This event is not relevant for this service.
	}
}


uint32_t ble_motion_raw_set(ble_collection_t *p_collection, ble_motion_raw_t *p_data)
{
    if((p_collection == NULL) || (p_data == NULL))
    {
        STLBLE_PRINTF("\nble_motion_raw_set() : Param Error");
        return BLE_STATUS_INVALID_PARAMS;
    }

    tBleStatus retval = BLE_STATUS_SUCCESS;

	uint16_t length = sizeof(ble_motion_raw_t) * BLE_MOTION_SEND_SAMPLES;

    if ((length > BLE_MOTION_MAX_DATA_LEN) || (p_data == NULL) || (p_collection == NULL))
	{
		return BLE_STATUS_INVALID_PARAMS;
	}

	if ((p_collection->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_collection->is_raw_notif_enabled))
	{
		return BLE_STATUS_FAILED;
	}

    retval = aci_gatt_update_char_value(m_iop_connection.m_collection.service_handle, m_iop_connection.m_collection.raw_handles, 0, length, (uint8_t *)p_data);
    if(retval != BLE_STATUS_SUCCESS)
    {
        //STLBLE_PRINTF("\nble_motion_raw_set() : Falied to update Charateristic");
    }

	return retval;
}


uint32_t ble_config_set(ble_collection_t *p_collection, uint16_t attr_handle, uint8_t* p_data, uint8_t p_len)
{
 	uint16_t length = MAX_BLE_PKT_SIZE;
    tBleStatus retval = BLE_STATUS_SUCCESS;
    
    if((p_collection == NULL) || (p_data == NULL))
    {
        STLBLE_PRINTF("\ble_config_set() : Param Error");
        return BLE_STATUS_INVALID_PARAMS;
    }

    if (length > BLE_MOTION_MAX_DATA_LEN)
	{
		return BLE_STATUS_INVALID_PARAMS;
	}

	if ((p_collection->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_collection->is_command_config_notif_en))
	{
		return BLE_STATUS_NULL_PARAM;
	}

    retval = aci_gatt_write_resp(p_collection->conn_handle,
                                               attr_handle,
                                               BLE_STATUS_SUCCESS,
                                               0x00,
                                               length,
                                               (uint8_t *)p_data);

    printf("\n ble_config_set");
	return retval;
}

