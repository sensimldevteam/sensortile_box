/*==========================================================
*
 *-  Copyright Notice  -------------------------------------
*
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
*
 *    File   : main.c
 *    Purpose: 
*
 *=========================================================*/

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include "Fw_global_config.h"
#include "dcl_commands.h"
#include "DataCollection.h"
#include "ql_bleTask.h"
#include "RtosTask.h"
#include "TargetFeatures.h"
#include "hci_tl_interface.h"
#include "main.h"
//#include "SensorTile_audio.h"
//#include "sensor_service.h"
//#include "bluenrg_utils.h"
#include "cmsis_os.h"
#include "datalog_application.h"

#include "micro_tick64.h"

#include "datablk_mgr.h"
#include "datablk_processor.h"
#include "process_ids.h"

#include "process_Audio.h"
#include "process_imu.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

typedef enum
{
  THREAD_1 = 0,
  THREAD_2,
  THREAD_3
} Thread_TypeDef;

/* Imported Variables -------------------------------------------------------------*/
extern osSemaphoreId readDataSem_id;
/* Exported Variables -------------------------------------------------------------*/

uint8_t BufferToWrite[256];
int32_t BytesToWrite;


/* Private variables ---------------------------------------------------------*/
static volatile int           ButtonPressed    = 0;
static volatile int           MEMSInterrupt    = 0;
volatile int PowerButtonPressed                = 0;
volatile uint32_t SendBatteryInfo    = 0;
volatile uint32_t SendEnv            = 0;
volatile uint32_t SendAccGyroMag     = 0;
volatile        int      hci_event          = 0;
volatile        int      BlinkLed           = 0;


osThreadId BLEThreadId;

char TmpBufferToWrite[256];

/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);

extern void Init_BlueNRG_Custom_Services(void);
extern void Init_BlueNRG_Stack(void);
extern void InitTimers(void);

extern void Poll_BLE_Events(void const *argument);
extern void GetData_Thread(void const *argument);
extern void WriteData_Thread(void const *argument);

volatile uint8_t no_H_HTS221 = 0;
volatile uint8_t no_T_HTS221 = 0;

void dataTimer_Callback(void const *arg);
void dataTimerStart(void);
void dataTimerStop(void);

osTimerId sensorTimId = 0;
osTimerDef(SensorTimer, dataTimer_Callback);

uint32_t  exec;

#if DBG_FLAGS_ENABLE
uint32_t DBG_flags = DBG_flags_default;
#endif

const char *SOFTWARE_VERSION_STR;

uint8_t DeviceClassUUID[UUID_TOTAL_BYTES] = {
                                                0x49, 0xab, 0xf7, 0xa8,
                                                0xef, 0x7b, 0x11, 0xe9,
                                                0x81, 0xb4, 0x2a, 0x2a,
                                                0xe2, 0xdb, 0xcc, 0xe4
                                        };
char *pSupportedPaths[NUM_SUPPORTED_PATHS] = {"/default/"};

/*========== BEGIN: AUDIO SENSOR Datablock processor definitions =============*/
/** @addtogroup QAI_AUDIO_PIPELINE_EXAMPLE QuickAI SDK Audio pipeline example
 *
 *  @brief Audio pipeline example code
 * This example code demonstrates setting up Audio Queues,
 * setting up the audio brick buffer manager (\ref DATABLK_MGR)
 * and setting up the thread processing elements (\ref DATABLK_PE).
 * 
 * @{
 */

#define NUM_AUDIO_BRICKS (20)
#define DBP_AUDIO_FRAME_SIZE (240)

/** Maximum number of audio bricks that may be queued for chain processing */
#define MAX_AUDIO_DATA_BLOCKS                  (NUM_AUDIO_BRICKS)

/** maximum number of vertical (parallel processing elements) that may generate datablock outputs 
 *  that may add to the front of the queue.
 *
 *  Queue size of a given datablock processor must be atleast 
 *  summation of maximum datablocks of all sensors registered for
 *  processing with some room to handle the vertical depth
 */
#define MAX_THREAD_VERTICAL_DEPTH_DATA_BLOCKS  (5)

#define DBP_AUDIO_THREAD_Q_SIZE   (MAX_AUDIO_DATA_BLOCKS+MAX_THREAD_VERTICAL_DEPTH_DATA_BLOCKS)
#define DBP_AUDIO_THREAD_PRIORITY (10)

typedef struct {
  QAI_DataBlockHeader_t dbHeader;
  int16_t pcm_data[DBP_AUDIO_FRAME_SIZE];
} QAI_AudioBrick_t ;

QAI_AudioBrick_t audio_data_blocks[NUM_AUDIO_BRICKS] ;
QAI_DataBlockMgr_t audioBuffDataBlkMgr;
QueueHandle_t  dbp_audio_thread_q;

/* Audio AI processing element functions */
datablk_pe_funcs_t audio_ai_funcs = {NULL, audio_ai_data_processor, NULL, NULL, (void *)NULL } ;

// outQ processor for audio block processor
outQ_processor_t audio_ai_outq_processor =
{
  .process_func = NULL,
  .p_dbm = &audioBuffDataBlkMgr,
  .in_pid = AUDIO_SENSIML_AI_PID,
  .outQ_num = 0,
  .outQ = NULL,
  .p_event_notifier = NULL
};

datablk_pe_descriptor_t  datablk_pe_descr_audio[] =
{ // { IN_ID, OUT_ID, ACTIVE, fSupplyOut, fReleaseIn, outQ, &pe_function_pointers, bypass_function, pe_semaphore }
  
    /* processing element descriptor for AI */
    { AUDIO_ISR_PID, AUDIO_SENSIML_AI_PID, true, false, true, &audio_ai_outq_processor, &audio_ai_funcs, NULL, NULL},   
};

datablk_processor_params_t datablk_processor_params[] = { 
    { DBP_AUDIO_THREAD_PRIORITY, 
      &dbp_audio_thread_q, 
      sizeof(datablk_pe_descr_audio)/sizeof(datablk_pe_descr_audio[0]), 
      datablk_pe_descr_audio, 
      256, 
      "DBP_AUDIO_THREAD", 
      NULL
    }
};

/* Audio PDM capture ISR */
#define AUDIO_ISR_OUTQS_NUM (1)
QueueHandle_t *audio_isr_outQs[AUDIO_ISR_OUTQS_NUM] = { &dbp_audio_thread_q };
outQ_processor_t audio_isr_outq_processor =
{
  .process_func = audio_isr_callback,
  .p_dbm = &audioBuffDataBlkMgr,
  .in_pid = AUDIO_ISR_PID,
  .outQ_num = 1,
  .outQ = audio_isr_outQs,
  .p_event_notifier = NULL
};

extern  void sensor_audio_configure(void);
extern void set_first_audio_data_block();
void audio_block_processor(void)
{
  /** Thread 1 : Create Audio Queues */
  dbp_audio_thread_q = xQueueCreate(DBP_AUDIO_THREAD_Q_SIZE, sizeof(QAI_DataBlock_t *));
  vQueueAddToRegistry( dbp_audio_thread_q, "AudioPipelineExampleQ" );
  set_first_audio_data_block();
  /** Thread 1: Setup Audio Thread Handler Processing Elements */
  datablk_processor_task_setup(&datablk_processor_params[0]);
  
  /* [TBD]: sensor configuration : should this be here or after scheduler starts? */
  sensor_audio_configure();
}
/*========== END: AUDIO SENSOR Datablock processor definitions =============*/

/*========== BEGIN: IMU SENSOR Datablock processor definitions =============*/
/** @addtogroup QAI_IMU_PIPELINE_EXAMPLE QuickAI SDK IMU pipeline example
 *
 *  @brief IMU (Inertial measurement unit) pipeline example code
 *
 * This example code demonstrates setting up IMU Queues,
 * setting up the datablock buffer manager (\ref DATABLK_MGR)
 * and setting up the datablock processor processing elements (\ref DATABLK_PE).
 * A specific IMU processing element for motion detection is provided in this
 * example.
 * 
 * @{
 */

#define IMU_MAX_BATCH_SIZE (4*18)      ///< Approximately 10ms @6666Hz
#define IMU_FRAME_SIZE_PER_CHANNEL       (IMU_MAX_BATCH_SIZE)
#define IMU_NUM_CHANNELS      (6)    ///< (X,Y,Z) for Accel and (X,Y,Z) for Gryo
#define IMU_FRAME_SIZE      ( (IMU_FRAME_SIZE_PER_CHANNEL) * (IMU_NUM_CHANNELS) )
#define NUM_IMU_DATA_BLOCKS ( 20)    ///< 100 data blocks approximately 1 sec of data

/** Maximum number of imu data blocks that may be queued for chain processing */
#define MAX_IMU_DATA_BLOCKS                  (NUM_IMU_DATA_BLOCKS)

/** maximum number of vertical (parallel processing elements) that may generate datablock outputs 
 *  that may add to the front of the queue.
 *
 *  Queue size of a given datablock processor must be atleast 
 *  summation of maximum datablocks of all sensors registered for
 *  processing with some room to handle the vertical depth
 */
#define MAX_THREAD_VERTICAL_DEPTH_DATA_BLOCKS  (5)

#define DBP_IMU_THREAD_Q_SIZE   (MAX_IMU_DATA_BLOCKS+MAX_THREAD_VERTICAL_DEPTH_DATA_BLOCKS)
#define DBP_IMU_THREAD_PRIORITY (10)

typedef struct {
  QAI_DataBlockHeader_t dbHeader;
  int16_t imu_data[IMU_FRAME_SIZE];
} QAI_IMU_DataBlock_t ;

QAI_IMU_DataBlock_t imu_data_blocks[NUM_IMU_DATA_BLOCKS] ;
QAI_DataBlockMgr_t imuBuffDataBlkMgr;
QueueHandle_t  dbp_imu_thread_q;

/* IMU AI processing element functions */
datablk_pe_funcs_t imu_sensiml_ai_funcs = {NULL, imu_ai_data_processor, NULL, NULL, (void *)NULL } ;

// outQ processor for IMU block processor
outQ_processor_t imu_sensiml_ai_outq_processor =
{
  .process_func = NULL,
  .p_dbm = &imuBuffDataBlkMgr,
  .in_pid = IMU_SENSIML_AI_PID,
  .outQ_num = 0,
  .outQ = NULL,
  .p_event_notifier = NULL
};

datablk_pe_descriptor_t  datablk_pe_descr_imu[] =
{ // { IN_ID, OUT_ID, ACTIVE, fSupplyOut, fReleaseIn, outQ, &pe_function_pointers, bypass_function, pe_semaphore }
  
    /* processing element descriptor for SensiML AI for IMU sensor */
    { IMU_ISR_PID, IMU_SENSIML_AI_PID, true, false, true, &imu_sensiml_ai_outq_processor, &imu_sensiml_ai_funcs, NULL, NULL},   
};

datablk_processor_params_t datablk_processor_params_imu[] = { 
    { DBP_IMU_THREAD_PRIORITY, 
      &dbp_imu_thread_q, 
      sizeof(datablk_pe_descr_imu)/sizeof(datablk_pe_descr_imu[0]), 
      datablk_pe_descr_imu, 
      256, 
      "DBP_IMU_THREAD", 
      NULL
    }
};

/* IMU sensors data reading function */
#define IMU_ISR_OUTQS_NUM (1)
QueueHandle_t *imu_isr_outQs[IMU_ISR_OUTQS_NUM] = { &dbp_imu_thread_q };
extern void imu_sensordata_read_callback(void);
extern void imu_event_notifier(int pid, int event_type, void *p_event_data, int num_data_bytes);
extern void set_first_imu_data_block(void);

outQ_processor_t imu_isr_outq_processor =
{
  .process_func = imu_sensordata_read_callback,
  .p_dbm = &imuBuffDataBlkMgr,
  .in_pid = IMU_ISR_PID,
  .outQ_num = 1,
  .outQ = imu_isr_outQs,
  .p_event_notifier = imu_event_notifier
};

void imu_block_processor(void)
{
  /** IMU datablock processor thread : Create IMU Queues */
  dbp_imu_thread_q = xQueueCreate(DBP_IMU_THREAD_Q_SIZE, sizeof(QAI_DataBlock_t *));
  vQueueAddToRegistry( dbp_imu_thread_q, "IMUPipelineExampleQ" );
  
  /** IMU datablock processor thread : Setup IMU Thread Handler Processing Elements */
  datablk_processor_task_setup(&datablk_processor_params_imu[0]);
  
  /** Set the first data block for the ISR or callback function */
  set_first_imu_data_block();
  /* [TBD]: sensor configuration : should this be here or after scheduler starts? */
  //sensor_imu_configure();
}

// temporary buffer to read sensor data from the FIFO 
//LSM6DSOX FIFO stores TAG + 6 bytes per sensor. TAG tells whether it is Accel or Gyro
//So every time, TAG byte has to be read first and 6 bytes of the Sensor next (1+6)
uint8_t fifo_bytes[(IMU_MAX_BATCH_SIZE+1)*2*(1+6)]; // 2 since accel and gyro

int imu_get_max_datablock_size(void)
{
  return IMU_MAX_BATCH_SIZE;
}

/*========== END: IMU SENSOR Datablock processor definitions =============*/

struct st_dbm_init {
  QAI_DataBlockMgr_t *pdatablk_mgr_handle;
  void  *pmem;
  int mem_size;
  int item_count;
  int item_size_bytes;
} ;

struct st_dbm_init dbm_init_table[] =
{ 
  {&imuBuffDataBlkMgr,   (void *)  imu_data_blocks, sizeof(  imu_data_blocks), IMU_FRAME_SIZE, sizeof(int16_t)},
  {&audioBuffDataBlkMgr, (void *)audio_data_blocks, sizeof(audio_data_blocks), DBP_AUDIO_FRAME_SIZE, sizeof(int16_t)},
};

void init_all_datablock_managers(struct st_dbm_init *p_dbm_table, int len_dbm_table)
{
  /** Setup the data block managers */
  for (int k = 0; k < len_dbm_table; k++) {
      datablk_mgr_init( p_dbm_table[k].pdatablk_mgr_handle, 
                        p_dbm_table[k].pmem, 
                        p_dbm_table[k].mem_size, 
                        p_dbm_table[k].item_count, 
                        p_dbm_table[k].item_size_bytes
                      );
  }

}

void setup_sensors_data_block_processor(void)
{
  /** Initialize all datablock managers */
  init_all_datablock_managers(dbm_init_table, sizeof(dbm_init_table)/sizeof(struct st_dbm_init));
  
  audio_block_processor();

  imu_block_processor();
}

/**
* @brief  Main program
* @param  None
* @retval None
*/
int main(void)
{
#if S3AI_FIRMWARE_IS_COLLECTION
    SOFTWARE_VERSION_STR = "C April-2020";
#endif
#if S3AI_FIRMWARE_IS_RECOGNITION
    SOFTWARE_VERSION_STR = "R April-2020";
#endif

  /* HAL_Init */
  HAL_Init();

  /* Configure the System clock */
  SystemClock_Config();

  InitTargetPlatform();

#ifdef STBOX1_ENABLE_PRINTF
  STLBLE_PRINTF("\tSensorTileBox %s\r\n", SOFTWARE_VERSION_STR);
  DisplayFirmwareInfo();
#endif /* STBOX1_ENABLE_PRINTF */

#ifdef ENABLE_USB_DEBUG_CONNECTION
  STLBLE_PRINTF("Debug Connection         Enabled\r\n");
#endif /* ENABLE_USB_DEBUG_CONNECTION */
  
#ifdef ENABLE_USB_DEBUG_NOTIFY_TRAMISSION
  STLBLE_PRINTF("Debug Notify Trasmission Enabled\r\n");
#endif /* ENABLE_USB_DEBUG_NOTIFY_TRAMISSION */

  /* Initialize the BlueNRG */
  Init_BlueNRG_Stack();
  STLBLE_PRINTF("Init_BlueNRG_Stack()\r\n");
  
  /* Initialize the BlueNRG Custom services */
  Init_BlueNRG_Custom_Services();  
  STLBLE_PRINTF("Init_BlueNRG_Custom_Services()\r\n");

    /* Initialize and Enable the available sensors */
//  MX_X_CUBE_MEMS1_Init();
//  STLBLE_PRINTF("MX_X_CUBE_MEMS1_Init()\r\n");
#if 0 //not available for SensorTileBoc  
extern int32_t LSM6DSM_Sensor_IO_ITConfig( void );  
  /* COnfigure LSM6DSM Double Tap interrupt*/  
  LSM6DSM_Sensor_IO_ITConfig();
  STLBLE_PRINTF("LSM6DSM_Sensor_IO_ITConfig()\r\n");
#endif
  /* initialize timers */
  //InitTimers();
  //STLBLE_PRINTF("InitTimers()\r\n");

   setup_sensors_data_block_processor();
   STLBLE_PRINTF("setup_sensors_data_block_processor()\r\n");

   StartRtosTaskMqttsnApp();
   StartRtosTaskMqttsnMsgHandler();

   // initial virtual sensor type
   sensor_set_virtual_sensor(IMU_V_SENSOR_NO);
#if S3AI_FIRMWARE_IS_COLLECTION
   create_datasave_task();
   STLBLE_PRINTF("create_datasave_task()\r\n");
#endif
   
#if S3AI_FIRMWARE_IS_RECOGNITION    
   StartRtosTaskRecognition();
   STLBLE_PRINTF("StartRtosTaskRecognition()\r\n");
#endif

   StartRtosTaskBLE();
   STLBLE_PRINTF("StartRtosTaskBLE()\r\n");
   
   xTaskSet_uSecCount(1546300800ULL * 1000ULL * 1000ULL); // start at 2019-01-01 00:00:00 UTC time

  /* Thread 3 definition -- BLE Task */
  osThreadDef(THREAD_3, Poll_BLE_Events, osPriorityRealtime, 0,  configMINIMAL_STACK_SIZE*16);
  /* Start thread 3 -- BLE Task */
  BLEThreadId = osThreadCreate(osThread(THREAD_3), NULL);
  configASSERT(BLEThreadId);

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  for (;;);

}


void dataTimer_Callback(void const *arg)
{ 
  // Warning: must not call vTaskDelay(), vTaskDelayUntil(), or specify a non zero 
  // block time when accessing a queue or a semaphore. 
  imu_sensordata_read_callback(); //osSemaphoreRelease(readDataSem_id);
} 
#if (USE_IMU_FIFO_MODE == 1)
extern void enable_lsm6dsox_stream(void);
extern int disable_lsm6dsox_stream(void);
#endif 
void dataTimerStart(void)
{
  osStatus  status;
#if (USE_IMU_FIFO_MODE == 1)
    int milli_secs = 9; // reads fifo @10milli-secs intervals
#else
    int milli_secs = 2; // reads when a sample is available (upto 416Hz)
#endif
  // Create periodic timer
  exec = 1;
  if (!sensorTimId) {
    sensorTimId = osTimerCreate(osTimer(SensorTimer), osTimerPeriodic, &exec);
  }
  
  if (sensorTimId)  {
    status = osTimerStart (sensorTimId, milli_secs);  // start timer
    if (status != osOK)  {
      // Timer could not be started
    } 
  }
  set_first_imu_data_block();
#if (USE_IMU_FIFO_MODE == 1)
  enable_lsm6dsox_stream();
#endif
}

void dataTimerStop(void)
{
  if (sensorTimId) {
    osTimerStop(sensorTimId);
  }
#if (USE_IMU_FIFO_MODE == 1)
  disable_lsm6dsox_stream();
#endif

}

/**
* @brief  System Clock Configuration
* @param  None
* @retval None
*/
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};
  
    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST) != HAL_OK) {
    /* Initialization Error */
    while(1);
  
  }
  
   /**Configure LSE Drive Capability 
    */
  HAL_PWR_EnableBkUpAccess();
  
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|
                                     RCC_OSCILLATORTYPE_LSE  |
                                     RCC_OSCILLATORTYPE_HSE  |
                                     RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.MSIState            = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange       = RCC_MSIRANGE_11;
  RCC_OscInitStruct.PLL.PLLState        = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 60;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
    /* Initialization Error */
    while(1);
  }
  
    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK   |
                                RCC_CLOCKTYPE_SYSCLK |
                                RCC_CLOCKTYPE_PCLK1  |
                                RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK) {
    /* Initialization Error */
    while(1);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_SAI1   |
                                      RCC_PERIPHCLK_DFSDM1 |
                                      RCC_PERIPHCLK_USB    |
                                      RCC_PERIPHCLK_RTC    |
                                      RCC_PERIPHCLK_SDMMC1 |
                                      RCC_PERIPHCLK_ADC;

  PeriphClkInit.Sai1ClockSelection = RCC_SAI1CLKSOURCE_PLLSAI1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;  
  PeriphClkInit.Dfsdm1ClockSelection = RCC_DFSDM1CLKSOURCE_PCLK2;  
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLP;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 5;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 96;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV25;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV4;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV4;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK|RCC_PLLSAI1_SAI1CLK;  
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
    /* Initialization Error */
    while(1);
  }
}
/**
* @brief This function provides accurate delay (in milliseconds) based 
*        on variable incremented.
* @note This is a user implementation using WFI state
* @param Delay: specifies the delay time length, in milliseconds.
* @retval None
*/
void HAL_Delay(__IO uint32_t Delay)
{
  uint32_t tickstart = 0;
  tickstart = HAL_GetTick();
  while((HAL_GetTick() - tickstart) < Delay){
    __WFI();
  }
}
/**
* @brief  This function is executed in case of error occurrence.
* @param  None
* @retval None
*/
void Error_Handler(void)
{
  STBOX1_PRINTF("Error_Handler\r\n");
  /* User may add here some code to deal with this error */
  while(1){
  }
}

/**
 * @brief  EXTI line detection callback.
 * @param  uint16_t GPIO_Pin Specifies the pin connected EXTI line
 * @retval None
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
  {
    //msgData_t msg;
    
  switch(GPIO_Pin){
    case HCI_TL_SPI_EXTI_PIN:
      hci_tl_lowlevel_isr();
      //msg.type  = PROCESS_EVENT;
      //SendMsgToHost(&msg);
    break;
    case USER_BUTTON_PIN:
    ButtonPressed = 1;
    //if(semRun) {
    //  osSemaphoreRelease(semRun);
    //}
    break;

    case POWER_BUTTON_PIN:
      /* Power off the board */
      PowerButtonPressed = 1;
      //if(semRun) {
      //  osSemaphoreRelease(semRun);
      //}
    break;

   /* HW events from LSM6DSOX */
   case GPIO_PIN_3:
    MEMSInterrupt=1;
    //if(semRun) {
    //  osSemaphoreRelease(semRun);
    //}
    break;
  }
}


#ifdef  USE_FULL_ASSERT
/**
* @brief  Reports the name of the source file and the source line number
*         where the assert_param error has occurred.
* @param  file: pointer to the source file name
* @param  line: assert_param error line source number
* @retval None
*/
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
  ex: STLBLE_PRINTF("Wrong parameters value: file %s on line %d\r\n", file, line) */
  
  /* Infinite loop */
  while (1){
  }
}
#endif

void vApplicationStackOverflowHook( TaskHandle_t xTask,
                                    signed char *pcTaskName )
{
  configASSERT( 0 == 0 );
}

//for SensorTileBox
#ifdef STBOX1_ENABLE_PRINTF
/**
  * @brief Display all the Firmware Information
  * @param  None
  * @retval None
  */
static void DisplayFirmwareInfo(void)
{
    STBOX1_PRINTF("\r\n------------------------------------------------------------\r\n");
    STBOX1_PRINTF("STMicroelectronics %s\r\n"
         "\tVersion %c.%c.%c\r\n"
        "\tSTM32L4R9ZI-SensorTile.box board\r\n",
          STBOX1_PACKAGENAME,
          STBOX1_VERSION_MAJOR,STBOX1_VERSION_MINOR,STBOX1_VERSION_PATCH);

    STBOX1_PRINTF("\t(HAL %ld.%ld.%ld_%ld)\r\n\tCompiled %s %s"
#if defined (__IAR_SYSTEMS_ICC__)
        " (IAR)\r\n",
#elif defined (__CC_ARM)
        " (KEIL)\r\n",
#elif defined (__GNUC__)
        " (STM32CubeIDE)\r\n",
#endif
           HAL_GetHalVersion() >>24,
          (HAL_GetHalVersion() >>16)&0xFF,
          (HAL_GetHalVersion() >> 8)&0xFF,
           HAL_GetHalVersion()      &0xFF,
           __DATE__,__TIME__);
  STBOX1_PRINTF("------------------------------------------------------------\r\n");
}
#endif /* STBOX1_ENABLE_PRINTF */



/******************* (C) COPYRIGHT 2016 STMicroelectronics *****END OF FILE****/
//missing functions for S3 project
void wait_ffe_fpga_load(void){ return; };

extern uint8_t ble_iop_msg_parse(uint8_t * data);    //@TODo Indra - cleanup by calling through header file include
extern uint8_t ble_spi_tx_data[256];
void GenerateInterruptToBLE(void)
{
  ble_iop_msg_parse( ble_spi_tx_data);
  return;
};

// dummys for missing functions while linking
void iop_set_error( int error_code, uint32_t more_info ) { return; }
void iop_send_motion_data(int num, const struct iop_xyz_data * data) { return; } 
void iop_process_command( void ){ return; }
