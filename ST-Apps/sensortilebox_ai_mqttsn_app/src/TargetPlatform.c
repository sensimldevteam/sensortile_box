/**
  ******************************************************************************
  * @file    SDDataLog\Src\TargetPlatform.c
  * @author  SRA - Central Labs
  * @version V1.1.0
  * @date    10-Dec-2019
  * @brief   Initialization of the Target Platform
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2019 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
#include <stdio.h>
#include "Fw_global_config.h"
#include "TargetFeatures.h"
#include "main.h"
#include "datalog_application.h"
#include "usbd_core.h"
#include "usbd_cdc.h"
#include "usbd_cdc_interface.h"
#include "usbd_desc.h"

/* Imported variables ---------------------------------------------------------*/
extern void InitTimers(void);

/* Exported variables ---------------------------------------------------------*/
uint16_t PCM_Buffer[AUDIO_CHANNELS*PCM_AUDIO_IN_SAMPLES];
BSP_AUDIO_Init_t MicParams;
TIM_HandleTypeDef    TimCCHandle;

USBD_HandleTypeDef USBD_Device;
volatile uint8_t VCOM_RxData;
volatile uint8_t *VCOM_RxBuffer = NULL; /* Pointer to data buffer (received from USB). */
volatile uint32_t VCOM_RxLength = 0;    /* Data length (received from USB). */
uint8_t VComBufferToWrite[256];
int32_t VComBytesToWrite;



/* Local defines -------------------------------------------------------------*/

/* Local function prototypes --------------------------------------------------*/
//static void InitTimers(void);

/* Local defines -------------------------------------------------------------*/
/** @brief Initialize all the MEMS1 sensors
 * @param None
 * @retval None
 */
static void Init_MEM1_Sensors(void)
{
  /* Accelero/Gyro  */
  if (BSP_MOTION_SENSOR_Init(LSM6DSOX_0, MOTION_ACCELERO|MOTION_GYRO) == BSP_ERROR_NONE){
    STBOX1_PRINTF("OK Accelero/Gyroscope Sensor\n\r");
  } else {
    STBOX1_PRINTF("Error Accelero/Gyroscope Sensor\n\r");
    /* Starting Error */
    Error_Handler();
  }

#if (USE_IMU_FIFO_MODE == 1) 
extern void set_lsm6dsox_fifo(void);
  set_lsm6dsox_fifo();
#endif  
  
/* Magneto */
  if(BSP_MOTION_SENSOR_Init(LIS2MDL_0, MOTION_MAGNETO)==BSP_ERROR_NONE){
    STBOX1_PRINTF("OK Magneto Sensor\n\r");
  } else {
    STBOX1_PRINTF("Error Magneto Sensor\n\r");
    /* Starting Error */
    Error_Handler();
  }

  /* Humidity  */  
  if(BSP_ENV_SENSOR_Init(HTS221_0, ENV_HUMIDITY)==BSP_ERROR_NONE){
    STBOX1_PRINTF("OK Humidity Sensor\n\r");
  } else {
    STBOX1_PRINTF("Error Humidity Sensor\n\r");
    /* Starting Error */
    Error_Handler();
  }

  /* Temperature */
  if(BSP_ENV_SENSOR_Init(STTS751_0, ENV_TEMPERATURE)==BSP_ERROR_NONE){
    STBOX1_PRINTF("OK Temperature Sensor\n\r");
  } else {
    STBOX1_PRINTF("Error Temperature Sensor\n\r");
    /* Starting Error */
    Error_Handler();
  }

  /* Pressure */
  if(BSP_ENV_SENSOR_Init(LPS22HH_0, ENV_PRESSURE)==BSP_ERROR_NONE){
    STBOX1_PRINTF("OK Pressure Sensor\n\r");
  } else {
    STBOX1_PRINTF("Error Pressure Sensor\n\r");
    /* Starting Error */
    Error_Handler();
  }
}

/**
  * @brief  Initialize all the Target platform's Features
  * @param  None
  * @retval None
  */
void InitTargetPlatform(void)
{  
  /* Init Led1/Led2/Led3 */
  BSP_LED_Init(LED_BLUE);
  BSP_LED_Init(LED_GREEN);
  BSP_LED_Init(LED_RED);

  //MCR_HEART_BIT();  
  BSP_LED_On(LED_BLUE);
  BSP_LED_On(LED_GREEN);

#if 0  //keep this case we want to send this info to BLE
  /* Initialize User Button */
  BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_EXTI);

  /* Initialize the Power Button */
  BSP_PowerButton_Init();

  /* Initialize the Battery Charger */
  BSP_BC_Init();

  /* In order to be able to Read Battery Volt */
  BSP_BC_BatMS_Init();

  /* In order to Initialize the GPIO for having the battery Status */
  BSP_BC_ChrgPin_Init();
#endif

  /* Initializes the Timers */
  InitTimers();


  /* Enable USB power on Pwrctrl CR2 register */
  HAL_PWREx_EnableVddUSB();

  /*** USB CDC Configuration ***/
  /* Init Device Library */
  USBD_Init(&USBD_Device, &VCP_Desc, 0);

  /* Add Supported Class */
  USBD_RegisterClass(&USBD_Device, USBD_CDC_CLASS);

  /* Add Interface callbacks for CDC Class */
  USBD_CDC_RegisterInterface(&USBD_Device, &USBD_CDC_fops);

  /* Start Device Process */
  USBD_Start(&USBD_Device);

  /* Wait 5 seconds for looking the Initialization phases */
  HAL_Delay(1000);

  
#if 1   
  
    /* Initialize the MEMS's Sensors */
  Init_MEM1_Sensors();
  
#else

  /* Initialize the MEMS's Sensors */
  /* Magneto */
  BSP_MOTION_SENSOR_Init(LIS2MDL_0, MOTION_MAGNETO);
  BSP_MOTION_SENSOR_SetOutputDataRate(LIS2MDL_0, MOTION_MAGNETO, 100.0f);
  /* FS = 50gauss */
  BSP_MOTION_SENSOR_SetFullScale(LIS2MDL_0, MOTION_MAGNETO, 50);

  /* Acc/Gyro */
  BSP_MOTION_SENSOR_Init(LSM6DSOX_0, MOTION_ACCELERO|MOTION_GYRO);
  BSP_MOTION_SENSOR_SetOutputDataRate(LSM6DSOX_0, MOTION_GYRO, 104.0f);
  /* FS = 2000dps */
  BSP_MOTION_SENSOR_SetFullScale(LSM6DSOX_0, MOTION_GYRO, 2000);
  BSP_MOTION_SENSOR_SetOutputDataRate(LSM6DSOX_0, MOTION_ACCELERO, 100.0f);
  /* FS = 2g */
  BSP_MOTION_SENSOR_SetFullScale(LSM6DSOX_0, MOTION_ACCELERO, 2);

#if (USE_IMU_FIFO_MODE == 1) 
extern void set_lsm6dsox_fifo(void);
  set_lsm6dsox_fifo();
#endif  
  
  /* Humidity */
  BSP_ENV_SENSOR_Init(HTS221_0, ENV_HUMIDITY);
  BSP_ENV_SENSOR_SetOutputDataRate(HTS221_0, ENV_HUMIDITY,12.5f);
  
  /* Temperature */
  BSP_ENV_SENSOR_Init(STTS751_0, ENV_TEMPERATURE);
  BSP_ENV_SENSOR_SetOutputDataRate(STTS751_0, ENV_TEMPERATURE,32.0f);

  /* Pressure */
  BSP_ENV_SENSOR_Init(LPS22HH_0, ENV_PRESSURE);
  BSP_ENV_SENSOR_SetOutputDataRate(LPS22HH_0, ENV_PRESSURE,100.0f);

#endif

  /* Initialize the local filesystem and volume driver */
  //DATALOG_SD_Init();

  /* Initializes the Microphone */
  MicParams.BitsPerSample = 16;
  MicParams.ChannelsNbr   = AUDIO_CHANNELS;
  MicParams.Device        = AMIC_ONBOARD;
  MicParams.SampleRate    = AUDIO_SAMPLING_FREQUENCY;
  MicParams.Volume        = AUDIO_VOLUME_VALUE;

  if(BSP_AUDIO_IN_Init(BSP_AUDIO_IN_INSTANCE, &MicParams) != BSP_ERROR_NONE) {
    ErrorHanlder(STBOX1_AUDIO_ERROR);
  }

  /* Just for signalizing that everything is ok */
  //MCR_HEART_BIT();
  HAL_Delay(200);        \
  BSP_LED_Off(LED_BLUE); \
  BSP_LED_Off(LED_GREEN);\
  
  return;
}

/** @brief  This function Blink the LED in function of Error Code
  * @param  ErrorType_t ErrorType Error Code
  * @retval None
  */
void ErrorHanlder(ErrorType_t ErrorType)
{
  ErrorType_t CountError;
  while(1) {
    for(CountError=STBOX1_INIT_ERROR; CountError<ErrorType;CountError++) {
       BSP_LED_On(LED_RED);
       HAL_Delay(200);
       BSP_LED_Off(LED_RED);
       HAL_Delay(1000);
    }
    HAL_Delay(5000);
  }
}
//=======================================================
//from TargetPlatfrom.c in BLESensors Project
#if 0 //use the InitTimers() from ble_task.c
/**
* @brief  Function for initializing timers:
 *  - 1 for sending Battery Informations
 *  - 1 for Led Blinking
 * @param  None
 * @retval None
 */
static void InitTimers(void)
{
  uint32_t uwPrescalerValue;

  /* Timer Output Compare Configuration Structure declaration */
  TIM_OC_InitTypeDef sConfig;

  /* Compute the prescaler value to counter clock equal to 10000 Hz */
  uwPrescalerValue = (uint32_t) ((SystemCoreClock / 10000) - 1);

  /* Set TIM1 instance */
  TimCCHandle.Instance = TIM1;
  TimCCHandle.Init.Period        = 65535;
  TimCCHandle.Init.Prescaler     = uwPrescalerValue;
  TimCCHandle.Init.ClockDivision = 0;
  TimCCHandle.Init.CounterMode   = TIM_COUNTERMODE_UP;
  if(HAL_TIM_OC_Init(&TimCCHandle) != HAL_OK) {
    /* Initialization Error */
    Error_Handler();
  }

 /* Configure the Output Compare channels */

 /* Common configuration for all channels */
  sConfig.OCMode     = TIM_OCMODE_TOGGLE;
  sConfig.OCPolarity = TIM_OCPOLARITY_LOW;

  /* Output Compare Toggle Mode configuration:
   * Channel 1 for LED Blinking */
  sConfig.Pulse = STBOX1_UPDATE_LED_BATTERY;
  if(HAL_TIM_OC_ConfigChannel(&TimCCHandle, &sConfig, TIM_CHANNEL_1) != HAL_OK) {
    /* Configuration Error */
    Error_Handler();
  }

#ifdef STBOX1_ENABLE_PRINTF
  /* Output Compare Toggle Mode configuration:
   * Channel 2 for sending Battery information */
  sConfig.Pulse = STBOX1_UPDATE_VCOM;
  if(HAL_TIM_OC_ConfigChannel(&TimCCHandle, &sConfig, TIM_CHANNEL_2) != HAL_OK) {
    /* Configuration Error */
    Error_Handler();
  }
#endif /* STBOX1_ENABLE_PRINTF */  
  
  /* Output Compare Toggle Mode configuration:
   * Channel 3 for sending Environmental Features */
  sConfig.Pulse = STBOX1_UPDATE_ENV;
  if(HAL_TIM_OC_ConfigChannel(&TimCCHandle, &sConfig, TIM_CHANNEL_3) != HAL_OK) {
    /* Configuration Error */
    Error_Handler();
  }

  /* Output Compare Toggle Mode configuration:
   * Channel 4 for sending Inertial Features */
  sConfig.Pulse = STBOX1_UPDATE_INV;
  if(HAL_TIM_OC_ConfigChannel(&TimCCHandle, &sConfig, TIM_CHANNEL_4) != HAL_OK) {
    /* Configuration Error */
    Error_Handler();
  }
}
#endif //InitTimers
/**
  * @brief  This function switches on the LED
  * @param  None
  * @retval None
  */
void LedOnTargetPlatform(void)
{
  BSP_LED_On(LED_BLUE);
}

/**
  * @brief  This function switches off the LED
  * @param  None
  * @retval None
  */
void LedOffTargetPlatform(void)
{
  BSP_LED_Off(LED_BLUE);
}

/** @brief  This function toggles the LED
  * @param  None
  * @retval None
  */
void LedToggleTargetPlatform(void)
{
  BSP_LED_Toggle(LED_BLUE);
}

/** @brief  This function initiliazes the LED
  * @param  None
  * @retval None
  */
void LedInitTargetPlatform(void)
{
  BSP_LED_Init(LED_BLUE);
  BSP_LED_Init(LED_GREEN);
}

#ifdef STBOX1_ENABLE_PRINTF
/**
 * @brief  Read from VCOM
 * @param  char *buffer Pointer to buffer.
 * @param  uint32_t len_maxData max. length.
 * @retval Number of really read data bytes.
 */
uint32_t VCOM_read(char *buffer, uint32_t len_max)
{
  /* VCOM data receive not completed or no VCOM data received at all. */
  if (VCOM_RxData == 0) {
    return 0;
  }

  /* ERROR: No VCOM data ready. */
  if (VCOM_RxLength == 0 || VCOM_RxBuffer == NULL) {
    Error_Handler();
  }

  /* Read all data */
  if (VCOM_RxLength <= len_max) {
    uint32_t len = VCOM_RxLength;
    memcpy((uint8_t*)buffer, (uint8_t*)VCOM_RxBuffer, len);

    VCOM_RxData   = 0;
    VCOM_RxBuffer = NULL;
    VCOM_RxLength = 0;

    CDC_Next_Packet_Rx();
    return len;
  } else {
    /* Read part of data that fits into buffer. */
    memcpy((uint8_t*)buffer, (uint8_t*)VCOM_RxBuffer, len_max);

    VCOM_RxBuffer += len_max;
    VCOM_RxLength -= len_max;

    return len_max;
  }
}
#endif /* define STBOX1_ENABLE_PRINTF */

/******************* (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
