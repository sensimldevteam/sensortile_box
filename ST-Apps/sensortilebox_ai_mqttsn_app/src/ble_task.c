#include "Fw_global_config.h"
#include "cmsis_os.h"
#include "TargetFeatures.h"
#include "main.h"
#include "sensor_service.h"
#include "bluenrg_utils.h"
#include "ble_iop_messages.h"
#include "STBOX1_config.h"
#include "ble_status.h"

/**
 * @brief  Target's Features data structure definition
 */
typedef struct
{
/*  TargetType_t BoardType;
  uint32_t SensorEmulation;
  int32_t NumTempSensors;
  int32_t HandleTempSensors[MAX_TEMP_SENSORS];
  int32_t HandlePressSensor;
  int32_t HandleHumSensor;

  int32_t HWAdvanceFeatures;
  int32_t HandleAccSensor;
  int32_t HandleGyroSensor;
  int32_t HandleMagSensor;
*/
  uint8_t LedStatus;

  //void *HandleGGComponent;

} TargetFeatures_t;
TargetFeatures_t TargetBoardFeatures;
//10kHz/2 For Environmental data@2Hz
#define uhCCR1_Val  5000
//10kHz/20  For Acc/Gyromag@20Hz
#define uhCCR4_Val  500
extern int setConnectable(void);

extern volatile uint32_t HCI_ProcessEvent;
  
extern void hci_user_evt_proc(void);
extern void hci_init(void(* UserEvtRx)(void* pData), void* pConf);
//======================
//TIM_HandleTypeDef TimCCHandle;

extern void InitTimers(void);

/* Private variables ---------------------------------------------------------*/

uint8_t bdaddr[6];
char BoardName[8]={NAME_BLUEMS,0};

/* Imported Variables -------------------------------------------------------------*/
extern uint8_t set_connectable;
extern int connected;
extern TIM_HandleTypeDef  TimHandle;

uint32_t ConnectionBleStatus  =0;

extern void CDC_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);


ble_iop_connection_t				m_iop_connection;  // Collection service and Device  Information service handles

extern tBleStatus add_pme_service(ble_iop_connection_t *m_iop_connection);
extern tBleStatus add_collection_service(ble_iop_connection_t *m_iop_connection);
extern tBleStatus QAI_addDeviceInformationService(void);

/**
  * @brief  Get data raw from sensors to queue
  * @param  thread not used
  * @retval None
  */
void Poll_BLE_Events(void const *argument)
{
  uint32_t StartTime;
  (void) argument;
  StartTime = HAL_GetTick();
  
  /* Infinite loop */
  while (1)
  { 
   /* Led Blinking when there is not a client connected */
    if(!connected) 
    {
      if(!TargetBoardFeatures.LedStatus) 
      {
        if(HAL_GetTick()-StartTime > 1000)
        {
          LedOnTargetPlatform();
          TargetBoardFeatures.LedStatus =1;
          StartTime = HAL_GetTick();
        }
      } 
      else 
      {
        if(HAL_GetTick()-StartTime > 50)
        {
          LedOffTargetPlatform();
          TargetBoardFeatures.LedStatus =0;
          StartTime = HAL_GetTick();
		  /* Update the BLE advertise data and make the Board connectable */
		  // Note: calling the setConnectable() api for advertising here is to allow HCI transction
		  // over SPI to through without it failing with timout. Otherwise if it called too quickly
		  // aci_gap_set_discoverable() fails with "0x0C: Command disallowed" status
          if(set_connectable)
          {
             if(setConnectable() == BLE_STATUS_SUCCESS)
             {
               set_connectable = FALSE;
             }
          }
        }
      }
    }
    
    /* handle BLE event */
    if(HCI_ProcessEvent) 
    {
      HCI_ProcessEvent=0;
      hci_user_evt_proc();
    }
    
    /* Wait for Interrupt */
    vTaskDelay(1);    //__WFI();
  }

}


/**
* @brief  Function for initializing timers for sending the information to BLE:
*  - 1 for sending MotionFX/AR/CP and Acc/Gyro/Mag
*  - 1 for sending the Environmental info
* @param  None
* @retval None
*/
void InitTimers(void)
{
  uint32_t uwPrescalerValue;
  
  /* Timer Output Compare Configuration Structure declaration */
  TIM_OC_InitTypeDef sConfig;
  
  /* Compute the prescaler value to have TIM3 counter clock equal to 10 KHz */
  uwPrescalerValue = (uint32_t) ((SystemCoreClock / 10000) - 1); 
  
  /* Set TIM1 instance (Motion)*/
  /* Set TIM1 instance */
  TimCCHandle.Instance = TIM1;
  TimCCHandle.Init.Period        = 65535;
  TimCCHandle.Init.Prescaler     = uwPrescalerValue;
  TimCCHandle.Init.ClockDivision = 0;
  TimCCHandle.Init.CounterMode   = TIM_COUNTERMODE_UP;
  if(HAL_TIM_OC_Init(&TimCCHandle) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
  
  /* Configure the Output Compare channels */
  /* Common configuration for all channels */
  sConfig.OCMode     = TIM_OCMODE_TOGGLE;
  sConfig.OCPolarity = TIM_OCPOLARITY_LOW;
  
  /* Output Compare Toggle Mode configuration: Channel1 */
  sConfig.Pulse = uhCCR1_Val;
  if(HAL_TIM_OC_ConfigChannel(&TimCCHandle, &sConfig, TIM_CHANNEL_1) != HAL_OK)
  {
    /* Configuration Error */
    Error_Handler();
  }  
  
}

/** @brief Initialize the BlueNRG Stack
 * @param None
 * @retval None
 */
void Init_BlueNRG_Stack(void)
{
  uint16_t service_handle, dev_name_char_handle, appearance_char_handle;
  tBleStatus ret;
  uint8_t data_len_out;

  /* Initialize the BlueNRG HCI */
  //hci_init(APP_UserEvtRx, NULL);
  hci_init(HCI_Event_CB, NULL);

  /* we will let the BLE chip to use its Random MAC address */
#define CONFIG_DATA_RANDOM_ADDRESS          (0x80) /**< Stored static random address. Read-only. */
  ret = aci_hal_read_config_data(CONFIG_DATA_RANDOM_ADDRESS, &data_len_out, bdaddr);

  if(ret != BLE_STATUS_SUCCESS){
    STBOX1_PRINTF("\r\nReading  Random BD_ADDR failed\r\n");
    goto fail;
  }

  ret = aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET,
                                  CONFIG_DATA_PUBADDR_LEN,
                                  bdaddr);

  if(ret != BLE_STATUS_SUCCESS){
     STBOX1_PRINTF("\r\nSetting Public BD_ADDR failed\r\n");
     goto fail;
  }

  ret = aci_gatt_init();
  if(ret != BLE_STATUS_SUCCESS){
     STBOX1_PRINTF("\r\nGATT_Init failed\r\n");
     goto fail;
  }

  ret = aci_gap_init(GAP_PERIPHERAL_ROLE, 0, strlen(BoardName), &service_handle, &dev_name_char_handle, &appearance_char_handle);
  

  if(ret != BLE_STATUS_SUCCESS){
     STBOX1_PRINTF("\r\nGAP_Init failed\r\n");
     goto fail;
  }

  ret = aci_gatt_update_char_value(service_handle, dev_name_char_handle, 0,
                                   strlen(BoardName), (uint8_t *)BoardName);

  if(ret != BLE_STATUS_SUCCESS){
     STBOX1_PRINTF("\r\naci_gatt_update_char_value failed\r\n");
    goto fail;
  }

  ret = aci_gap_set_authentication_requirement(BONDING,
                                               MITM_PROTECTION_REQUIRED,
                                               SC_IS_SUPPORTED,
                                               KEYPRESS_IS_NOT_SUPPORTED,
                                               7, 
                                               16,
                                               USE_FIXED_PIN_FOR_PAIRING,
                                               123456,
                                               0x00);
  if (ret != BLE_STATUS_SUCCESS) {
     STBOX1_PRINTF("\r\nGAP setting Authentication failed\r\n");
     goto fail;
  }

  STBOX1_PRINTF("\r\nSERVER: BLE Stack Initialized \r\n"
         "\t\tBoardName= %s\r\n"
         "\t\tBoardMAC = %x:%x:%x:%x:%x:%x\r\n",
         BoardName,
         bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]);

  /* Set output power level */
  aci_hal_set_tx_power_level(1,4); /* -2,1 dBm */

  return;

fail:
  return;
}


/** @brief Initialize all the Custom BlueNRG services
* @param None
* @retval None
*/
void Init_BlueNRG_Custom_Services(void)
{
  int ret;

  memset(&m_iop_connection, 0, sizeof(m_iop_connection));
#if S3AI_FIRMWARE_IS_COLLECTION // Data collection mode
  // Add Data collection service
  ret = add_collection_service(&m_iop_connection);
  if(ret == BLE_STATUS_SUCCESS)
  {
    STLBLE_PRINTF("Collection Service added successfully\r\n");
  }
  else
  {
    STLBLE_PRINTF("\r\nError while adding Collection Service\r\n");
  }
#endif

#if S3AI_FIRMWARE_IS_RECOGNITION  
 //recognition mode
  ret = add_pme_service(&m_iop_connection);
  if(ret == BLE_STATUS_SUCCESS)
  {
    STLBLE_PRINTF("PME  Service added successfully\r\n");
  }
  else
  {
    STLBLE_PRINTF("\r\nError while adding PME Service \r\n");
  }
#endif
  ret = QAI_addDeviceInformationService();
  if(ret == BLE_STATUS_SUCCESS)
  {
    STLBLE_PRINTF("Config  Service W2ST added successfully\r\n");
  }
  else
  {
    STLBLE_PRINTF("\r\nError while adding Config Service W2ST\r\n");
  }

  

}


/**
* @brief  Output Compare callback in non blocking mode 
  * @param  TIM_HandleTypeDef *htim TIM OC handle
  * @retval None
  */
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
  uint32_t uhCapture=0;
  /* TIM1_CH1 toggling with frequency = 1Hz */
  if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1) {
    uhCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
    /* Set the Capture Compare Register value */
    __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_1, (uhCapture + STBOX1_UPDATE_LED_BATTERY));

    /* Because the Led Blink only if we are not connected to the board... */
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_BAT_EVENT)) {
      SendBatteryInfo=1;
    } else {
      BlinkLed =1;
    }
  }

#if 1 //def STBOX1_ENABLE_PRINTF
  /* TIM1_CH2 toggling with frequency = 200Hz */
  if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2) {
    uhCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);
    /* Set the Capture Compare Register value */
    __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_2, (uhCapture + STBOX1_UPDATE_VCOM));
    CDC_PeriodElapsedCallback();
  }
#endif /* STBOX1_ENABLE_PRINTF */
  
  /* TIM1_CH3 toggling with frequency = 2Hz */
  if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_3) {
    uhCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3);
    /* Set the Capture Compare Register value */
    __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_3, (uhCapture + STBOX1_UPDATE_ENV));
    SendEnv=1;
  }

  /* TIM1_CH4 toggling with frequency = 20Hz */
  if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_4) {
    uhCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4);
    /* Set the Capture Compare Register value */
    __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_4, (uhCapture + STBOX1_UPDATE_INV));
    SendAccGyroMag=1;
  }
}
#if 0 //redefined above for 4 channels
/**
* @brief  Output Compare callback in non blocking mode 
* @param  htim : TIM OC handle
* @retval None
*/
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
  uint32_t uhCapture=0;
  
  /* TIM1_CH1 toggling with frequency = 2Hz */
  if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
  {
    uhCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
    /* Set the Capture Compare Register value */
    __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_1, (uhCapture + uhCCR1_Val));
    SendEnv=1;
  }
}
#endif
#if 0 //not used in SensorTile Box
/**
* @brief  Period elapsed callback in non blocking mode for Environmental timer
* @param  htim : TIM handle
* @retval None
*/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if(htim == (&TimHandle)) 
  {
    CDC_TIM_PeriodElapsedCallback(htim);
  }
}
#endif
#if 0 // we use our own callback function
/** @brief HCI Transport layer user function
  * @param void *pData pointer to HCI event data
  * @retval None
  */
void APP_UserEvtRx(void *pData)
{
  uint32_t i;

  hci_spi_pckt *hci_pckt = (hci_spi_pckt *)pData;

  if(hci_pckt->type == HCI_EVENT_PKT) {
    hci_event_pckt *event_pckt = (hci_event_pckt*)hci_pckt->data;

    if(event_pckt->evt == EVT_LE_META_EVENT) {
      evt_le_meta_event *evt = (void *)event_pckt->data;

      for (i = 0; i < (sizeof(hci_le_meta_events_table)/sizeof(hci_le_meta_events_table_type)); i++) {
        if (evt->subevent == hci_le_meta_events_table[i].evt_code) {
          hci_le_meta_events_table[i].process((void *)evt->data);
        }
      }
    } else if(event_pckt->evt == EVT_VENDOR) {
      evt_blue_aci *blue_evt = (void*)event_pckt->data;        

      for (i = 0; i < (sizeof(hci_vendor_specific_events_table)/sizeof(hci_vendor_specific_events_table_type)); i++) {
        if (blue_evt->ecode == hci_vendor_specific_events_table[i].evt_code) {
          hci_vendor_specific_events_table[i].process((void *)blue_evt->data);
        }
      }
    } else {
      for (i = 0; i < (sizeof(hci_events_table)/sizeof(hci_events_table_type)); i++) {
        if (event_pckt->evt == hci_events_table[i].evt_code) {
          hci_events_table[i].process((void *)event_pckt->data);
        }
      }
    }
  }
}
#endif
#if 0 // we use our own custom services
/** @brief Initialize all the Custom BlueNRG services
 * @param None
 * @retval None
 */
static void Init_BlueNRG_Custom_Services(void)
{
  int ret;

  ret = Add_HW_SW_ServW2ST_Service();
  if(ret == BLE_STATUS_SUCCESS) {
     STBOX1_PRINTF("HW & SW Service W2ST added successfully\r\n");
  } else {
     STBOX1_PRINTF("\r\nError while adding HW & SW Service W2ST\r\n");
  }

  ret = Add_ConsoleW2ST_Service();
  if(ret == BLE_STATUS_SUCCESS) {
     STBOX1_PRINTF("Console Service W2ST added successfully\r\n");
  } else {
     STBOX1_PRINTF("\r\nError while adding Console Service W2ST\r\n");
  }

  ret = Add_ConfigW2ST_Service();
  if(ret == BLE_STATUS_SUCCESS) {
     STBOX1_PRINTF("Config  Service W2ST added successfully\r\n");
  } else {
     STBOX1_PRINTF("\r\nError while adding Config Service W2ST\r\n");
  }
}
#endif