/**
  ******************************************************************************
  * @file    sensor_service.c
  * @author  SRA - Central Labs
  * @version v2.1.0
  * @date    5-Apr-2019
  * @brief   Add bluetooth services using vendor specific profiles.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
#include <stdio.h>
#include "TargetFeatures.h"
#include "main.h"
#include "sensor_service.h"
#include "console.h"
#include "bluenrg_utils.h"
#include "bluenrg1_l2cap_aci.h"
#include "uuid_ble_service.h"
#include "qai_dev_params.h"
#include "ble_iop_messages.h"
#include "ble_collection.h"
//#include "sensor.h"
#include "ble_gatt.h"
#include "ble_pme.h"


//from SensorTile project uuid.h
#define DEVICE_INFORMATION_SERVICE_UUID (0x180A)
#define FIRMWARE_REVISION_UUID          (0x2A26)
#define MANUFACTURER_NAME_UUID          (0x2A29)


//from SensorTile project ble_profile.h
#define MAX_DIS_LEN			(9)
#define FIRMWARE_REVISION_STRING_LEN_MAX                  (32)
#define MANUFACTURER_NAME_STRING_LEN_MAX                  (32)



//from bluenrg1_gatt_aci.c
extern tBleStatus aci_gatt_add_service(uint8_t Service_UUID_Type,
                                Service_UUID_t *Service_UUID,
                                uint8_t Service_Type,
                                uint8_t Max_Attribute_Records,
                                uint16_t *Service_Handle);

extern tBleStatus aci_gatt_write_resp(uint16_t Connection_Handle,
                               uint16_t Attr_Handle,
                               uint8_t Write_status,
                               uint8_t Error_Code,
                               uint8_t Attribute_Val_Length,
                               uint8_t Attribute_Val[]);

/* Exported variables ---------------------------------------------------------*/
int connected = FALSE;
uint8_t set_connectable = TRUE;

/* Imported Variables -------------------------------------------------------------*/
extern uint32_t ConnectionBleStatus;

extern TIM_HandleTypeDef    TimCCHandle;

extern uint8_t bdaddr[6];

extern ble_iop_connection_t				m_iop_connection;  // Collection service and Device  Information service handles

/* Private variables ------------------------------------------------------------*/
//new for SensorTileBox
Service_UUID_t service_uuid;
Char_UUID_t char_uuid;
Char_Desc_Uuid_t char_desc_uuid;

//from SensorTile project mani.h
//#define DEVICE_NAME                     'S', 'e', 'n', 's', 'o', 'r', 'T', 'i', 'l', 'e'                           /**< Name of device. Will be included in the advertising data. */
#define DEVICE_MFG                      "ST"
#define BLE_FIRMWARE_REV                "5.0"

extern char BoardName[8];
//=======================================

static tDevInfoService qai_devInfoService;


static uint16_t connection_handle = 0;  // /**< Connection handle for the active link. */

/* Private functions ------------------------------------------------------------*/
static void GAP_ConnectionComplete_CB(uint8_t addr[6], uint16_t handle);
static void GAP_DisconnectionComplete_CB(void);
tBleStatus add_collection_service(ble_iop_connection_t *m_iop_connection);

/* Private define ------------------------------------------------------------*/

#ifdef ACC_BLUENRG_CONGESTION
#define ACI_GATT_UPDATE_CHAR_VALUE safe_aci_gatt_update_char_value
static int32_t breath;


/* @brief  Update the value of a characteristic avoiding (for a short time) to
 *         send the next updates if an error in the previous sending has
 *         occurred.
 * @param  servHandle The handle of the service
 * @param  charHandle The handle of the characteristic
 * @param  charValOffset The offset of the characteristic
 * @param  charValueLen The length of the characteristic
 * @param  charValue The pointer to the characteristic
 * @retval tBleStatus Status
 */
tBleStatus safe_aci_gatt_update_char_value(uint16_t servHandle,
                                           uint16_t charHandle,
                                           uint8_t charValOffset,
                                           uint8_t charValueLen,
                                           uint8_t *charValue)
{
  tBleStatus ret = BLE_STATUS_INSUFFICIENT_RESOURCES;

  if (breath > 0)
  {
    breath--;
  }
  else
  {
    ret = aci_gatt_update_char_value(servHandle,charHandle,charValOffset,charValueLen,charValue);

    if (ret != BLE_STATUS_SUCCESS)
    {
      breath = ACC_BLUENRG_CONGESTION_SKIP;
    }
  }

  return (ret);
}

#else /* ACC_BLUENRG_CONGESTION */
#define ACI_GATT_UPDATE_CHAR_VALUE aci_gatt_update_char_value
#endif /* ACC_BLUENRG_CONGESTION */


/**
* @brief  Add the Collection service using a vendor specific profile
* @param  None
* @retval tBleStatus Status
*/
tBleStatus add_collection_service(ble_iop_connection_t *m_iop_connection)
{
  tBleStatus ret;

  uint8_t uuid[16];
  int32_t NumberChars = 6;
  uint16_t handle = 0;
  // Add user description
  uint16_t descUUID = CHAR_USER_DESC_UUID;
  char buf[8] = "RAW-Dat";
  uint16_t charUsrDesc = 0;

  COLLECTION_SERVICE_UUID(uuid);
  BLUENRG_memcpy(&service_uuid.Service_UUID_128, uuid, 16);
  ret = aci_gatt_add_service(UUID_TYPE_128, &service_uuid, PRIMARY_SERVICE,
                          1+3*NumberChars,
                          &handle);

  m_iop_connection->m_collection.service_handle =  handle;

  if (ret != BLE_STATUS_SUCCESS)
  {
    goto fail;
  }
  STLBLE_PRINTF("\n m_iop_connection->m_collection.service_handle =%d", m_iop_connection->m_collection.service_handle);

  COLLECTION_CONFIG_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(m_iop_connection->m_collection.service_handle, UUID_TYPE_128, &char_uuid, 20 /* Max Dimension */,
                           CHAR_PROP_READ | CHAR_PROP_WRITE | CHAR_PROP_NOTIFY|CHAR_PROP_INDICATE,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP
                             |GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP,
                           MIN_ENCRYPTION_KEY_SIZE, 1, &m_iop_connection->m_collection.config_handles);

  if (ret != BLE_STATUS_SUCCESS)
  {
    goto fail;
  }
  STLBLE_PRINTF("\n m_collection.config_handles =%d", m_iop_connection->m_collection.config_handles);

  memcpy(buf, "Conf-hdl", sizeof(buf));
  //memset(buf, 0x0, sizeof(buf));
  descUUID = CHAR_USER_DESC_UUID;

  /* add the valid descriptor */
  char_desc_uuid.Char_UUID_16 = descUUID;
   ret = aci_gatt_add_char_desc(m_iop_connection->m_collection.service_handle,
                                         m_iop_connection->m_collection.config_handles,
                                         UUID_TYPE_16,
                                         &char_desc_uuid,
                                          5,
                                          5,
                                          (void *)&buf, //TBR
                                          ATTR_PERMISSION_NONE,
                                          ATTR_ACCESS_READ_ONLY | ATTR_ACCESS_READ_WRITE,
                                          GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP
                                            | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                                          MIN_ENCRYPTION_KEY_SIZE,
                                          0x00,
                                          &charUsrDesc);
   if (ret != BLE_STATUS_SUCCESS)
   {
      goto fail;
   }


  COLLECTION_FILE_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(m_iop_connection->m_collection.service_handle, UUID_TYPE_128, &char_uuid, 20 /* Max Dimension */,
                           CHAR_PROP_READ | CHAR_PROP_WRITE | CHAR_PROP_NOTIFY ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP
                              |GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &m_iop_connection->m_collection.filename_handles);

  if (ret != BLE_STATUS_SUCCESS)
  {
    goto fail;
  }

  memcpy(buf, "file-hdl", sizeof(buf));
  descUUID = CHAR_USER_DESC_UUID;

  /* add the valid descriptor */
  char_desc_uuid.Char_UUID_16 = descUUID;
   ret = aci_gatt_add_char_desc(m_iop_connection->m_collection.service_handle,
                                         m_iop_connection->m_collection.filename_handles,
                                         UUID_TYPE_16,
                                         &char_desc_uuid,
                                          8,
                                          8,
                                          (void *)&buf, //TBR
                                          ATTR_PERMISSION_NONE,
                                          ATTR_ACCESS_READ_ONLY,
                                          GATT_DONT_NOTIFY_EVENTS,
                                          MIN_ENCRYPTION_KEY_SIZE,
                                          0x00,
                                          &charUsrDesc);
   if (ret != BLE_STATUS_SUCCESS)
   {
      goto fail;
   }

   COLLECTION_RAW_CHAR_UUID(uuid);
   BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
   ret =  aci_gatt_add_char(m_iop_connection->m_collection.service_handle, UUID_TYPE_128, &char_uuid, 20 /* Max Dimension */,
                           CHAR_PROP_READ | CHAR_PROP_NOTIFY ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &m_iop_connection->m_collection.raw_handles);

  if (ret != BLE_STATUS_SUCCESS)
  {
    goto fail;
  }


  descUUID = CHAR_USER_DESC_UUID;
  memcpy(buf, "raw-dat", sizeof(buf));

  /* add the valid descriptor */
  char_desc_uuid.Char_UUID_16 = descUUID;
  ret = aci_gatt_add_char_desc(m_iop_connection->m_collection.service_handle,
                                         m_iop_connection->m_collection.raw_handles,
                                         UUID_TYPE_16,
                                         &char_desc_uuid,
                                          8,
                                          8,
                                          (void *)&buf, //TBR
                                          ATTR_PERMISSION_NONE,
                                          ATTR_ACCESS_READ_ONLY,
                                          GATT_DONT_NOTIFY_EVENTS,
                                          MIN_ENCRYPTION_KEY_SIZE,
                                          0x00,
                                          &charUsrDesc);
   if (ret != BLE_STATUS_SUCCESS)
   {
      goto fail;
   }

  return BLE_STATUS_SUCCESS;

fail:
  //STLBLE_PRINTF("Error while adding Configuration service.\n");
  return BLE_STATUS_ERROR;
}


/**
* @brief  Add the PME service using a vendor specific profile
* @param  None
* @retval tBleStatus Status
*/
tBleStatus add_pme_service(ble_iop_connection_t *m_iop_connection)
{
  tBleStatus ret;

  uint8_t uuid[16];
  int32_t NumberChars = 4;
  uint16_t handle = 0;
  // Add user description
  //uint16_t descUUID = CHAR_USER_DESC_UUID;
  //char buf[8] = "PME-RES";
  //uint16_t charUsrDesc = 0;

  // Initialize the service structure.
  m_iop_connection->m_pme.conn_handle = BLE_CONN_HANDLE_INVALID;
  m_iop_connection->m_pme.evt_handler = ble_pme_evt_handler;

  // Add PME service
  PME_SERVICE_UUID(uuid);
  BLUENRG_memcpy(&service_uuid.Service_UUID_128, uuid, 16);
  ret = aci_gatt_add_service(UUID_TYPE_128, &service_uuid, PRIMARY_SERVICE,
                          1+3*NumberChars,
                          &handle);
  if (ret != BLE_STATUS_SUCCESS)
  {
    goto fail;
  }

  m_iop_connection->m_pme.service_handle =  handle;
  STLBLE_PRINTF("\n m_iop_connection->pme.service_handle =%d", m_iop_connection->m_pme.service_handle);

  PME_PME_RESULTS_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(m_iop_connection->m_pme.service_handle, UUID_TYPE_128, &char_uuid, 2*sizeof(ble_pme_result_t),
                           CHAR_PROP_NOTIFY,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP
                             |GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP,
                           MIN_ENCRYPTION_KEY_SIZE, 1, &m_iop_connection->m_pme.pme_result_handle);

  if (ret != BLE_STATUS_SUCCESS)
  {
    goto fail;
  }

  PME_FEAT_VEC_RESULTS_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(m_iop_connection->m_pme.service_handle, UUID_TYPE_128, &char_uuid, sizeof(ble_pme_result_w_fv_t),
                           CHAR_PROP_NOTIFY ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP
                              |GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &m_iop_connection->m_pme.feat_vector_handle);

  if (ret != BLE_STATUS_SUCCESS)
  {
    goto fail;
  }

//  memcpy(buf, "feat-vec", sizeof(buf));
//  descUUID = CHAR_USER_DESC_UUID;
//
//  /* add the valid descriptor */
//   ret = aci_gatt_add_char_desc(m_iop_connection->m_pme.service_handle,
//                                         m_iop_connection->m_pme.feat_vector_handle,
//                                         UUID_TYPE_16,
//                                         (uint8_t *)&descUUID,
//                                          8,
//                                          8,
//                                          (void *)&buf, //TBR
//                                          ATTR_PERMISSION_NONE,
//                                          ATTR_ACCESS_READ_ONLY,
//                                          GATT_DONT_NOTIFY_EVENTS,
//                                          MIN_ENCRYPTION_KEY_SIZE,
//                                          0x00,
//                                          &charUsrDesc);
//   if (ret != BLE_STATUS_SUCCESS)
//   {
//     while(1);
//      goto fail;
//   }

   return BLE_STATUS_SUCCESS;

fail:
  STLBLE_PRINTF("Error while adding PME service.\n");
  return BLE_STATUS_ERROR;
}


tBleStatus Add_DIS_Charac()
{
  tBleStatus ret;
  //uint8_t uuid[2];
   // Add user description
  uint16_t descUUID = CHAR_USER_DESC_UUID;
  char buf[] = BLE_FIRMWARE_REV;
  uint16_t charUsrDesc = 0;

   {
    //HOST_TO_LE_16(uuid,FIRMWARE_REVISION_UUID);
    char_uuid.Char_UUID_16 = FIRMWARE_REVISION_UUID;
    ret = aci_gatt_add_char(qai_devInfoService.devInfoServHandle,
                            UUID_TYPE_16,
                            &char_uuid,
                            FIRMWARE_REVISION_STRING_LEN_MAX,
                            CHAR_PROP_READ,
                            ATTR_PERMISSION_NONE,
                            GATT_DONT_NOTIFY_EVENTS,
                            10,
                            CHAR_VALUE_LEN_VARIABLE,
                            &qai_devInfoService.firmwareRevisionCharHandle);
    if(ret != BLE_STATUS_SUCCESS)
      goto ADD_DIS_CHARAC_FAIL;

	STLBLE_PRINTF("4 - DIS] firmware revision Handle: %04X !!\n",
                     qai_devInfoService.firmwareRevisionCharHandle);
  }

    descUUID = CHAR_USER_DESC_UUID;

  /* add the valid descriptor */
   char_desc_uuid.Char_UUID_16 = descUUID;
   ret = aci_gatt_add_char_desc(qai_devInfoService.devInfoServHandle,
                                         qai_devInfoService.firmwareRevisionCharHandle,
                                         UUID_TYPE_16,
                                         &char_desc_uuid,
                                          8,
                                          8,
                                          (void *)&buf, //TBR
                                          ATTR_PERMISSION_NONE,
                                          ATTR_ACCESS_READ_ONLY,
                                          GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP
                                            | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                                          MIN_ENCRYPTION_KEY_SIZE,
                                          0x00,
                                          &charUsrDesc);
   if (ret != BLE_STATUS_SUCCESS)
   {
      goto ADD_DIS_CHARAC_FAIL;
   }


    //HOST_TO_LE_16(uuid,MANUFACTURER_NAME_UUID);
    char_uuid.Char_UUID_16 = MANUFACTURER_NAME_UUID;
    ret = aci_gatt_add_char(qai_devInfoService.devInfoServHandle,
                            UUID_TYPE_16,
                            &char_uuid,
                            MANUFACTURER_NAME_STRING_LEN_MAX,
                            CHAR_PROP_READ,
                            ATTR_PERMISSION_NONE,
                            GATT_DONT_NOTIFY_EVENTS,
                            10,
                            CHAR_VALUE_LEN_VARIABLE,
                            &qai_devInfoService.manufacturerNameCharHandle);
    if(ret != BLE_STATUS_SUCCESS)
      goto ADD_DIS_CHARAC_FAIL;

	STLBLE_PRINTF("7 -[DIS] manufacturer name Handle: %04X !!\n",
                     qai_devInfoService.manufacturerNameCharHandle);

    descUUID = CHAR_USER_DESC_UUID;

    memcpy(buf, DEVICE_MFG, sizeof(DEVICE_MFG));
  /* add the valid descriptor */
   char_desc_uuid.Char_UUID_16 = descUUID;
   ret = aci_gatt_add_char_desc(qai_devInfoService.devInfoServHandle,
                                         qai_devInfoService.manufacturerNameCharHandle,
                                         UUID_TYPE_16,
                                         &char_desc_uuid,
                                          10,
                                          10,
                                          (void *)&buf, //TBR
                                          ATTR_PERMISSION_NONE,
                                          ATTR_ACCESS_READ_ONLY,
                                          GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP
                                            | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                                          MIN_ENCRYPTION_KEY_SIZE,
                                          0x00,
                                          &charUsrDesc);
   if (ret != BLE_STATUS_SUCCESS)
   {
      goto ADD_DIS_CHARAC_FAIL;
   }

  return BLE_STATUS_SUCCESS;

ADD_DIS_CHARAC_FAIL:
  STLBLE_PRINTF("[DIS] Add_DIS_Charac() Error while adding characteristics.\n");
  return ret;
}

/**
* @brief  Add the Collection service using a vendor specific profile
* @param  None
* @retval tBleStatus Status
*/
tBleStatus QAI_addDeviceInformationService(void)
{
  tBleStatus ret;
  /* we have to add the device information service */
  //uint8_t uuid[2];
  //HOST_TO_LE_16(uuid,DEVICE_INFORMATION_SERVICE_UUID);

  service_uuid.Service_UUID_16 = DEVICE_INFORMATION_SERVICE_UUID;
  ret = aci_gatt_add_service(UUID_TYPE_16, &service_uuid, PRIMARY_SERVICE,
                            1 + 3*MAX_DIS_LEN, &qai_devInfoService.devInfoServHandle);
  if(ret!=BLE_STATUS_SUCCESS)
      goto fail;
  STLBLE_PRINTF("Device Information Service is added successfully \n");

  STLBLE_PRINTF("Device Information Service Handle: %04X\n",
                      qai_devInfoService.devInfoServHandle);

    ret = Add_DIS_Charac();
    if(ret!=BLE_STATUS_SUCCESS)
      goto fail;
    STLBLE_PRINTF ("Device Information characteristics added successfully \n");

  return BLE_STATUS_SUCCESS;

  fail:
  STLBLE_PRINTF("Error while adding Device Information service.\n");
  return BLE_STATUS_ERROR;
}

/**
 * @brief  Puts the device in connectable mode.
 * @param  None
 * @retval None
 */
int setConnectable(void)
{
  uint8_t local_name[8] = {AD_TYPE_COMPLETE_LOCAL_NAME,BoardName[0],BoardName[1],BoardName[2],BoardName[3],BoardName[4],BoardName[5],BoardName[6]};
#if 0
    uint8_t manuf_data[26] = {
    2,0x0A,0x00 /* 0 dBm */, // Transmission Power
    8,0x09,BoardName[0],BoardName[1],BoardName[2],BoardName[3],BoardName[4],BoardName[5],BoardName[6], // Complete Name
    13,0xFF,0x01/*SKD version */,
    0x06, /* SensorTile.box */
    0x00 /* AudioSync+AudioData */,
    0xE0 /* ACC+Gyro+Mag*/           | 
      0x04 /* Temperature value */   |
      0x02 /* Battery Present */     |
      0x08 /* Humidity */            |
      0x10 /* Pressure value*/       ,
    0x00 /*  */,
    0x00, /*  */
    0x00, /* BLE MAC start */
    0x00,
    0x00,
    0x00,
    0x00,
    0x00, /* BLE MAC stop */
  };
#endif
  tBleStatus RetStatus;
#if 0
  /* BLE MAC */
  manuf_data[20] = bdaddr[5];
  manuf_data[21] = bdaddr[4];
  manuf_data[22] = bdaddr[3];
  manuf_data[23] = bdaddr[2];
  manuf_data[24] = bdaddr[1];
  manuf_data[25] = bdaddr[0];
#endif
  /* disable scan response */
  RetStatus = hci_le_set_scan_response_data(0,NULL);
  if(RetStatus !=BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error hci_le_set_scan_response_data [%x]\r\n",RetStatus);
    goto EndLabel;
  }

  // we use this for QAI, But they are not min and max as required
  RetStatus = aci_gap_set_discoverable(ADV_IND, APP_ADV_INTERVAL, APP_ADV_INTERVAL,
  //RetStatus = aci_gap_set_discoverable(ADV_IND, 0, 0,
                           RANDOM_ADDR,
                           NO_WHITE_LIST_USE,
                           sizeof(local_name), local_name, 0, NULL, 0, 0);
  if(RetStatus !=BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error aci_gap_set_discoverable [%x]\r\n",RetStatus);
  } else {
    STBOX1_PRINTF("aci_gap_set_discoverable OK\r\n");
  }

#if 0 // Note: We don't need to update advertising data for now so commenting the follwing lines
  /* Send Advertising data */
  RetStatus = aci_gap_update_adv_data(26, manuf_data);
  if(RetStatus !=BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error aci_gap_update_adv_data [%x]\r\n",RetStatus);
  } else {
    STBOX1_PRINTF("aci_gap_update_adv_data OK\r\n");
}
#endif

EndLabel:
  return RetStatus;
}

/**
 * @brief  This function is called when there is a LE Connection Complete event.
 * @param  uint8_t addr[6] Address of peer device
 * @param  uint16_t handle Connection handle
 * @retval None
 */
static void GAP_ConnectionComplete_CB(uint8_t addr[6], uint16_t handle)
{
  connected = TRUE;
  connection_handle = handle;

#ifdef ENABLE_USB_DEBUG_CONNECTION
  STLBLE_PRINTF(">>>>>>CONNECTED %x:%x:%x:%x:%x:%x\r\n",addr[5],addr[4],addr[3],addr[2],addr[1],addr[0]);
#endif /* ENABLE_USB_DEBUG_CONNECTION */

  ConnectionBleStatus=0;

}

/**
 * @brief  This function is called when the peer device get disconnected.
 * @param  None
 * @retval None
 */
static void GAP_DisconnectionComplete_CB(void)
{
  connected = FALSE;

#ifdef ENABLE_USB_DEBUG_CONNECTION
  STLBLE_PRINTF("<<<<<<DISCONNECTED\r\n");
#endif /* ENABLE_USB_DEBUG_CONNECTION */

  /* Make the device connectable again. */
  set_connectable = TRUE;

  ConnectionBleStatus=0;

  if(HAL_TIM_OC_Stop_IT(&TimCCHandle, TIM_CHANNEL_1) != HAL_OK){
    /* Stopping Error */
    Error_Handler();
  }

  // Enable the advertising will be done in the main BLE task
  //setConnectable(); //) // if failed to advertise try again
  //{
  //  set_connectable = TRUE; 
  //}
}

/**
* @brief  This function is called when there is a Bluetooth Read request
* @param  uint16_t handle Handle of the attribute
* @retval None
*/
void Read_Request_CB(uint16_t handle)
{

  //EXIT:
  if(connection_handle != 0)
    aci_gatt_allow_read(connection_handle);
}
void Attribute_Modified_Request_CB(uint16_t Connection_Handle,
                                       uint16_t Attr_Handle,
                                       uint16_t Offset,
                                       uint16_t Attr_Data_Length,
                                       uint8_t Attr_Data[])
{
    STLBLE_PRINTF("\n ATTRIBUTE_MODIFIED *** Attr_Handle=%d, len=%d", Attr_Handle, Attr_Data_Length);

    if((Attr_Handle == m_iop_connection.m_pme.pme_result_handle + 2)
        || (Attr_Handle == m_iop_connection.m_pme.feat_vector_handle + 2))
     // PME results handle
  {
       ble_pme_on_write(&m_iop_connection.m_pme, Attr_Handle, Attr_Data_Length, Attr_Data);
  }
    else if( (Attr_Handle == m_iop_connection.m_collection.config_handles + 2) ||
             (Attr_Handle == m_iop_connection.m_collection.raw_handles + 2) )
    {
         ble_motion_on_write(&m_iop_connection.m_collection,  Attr_Handle, Attr_Data_Length, Attr_Data);
    }

    }

/* ***************** BlueNRG-1 Stack Callbacks ********************************/

/*******************************************************************************
 * Function Name  : hci_le_connection_complete_event.
 * Description    : This event indicates that a new connection has been created.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_le_connection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Role,
                                      uint8_t Peer_Address_Type,
                                      uint8_t Peer_Address[6],
                                      uint16_t Conn_Interval,
                                      uint16_t Conn_Latency,
                                      uint16_t Supervision_Timeout,
                                      uint8_t Master_Clock_Accuracy)
    {
#if 0  
  connection_handle = Connection_Handle;

  STBOX1_PRINTF(">>>>>>CONNECTED %x:%x:%x:%x:%x:%x\r\n",Peer_Address[5],Peer_Address[4],Peer_Address[3],Peer_Address[2],Peer_Address[1],Peer_Address[0]);

  ConnectionBleStatus=0;

  /* Stop the TIM Base generation in interrupt mode for Led Blinking*/
  if(HAL_TIM_OC_Stop_IT(&TimCCHandle, TIM_CHANNEL_1) != HAL_OK){
    /* Stopping Error */
    Error_Handler();
    }

  /* Just in order to be sure to switch off the User Led */
  LedOffTargetPlatform();

  HAL_Delay(100);
#endif  

  GAP_ConnectionComplete_CB(Peer_Address, Connection_Handle);
  if (Status == BLE_STATUS_SUCCESS)
          {
            /* store connection handle */
    m_iop_connection.m_collection.conn_handle = Connection_Handle;
    m_iop_connection.m_pme.conn_handle = Connection_Handle;
    STLBLE_PRINTF("HCI_Event_CB() EVT_LE_CONN_COMPLETE %d handle\n",Connection_Handle);
          }



}/* end hci_le_connection_complete_event() */

/*******************************************************************************
 * Function Name  : hci_disconnection_complete_event.
 * Description    : This event occurs when a connection is terminated.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_disconnection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Reason)
    {
#if 0
  /* No Device Connected */
  connection_handle =0;

  STBOX1_PRINTF("<<<<<<DISCONNECTED\r\n");

  /* Make the device connectable again. */
  set_connectable = TRUE;
  ConnectionBleStatus=0;
          
  HAL_Delay(100);
#endif

  ble_collection_toggle(false);
  GAP_DisconnectionComplete_CB();
  m_iop_connection.m_collection.is_raw_notif_enabled = false;
  m_iop_connection.m_collection.is_command_config_notif_en = false;
  STLBLE_PRINTF("HCI_Event_CB()  \n\nDISCONNECTED\r\n");


}/* end hci_disconnection_complete_event() */

/*******************************************************************************
 * Function Name  : aci_gatt_read_permit_req_event.
 * Description    : This event is given when a read request is received
 *                  by the server from the client.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_read_permit_req_event(uint16_t Connection_Handle,
                                    uint16_t Attribute_Handle,
                                    uint16_t Offset)
{
  Read_Request_CB(Attribute_Handle);    
}

/*******************************************************************************
 * Function Name  : aci_gatt_attribute_modified_event.
 * Description    : This event is given when an attribute change his value.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_attribute_modified_event(uint16_t Connection_Handle,
                                       uint16_t Attr_Handle,
                                       uint16_t Offset,
                                       uint16_t Attr_Data_Length,
                                       uint8_t Attr_Data[])
{
  Attribute_Modified_Request_CB(Connection_Handle, Attr_Handle, Offset, Attr_Data_Length, Attr_Data);
}
//ecode = 0x0c13 
void aci_gatt_write_permit_req_event(uint16_t Connection_Handle,
                                     uint16_t Attribute_Handle,
                                     uint8_t Data_Length,
                                     uint8_t Data[])
        {
  uint8_t attValLen;
  tBleStatus retval = BLE_STATUS_SUCCESS;
  uint16_t attrHandle;

  attrHandle = Attribute_Handle;
  attValLen = Data_Length; 

          STLBLE_PRINTF ("HT_Event_Handler(), EVT_BLUE_GATT_WRITE_PERMIT_REQ, Handle: 0x%02X, Len: 0x%02X!!\n", attrHandle, attValLen);

          if (attrHandle== m_iop_connection.m_collection.config_handles + 1)
		  {
              STLBLE_PRINTF("HCI_Event_CB(),EVT_BLUE_GATT_WRITE_PERMIT_REQ, valid value\n");
              retval = aci_gatt_write_resp(m_iop_connection.m_collection.conn_handle,
                                               attrHandle,
                                               BLE_STATUS_SUCCESS,
                                               0x00,
                                               attValLen,
                                       Data);
            if(retval == BLE_STATUS_SUCCESS)
            {
              STLBLE_PRINTF("HT_Event_Handler(),HCI_GATT_Write_Response_Cmd OK-1\n");
            }
            else
            {
              STLBLE_PRINTF("HT_Event_Handler(),FAILED HCI_GATT_Write_Response_Cmd %02X \n", retval);
            }
    if(Data[0] == 0xF7) // @TODO Indra ask Justin why 0xF7 is coming as set set file name
            {
        ble_collection_data_handler(&m_iop_connection.m_collection, attrHandle, BLE_COLLECTION_EVT_FILE_NAME, attValLen, Data);
            }
            else
            {
        ble_collection_data_handler(&m_iop_connection.m_collection, attrHandle, BLE_COLLECTION_EVT_CONFIG_RECEIVED, attValLen, Data);
            }
          }
          else if(attrHandle == m_iop_connection.m_collection.filename_handles + 1)
          {
            if (attValLen > MAX_FILE_PREFIX_LEN)
			{
				retval = aci_gatt_write_resp(m_iop_connection.m_collection.conn_handle,
                                               attrHandle,
                                               0x01, /* write_status = 1 (error))*/ // BLE_STATUS_FAILED
                                               BLE_GATT_STATUS_ATTERR_WRITE_NOT_PERMITTED,
                                               attValLen,
                                       Data);


			}
			else
			{
				STLBLE_PRINTF("HT_Event_Handler(),EVT_BLUE_GATT_WRITE_PERMIT_REQ, valid value\n");
                retval = aci_gatt_write_resp(m_iop_connection.m_collection.conn_handle,
                                               attrHandle,
                                               BLE_STATUS_SUCCESS,
                                               0x00,
                                               attValLen,
                                       Data);
			}
            if(retval == BLE_STATUS_SUCCESS)
            {
              STLBLE_PRINTF("HT_Event_Handler(),HCI_GATT_Write_Response_Cmd OK-3\n");
            }
            else
            {
                STLBLE_PRINTF("HT_Event_Handler(),FAILED HCI_GATT_Write_Response_Cmd %02X \n", retval);
                // Retry one more time
                retval = aci_gatt_write_resp(m_iop_connection.m_collection.conn_handle,
                                               attrHandle,
                                               BLE_STATUS_SUCCESS,
                                               0x00,
                                               attValLen,
                                       Data);
            }

    ble_collection_data_handler(&m_iop_connection.m_collection, attrHandle, BLE_COLLECTION_EVT_FILE_NAME, attValLen, Data);
          } //if(attrHandle == m_iop_connection.m_collection.filename_handles + 1)





  
        }

//ecode = 0x0c18 
void aci_gatt_prepare_write_permit_req_event(uint16_t Connection_Handle,
                                             uint16_t Attribute_Handle,
                                             uint16_t Offset,
                                             uint8_t Data_Length,
                                             uint8_t Data[])
        {

          bool valid_data = true;
          uint8_t status = 0;
          uint8_t errorCode = 0;
          tBleStatus retval;

  STLBLE_PRINTF("\n EVT_BLUE_GATT_PREPARE_WRITE_PERMIT_REQ == Attribute_Handle=%d, len=%d", Attribute_Handle, Data_Length );
  if (Attribute_Handle == m_iop_connection.m_collection.filename_handles + 1)
		  {
      if (Data_Length > MAX_FILE_PREFIX_LEN)
			  {
				  valid_data = false;
                  status = 0x01;  //// The value can be written to the attribute specified by attr_handle
                  errorCode = BLE_GATT_STATUS_SUCCESS;
			  }
			  else
			  {
				  valid_data = true;
                  status = 0x0;  //* write_status = 1 (error))*/ // BLE_STATUS_FAILED
                  errorCode = BLE_GATT_STATUS_ATTERR_WRITE_NOT_PERMITTED;
			  }

              retval = aci_gatt_write_resp(m_iop_connection.m_collection.service_handle,
                                       Attribute_Handle,
                                               status, // The value can be written to the attribute specified by attr_handle\n, /* write_status = 1 (error))*/ // BLE_STATUS_FAILED
                                               errorCode,
                                       Data_Length,
                                       Data);
            if(retval != 0)
            {
              STLBLE_PRINTF("\nHCI_Event_CB()-aci_gatt_write_resp() failed\n");
      //while(1);
            }

			//APP_ERROR_CHECK(err_code);

			if (valid_data )
            {
                STLBLE_PRINTF("\nBLE_COLLECTION_EVT_FILE_NAME-5");
        ble_collection_data_handler(&m_iop_connection.m_collection, Attribute_Handle, BLE_COLLECTION_EVT_FILE_NAME,  Data_Length, Data);
    }
            }
          }

/** @brief HCI Transport layer user function
  * @param void *pData pointer to HCI event data
  * @retval None
  */
//void APP_UserEvtRx(void *pData)
void HCI_Event_CB(void *pData)
{
  uint32_t i;

  hci_spi_pckt *hci_pckt = (hci_spi_pckt *)pData;

  if(hci_pckt->type == HCI_EVENT_PKT) {
    hci_event_pckt *event_pckt = (hci_event_pckt*)hci_pckt->data;

    if(event_pckt->evt == EVT_LE_META_EVENT) {
      evt_le_meta_event *evt = (void *)event_pckt->data;

        for (i = 0; i < (sizeof(hci_le_meta_events_table)/sizeof(hci_le_meta_events_table_type)); i++) {
          if (evt->subevent == hci_le_meta_events_table[i].evt_code) {
            hci_le_meta_events_table[i].process((void *)evt->data);
          }
        }
    } else if(event_pckt->evt == EVT_VENDOR) {
      evt_blue_aci *blue_evt = (void*)event_pckt->data;        

      for (i = 0; i < (sizeof(hci_vendor_specific_events_table)/sizeof(hci_vendor_specific_events_table_type)); i++) {
        if (blue_evt->ecode == hci_vendor_specific_events_table[i].evt_code) {
          hci_vendor_specific_events_table[i].process((void *)blue_evt->data);
        }
      }
    } else {
      for (i = 0; i < (sizeof(hci_events_table)/sizeof(hci_events_table_type)); i++) {
        if (event_pckt->evt == hci_events_table[i].evt_code) {
          hci_events_table[i].process((void *)event_pckt->data);
        }
      }
    }
  }
}

/******************* (C) COPYRIGHT 2016 STMicroelectronics *****END OF FILE****/
