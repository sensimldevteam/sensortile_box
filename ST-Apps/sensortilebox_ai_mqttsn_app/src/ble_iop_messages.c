
#include "Fw_global_config.h"
#include "main.h"
#include "iop_messages.h"
#include "ble_iop_messages.h"
//#include "ble_collection.h"
#include "ble_pme.h"
#include "DataCollection.h"
#include "ble_pme_defs.h"
#include "dbg_uart.h"


#define APP_TIMER_PRESCALER             0                                           /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE         4                                           /**< Size of timer operation queues. */
#define SPI_WRITE_TIMEOUT_VAL           APP_TIMER_MIN_TIMEOUT_TICKS //=5ms

// To be on safer side keeping it 64 bytes  ideally most of the command message at current implementaion should be 20 byte  iop_msg_filename_t is one exception
#define DCL_MAX_CMD_MSG_SIZE            64           

static uint8_t dclMsg[(DCL_MAX_CMD_MSG_SIZE + 3)];

extern ble_iop_connection_t				m_iop_connection;
extern uint8_t *my2_ble_callback( uint8_t *pData );

//#define configASSERT1( x ) if( ( x ) == 0 ) { STLBLE_PRINTF("configASSERT1 File:%s, Line:%d\n",__FILE__, __LINE__ ); }

void ble_iop_msg_parse(uint8_t * data)
{
    if(data == NULL)
    {
        STLBLE_PRINTF("ble_iop_msg_parse(): NULL Error");
        return ;
    }
    uint8_t len = data[0];
    uint8_t * p_data = &data[0];
    iop_msg_header_t *header = (iop_msg_header_t *)&data[1];
    iop_msg_pme_result_t * p_result;
    iop_msg_pme_fv_result_t * p_fv_res;
    ble_motion_raw_t *p_raw_motion;
    uint8_t sdf[20];
    memset(sdf, 0, 20);

    #if QUICKAI_UART_DEBUG
    dbg_str_int("IOP rx L\n",len);
    dbg_str_int("CMD: %d \n", header->cmd);
    #endif

    if (len < 2)
    {
        return;
    }

    switch(header->cmd)
    {
        case IOP_MSG_IMU_DATA:
            for(int i = 2; i < len;)
            {
                p_raw_motion = (ble_motion_raw_t*)&p_data[i];
                ble_motion_raw_set(&m_iop_connection.m_collection, p_raw_motion);
                i += sizeof(ble_motion_raw_t) + 1;
            }
            break;
           
        case IOP_MSG_RECO_CLASSIFICATION_DATA:
            p_result = (iop_msg_pme_result_t *)&data[1];
            ble_pme_result_t pme_results;
            memcpy(&pme_results, &p_result->pme_results, sizeof(ble_pme_result_t));
            ble_pme_results_set(&m_iop_connection.m_pme, &pme_results);
        break;

        case IOP_MSG_RECO_FEATURE_DATA:
            p_fv_res = ( iop_msg_pme_fv_result_t *)&data[1];
            ble_pme_result_w_fv_t pme_fv_results;
            memcpy(&pme_fv_results, &p_fv_res->pme_fv_results, sizeof(ble_pme_result_w_fv_t));
            #if QUICKAI_UART_DEBUG
            dbg_strhex32("model: ", p_fv_res->pme_fv_results.context);
            dbg_strhex32("\nclass: ", p_fv_res->pme_fv_results.classification);
            #endif
            ble_pme_feature_vector_set(&m_iop_connection.m_pme, &pme_fv_results);
            break;

        default:
            break;
    }
    return;
}

void iop_send_filename(char* filename, uint16_t length)
{
    if((filename == NULL) || (length == 0) || (length>IOP_MSG_FILENAME_LENGTH))
    {
        STLBLE_PRINTF("iop_send_filename(): Param error");
        return;
    }
    iop_msg_filename_t msg;

    msg.header.cmd = IOP_MSG_STORAGE_FILENAME;
    memset(msg.filename, 0, IOP_MSG_FILENAME_LENGTH);
    memcpy(msg.filename, filename, length);
    
    memset(&dclMsg, 0, (BLE_COLLECTION_CFG_DATA_SZ + 3));
    dclMsg[0] = IOP_MSG_STORAGE_FILENAME;
    memcpy((uint8_t *)&dclMsg[1], &msg, sizeof(iop_msg_filename_t));
    my2_ble_callback( dclMsg );   
}

void iop_send_motion_toggle(bool enable)
{
    iop_msg_motion_toggle_t toggle;
    dbg_str_int("Motion Toggle Send  ", enable);
    toggle.header.cmd = enable ? IOP_MSG_IMU_DATA_START : IOP_MSG_IMU_DATA_STOP;
    toggle.enable_motion = enable;
    memset(&dclMsg, 0, (BLE_COLLECTION_CFG_DATA_SZ + 3));
    dclMsg[0] = BLE_COLLECTION_CFG_DATA_SZ;
    memcpy((uint8_t *)&dclMsg[1], &toggle, sizeof(iop_msg_motion_toggle_t));
    my2_ble_callback( dclMsg );
    sensors_all_startstop(enable);
}

void iop_send_record_toggle(bool enable, uint8_t* p_data)
{
    if(p_data == NULL)
    {
        STLBLE_PRINTF("iop_send_record_toggle(): Param error");
        return;
    }
    iop_msg_record_toggle_t toggle;
    memset(&dclMsg, 0, (BLE_COLLECTION_CFG_DATA_SZ + 3));
    dclMsg[0] = BLE_COLLECTION_CFG_DATA_SZ;
    dclMsg[1] = enable? IOP_MSG_STORAGE_START : IOP_MSG_STORAGE_STOP;
    memcpy((uint8_t *)&dclMsg[2], p_data, sizeof(toggle.guid_data));
    my2_ble_callback( dclMsg );    
}

void iop_send_recognition_toggle(bool enable, ble_pme_notif_t toggle_type)
{
    iop_msg_recognition_toggle_t toggle;
    switch(toggle_type)
    {
        case RECO_CLASS_ONLY:
            toggle.header.cmd = enable ? IOP_MSG_RECO_CLASSIFICATION_START : IOP_MSG_RECO_CLASSIFICATION_STOP;
            break;
        case RECO_CLASS_FV:
            toggle.header.cmd = enable ? IOP_MSG_RECO_FEATURES_START : IOP_MSG_RECO_FEATURES_STOP;
            break;
        default:
            toggle.header.cmd = enable ? IOP_MSG_RECO_CLASSIFICATION_START : IOP_MSG_RECO_CLASSIFICATION_STOP;
            break;
    }
    toggle.enable_recognition = enable;
    dclMsg[0] = sizeof(iop_msg_recognition_toggle_t) + 1;
    memcpy((uint8_t *)&dclMsg[1], &toggle, sizeof(iop_msg_motion_toggle_t));
    my2_ble_callback((uint8_t*) &dclMsg );
}

/**
 * @brief Sends Sensor Configuration/DCL Command message down to S3
 *
 * @param dcl_cmd_id DCL Command ID
 * @param cfg_data Pointer to configuration data recieved from DCL.
 */
void iop_send_sensor_config_msg(uint8_t dcl_cmd_id, uint8_t* cfg_data)
{
    if(cfg_data == NULL)
    {
        STLBLE_PRINTF("iop_send_sensor_config_msg(): Param error");
        return;
    }
    //+2 with these because 20 byte message pluss IOP MAJOR command byte.
    //(BLE_COLLECTION_CFG_DATA_SZ is 19 bytes)
    uint8_t msg[(BLE_COLLECTION_CFG_DATA_SZ + 3)];

    memset(&msg, 0, (BLE_COLLECTION_CFG_DATA_SZ + 3));
    msg[0] = BLE_COLLECTION_CFG_DATA_SZ;
    msg[1] = IOP_MSG_SENSOR_CONFIG;
    msg[2] = dcl_cmd_id;
    memcpy(&msg[3], cfg_data, BLE_COLLECTION_CFG_DATA_SZ);
    my2_ble_callback( msg );

}

/**
 * @brief Gets Miscellaneous data from S3. Including Version #, Compilation time string,
 * and ....
 *
 */
void iop_get_s3_misc_data(uint8_t data_id)
{
    uint8_t msg[(BLE_COLLECTION_CFG_DATA_SZ + 3)];

    memset(&msg, 0, (BLE_COLLECTION_CFG_DATA_SZ + 3));
    msg[0] = BLE_COLLECTION_CFG_DATA_SZ + 3;
    msg[1] = data_id;
    my2_ble_callback( msg );
}
