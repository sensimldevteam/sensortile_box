#ifndef __PROCESS_IMU_H__
#define __PROCESS_IMU_H__

/* IMU AI processing element functions */
extern void imu_ai_data_processor(
       QAI_DataBlock_t *pIn,
       QAI_DataBlock_t *pOut,
       QAI_DataBlock_t **pRet,
       datablk_pe_event_notifier_t *pevent_notifier
     );

#endif /* __PROCESS_IMU_H__ */