/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : common.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef _common_h
#define _common_h

#ifndef FALSE
#define FALSE		(0)
#endif

#ifndef TRUE
#define TRUE		(1)
#endif

#endif /* !_common_h */
