#
# GCC Configuration options for Quick-AI SDK
#

DASH_G=-g
DASH_O=-Os

#Assembler flags
export AS_FLAGS= -mcpu=cortex-m4 -mthumb -mlittle-endian -mfloat-abi=hard -mfpu=fpv4-sp-d16 $(DASH_O) -fmessage-length=0 \
        -fsigned-char -ffunction-sections -fdata-sections  $(DASH_G) -MMD -MP

#Preprocessor macros
export MACROS=-D__FPU_PRESENT \
        -DSTM32L4R9xx \
        -DUSE_HAL_DRIVER \
        -DSTM32_SENSORTILEBOX \
        -DARM_MATH_CM4 \
        -DARM_MATH_MATRIX_CHECK \
        -DARM_MATH_ROUNDING \
        -DconfigUSE_STATS_FORMATTING_FUNCTIONS \
        -DconfigUSE_TRACE_FACILITY \
        -DGCC_MAKE \
        -DSENSIML_RECOGNITION \
        -DKBSIM \
        -D$(TOOLCHAIN) \
        -D__GNU_SOURCE \
        -D_DEFAULT_SOURCE \

export OPT_FLAGS=-fmerge-constants -fomit-frame-pointer -fcrossjumping -fexpensive-optimizations -ftoplevel-reorder
export LIBSENSIML_DIR=$(PROJ_ROOT)$(DIR_SEP)ST-Apps$(DIR_SEP)$(APP_NAME)$(DIR_SEP)knowledgepack$(DIR_SEP)sensiml
export LIBCMSIS_GCC_DIR=$(PROJ_ROOT)$(DIR_SEP)Libraries$(DIR_SEP)CMSIS$(DIR_SEP)lib$(DIR_SEP)GCC

# use IAR library

export INCLUDE_DIRS=-I"$(PROJ_DIR)/../inc" \
	-I"$(PROJ_DIR)/../../../Libraries/QLFS/inc" \
	-I"$(PROJ_DIR)/../../../Tasks/Audio/inc" \
	-I"$(PROJ_DIR)/../../../Tasks/IMU/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/Utils/inc" \
	-I"$(PROJ_DIR)/../../../Tasks/BLE/inc" \
	-I"$(PROJ_DIR)/../../../Tasks/SDCard/inc" \
	-I"$(PROJ_DIR)/../knowledgepack/sensiml/inc" \
	-I"$(PROJ_DIR)/../knowledgepack/inc" \
	-I"$(PROJ_DIR)/../IOP_MQTTSN/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/MQTTSN/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/MQTTSN_SML/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/riff_file/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/cli/inc" \
	-I"$(PROJ_DIR)/../../../Libraries/FreeRTOS_FAT/portable/QL" \
	-I"$(PROJ_DIR)/../../../Libraries/DatablockManager/inc" \
	-I"$(PROJ_DIR)/../../../Tasks/DatablockProcessor/inc" \
	-I"$(STBOX1_V1_1_0)/Drivers/CMSIS/Include" \
	-I"$(STBOX1_V1_1_0)/Drivers/CMSIS/Device/ST/STM32L4xx/Include" \
	-I"$(STBOX1_V1_1_0)/Drivers/STM32L4xx_HAL_Driver/Inc" \
	-I"$(STBOX1_V1_1_0)/Drivers/BSP/SensorTile.box" \
	-I"$(STBOX1_V1_1_0)/Drivers/BSP/Components/Common" \
	-I"$(STBOX1_V1_1_0)/Drivers/BSP/Components/hts221" \
	-I"$(STBOX1_V1_1_0)/Drivers/BSP/Components/lis2dw12" \
	-I"$(STBOX1_V1_1_0)/Drivers/BSP/Components/lis2mdl" \
	-I"$(STBOX1_V1_1_0)/Drivers/BSP/Components/lis3dhh" \
	-I"$(STBOX1_V1_1_0)/Drivers/BSP/Components/lps22hh" \
	-I"$(STBOX1_V1_1_0)/Drivers/BSP/Components/stts751" \
	-I"$(STBOX1_V1_1_0)/Drivers/BSP/Components/lsm6dsox" \
	-I"$(STBOX1_V1_1_0)/Middlewares/ST/STM32_USB_Device_Library/Class/AUDIO/Inc" \
	-I"$(STBOX1_V1_1_0)/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" \
	-I"$(STBOX1_V1_1_0)/Middlewares/ST/STM32_USB_Device_Library/Class/CustomHID/Inc" \
	-I"$(STBOX1_V1_1_0)/Middlewares/ST/STM32_USB_Device_Library/Class/DFU/Inc" \
	-I"$(STBOX1_V1_1_0)/Middlewares/ST/STM32_USB_Device_Library/Class/HID/Inc" \
	-I"$(STBOX1_V1_1_0)/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" \
	-I"$(STBOX1_V1_1_0)/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" \
	-I"$(STBOX1_V1_1_0)/Middlewares/ST/BlueNRG2/hci/hci_tl_patterns/Basic" \
	-I"$(STBOX1_V1_1_0)/Middlewares/ST/BlueNRG2/includes" \
	-I"$(STBOX1_V1_1_0)/Middlewares/ST/BlueNRG2/utils" \
	-I"$(STBOX1_V1_1_0)/Middlewares/ST/BlueNRG-MS/STM32L4xx_HAL_BlueNRG_Drivers/inc" \
	-I"$(STBOX1_V1_1_0)/Middlewares/Third_Party/FreeRTOS/Source/include" \
	-I"$(STBOX1_V1_1_0)/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F" \
	-I"$(STBOX1_V1_1_0)/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" \
	-I"$(STBOX1_V1_1_0)/Middlewares/Third_Party/FatFs/src" \
	-I"$(STBOX1_V1_1_0)/Middlewares/Third_Party/FatFs/src/option" \
	-I"$(STBOX1_V1_1_0)/Middlewares/Third_Party/FatFs/src/drivers" \

# C compiler flags
export CFLAGS= $(MACROS) \
        -mcpu=cortex-m4 -mthumb -mlittle-endian -mfloat-abi=hard -mfpu=fpv4-sp-d16 \
        $(DASH_O) $(OPT_FLAGS) -fmessage-length=0 -lm \
        -fsigned-char -ffunction-sections -fdata-sections  $(DASH_G) -std=c99 -MMD -MD -MP


export LD_FLAGS_1= -mcpu=cortex-m4 -mthumb -mlittle-endian -mfloat-abi=hard -mfpu=fpv4-sp-d16 \
	$(DASH_O) $(OPT_FLAGS) -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections \
	$(DASH_G) -T "$(PROJ_DIR)/$(OUTPUT_FILE).ld" -Xlinker --gc-sections -Wall -Werror \
	-Wl,--fatal-warnings -Wl,-Map,"$(OUTPUT_PATH)/$(OUTPUT_FILE).map" \
    --specs=nano.specs -u _printf_float --specs=nosys.specs -Wl,--no-wchar-size-warning \
    -o "$(OUTPUT_PATH)/$(OUTPUT_FILE).out" \
	-L$(LIBCMSIS_GCC_DIR) -L$(LIBSENSIML_DIR) -lsensiml -lm -larm_cortexM4lf_math


export ELF2BIN_OPTIONS=-O binary

#
# Export the files and Directoris that work for both Windows and Linux
# The DIR_SEP is needed only for OS specific command, whereas make can deal with any
#
export COMMON_STUB =$(PROJ_DIR)$(DIR_SEP)makefiles$(DIR_SEP)Makefile_common

export APP_DIR        = $(PROJ_ROOT)$(DIR_SEP)ST-Apps$(DIR_SEP)$(APP_NAME)$(DIR_SEP)src

export LIB_DIR          = $(PROJ_ROOT)$(DIR_SEP)Libraries
export CLI_DIR          = $(LIB_DIR)$(DIR_SEP)cli$(DIR_SEP)src
export MQTTSN_DIR       = $(LIB_DIR)$(DIR_SEP)MQTTSN$(DIR_SEP)src
export MQTTSN_SML_DIR   = $(LIB_DIR)$(DIR_SEP)MQTTSN_SML$(DIR_SEP)src
export RIFF_FILE_DIR    = $(LIB_DIR)$(DIR_SEP)riff_file$(DIR_SEP)src

export IOP_DIR        = $(LIB_DIR)$(DIR_SEP)IOP$(DIR_SEP)src
export QUICKAI_DIR    = $(PROJ_ROOT)$(DIR_SEP)BSP$(DIR_SEP)QuickAI$(DIR_SEP)src
export IMU_DIR        = $(PROJ_ROOT)$(DIR_SEP)Tasks$(DIR_SEP)IMU$(DIR_SEP)src
export BLE_DIR        = $(PROJ_ROOT)$(DIR_SEP)Tasks$(DIR_SEP)BLE$(DIR_SEP)src
export UTILS_DIR         = $(LIB_DIR)$(DIR_SEP)Utils$(DIR_SEP)src
export UTILS_DIR         = $(LIB_DIR)$(DIR_SEP)Utils$(DIR_SEP)src
export FREERTOS_FAT_DIR  = $(LIB_DIR)$(DIR_SEP)FreeRTOS_FAT
export FREERTOS_FAT_COMMON_DIR  = $(LIB_DIR)$(DIR_SEP)FreeRTOS_FAT$(DIR_SEP)portable$(DIR_SEP)common
export FREERTOS_FAT_QL_DIR  = $(LIB_DIR)$(DIR_SEP)FreeRTOS_FAT$(DIR_SEP)portable$(DIR_SEP)QL
export DBM_DIR   = $(LIB_DIR)$(DIR_SEP)DatablockManager$(DIR_SEP)src
export DBP_DIR = $(PROJ_ROOT)$(DIR_SEP)Tasks$(DIR_SEP)DatablockProcessor$(DIR_SEP)src

export ST_DRIVERS_DIR      = $(STBOX1_V1_1_0)$(DIR_SEP)Drivers
export ST_BSP_DIR          = $(ST_DRIVERS_DIR)$(DIR_SEP)BSP
export ST_COMPONENTS_DIR   = $(ST_BSP_DIR)$(DIR_SEP)Components
export ST_MIDDLEWARE_DIR   = $(STBOX1_V1_1_0)$(DIR_SEP)Middlewares
export ST_THIRDPARTY_DIR   = $(ST_MIDDLEWARE_DIR)$(DIR_SEP)Third_Party
export ST_BLE_DIR          = $(ST_MIDDLEWARE_DIR)$(DIR_SEP)ST$(DIR_SEP)BlueNRG2
export ST_USB_DIR          = $(ST_MIDDLEWARE_DIR)$(DIR_SEP)ST$(DIR_SEP)STM32_USB_Device_Library

export ST_HAL_DIR           = $(ST_DRIVERS_DIR)$(DIR_SEP)STM32L4xx_HAL_Driver$(DIR_SEP)Src

export ST_FREERTOS_DIR      = $(ST_THIRDPARTY_DIR)$(DIR_SEP)FreeRTOS$(DIR_SEP)Source
export ST_FATFS_DIR         = $(ST_THIRDPARTY_DIR)$(DIR_SEP)FatFs$(DIR_SEP)src
export ST_FATFS_OPTION_DIR  = $(ST_THIRDPARTY_DIR)$(DIR_SEP)FatFs$(DIR_SEP)src$(DIR_SEP)option

export ST_COMMON_COMP_DIR   = $(ST_COMPONENTS_DIR)$(DIR_SEP)Common
export ST_HTS221_COMP_DIR   = $(ST_COMPONENTS_DIR)$(DIR_SEP)hts221
export ST_LIS2DW12_COMP_DIR = $(ST_COMPONENTS_DIR)$(DIR_SEP)lis2dw12
export ST_LIS2MDL_COMP_DIR  = $(ST_COMPONENTS_DIR)$(DIR_SEP)lis2mdl
export ST_LIS3DHH_COMP_DIR  = $(ST_COMPONENTS_DIR)$(DIR_SEP)lis3dhh
export ST_LPS22HH_COMP_DIR  = $(ST_COMPONENTS_DIR)$(DIR_SEP)lps22hh
export ST_LSM6DSOX_COMP_DIR = $(ST_COMPONENTS_DIR)$(DIR_SEP)lsm6dsox
export ST_STTS751_COMP_DIR  = $(ST_COMPONENTS_DIR)$(DIR_SEP)stts751

export ST_SENSORTILE_BOX_DIR = $(ST_BSP_DIR)$(DIR_SEP)SensorTile.box

export ST_USBD_CORE_DIR        = $(ST_USB_DIR)$(DIR_SEP)Core$(DIR_SEP)Src
export ST_USBD_CLASS_CDC_DIR   = $(ST_USB_DIR)$(DIR_SEP)Class$(DIR_SEP)CDC$(DIR_SEP)Src

export ST_BLE_HCI_DIR             = $(ST_BLE_DIR)$(DIR_SEP)hci
export ST_BLE_HCI_CONTROLLER_DIR  = $(ST_BLE_DIR)$(DIR_SEP)hci$(DIR_SEP)controller
export ST_BLE_HCI_TL_PATTERNS_DIR = $(ST_BLE_DIR)$(DIR_SEP)hci$(DIR_SEP)hci_tl_patterns$(DIR_SEP)Basic
export ST_BLE_UTILS_DIR           = $(ST_BLE_DIR)$(DIR_SEP)utils
