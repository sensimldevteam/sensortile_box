@echo off
REM Give jlink.exe along with path. If the exe is there in the PATH, just give JLink.exe. 
set JLINKCMD="JLink.exe"
%JLINKCMD% -Device STM32L4R9ZI -If SWD -Speed 4000 -commandFile "LoadSensorTileBox.jlink"
pause