SensorTileBox AI Application Project
=================================

This demo project performs SensiML data collection and recognition tasks 
for the SensorTileBox evaluation platform using the "STM32CubeFunctionPack_STBOX1_V1.1.0 SDK" 
(Refer: https://www.st.com/en/evaluation-tools/steval-mksbox1v1.html and get the pro pack)


Requirements for building the IAR project:
------------------------------------------
1. Must have installed the SensorTileBox Pro SDK.
2. Must define the following IAR custom variable to point to the directory
   where the SensorTileBox SDK is installed.

   STBOX1_V1_1_0: 

    This custom IAR workspace variable is used by the "sensortilebox_ai_mqttsn_app"
    IAR project. This variable must be defined in the 
        "sensortilebox_ai_mqttsn_app.custom_argvars" file
    to point to the STM32CubeFunctionPack_STBOX1_V1.1.0 SDK location.

Procedure:

1. Copy "1_sensortilebox_ai_mqttsn_app.custom_argvars" file to "sensortilebox_ai_mqttsn_app.custom_argvars"
2. Locate the variable name STM32CubeFunctionPack_STBOX1_V1.1.0 in the file "sensortilebox_ai_mqttsn_app.custom_argvars"
   and modify its associated value to point to the location of the SensorTileBox SDK 
   installation directory.

Example:

   If the SensorTileBox SDK is installed in the following directory,

     C:\Users\STsoftware\SensorTileBox\STM32CubeFunctionPack_STBOX1_V1.1.0

   then replace the following line in the file "sensortilebox_ai_mqttsn_app.custom_argvars"

    <value>C:\SensorTileBox\STM32CubeFunctionPack_STBOX1_V1.1.0</value>
  
   with

    <value>C:\Users\STsoftware\SensorTileBox\STM32CubeFunctionPack_STBOX1_V1.1.0</value>


3. Alternatively, 
     Open the IAR project "sensortilebox_ai_mqttsn_app.eww", then navigate to 
     Tools -> Configure Custom Argument Variables...
     Then select Workspace tab and then locate the variable STSW_STLKT01
     under the group "SENSIML". Select "Edit Variable..." and update the
     value to point to the SensorTile SDK installation directory.

     Close the project and reopen for the changes to take effect.

4. In case build errors persist, close the IAR embedded workbench IDE
   and reopen for the changes to take effect.

