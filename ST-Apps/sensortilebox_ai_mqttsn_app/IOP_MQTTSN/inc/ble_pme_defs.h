/*==========================================================
 *                                                          
 *-  Copyright Notice  -------------------------------------
 *                                                          
 *    Licensed Materials - Property of QuickLogic Corp.     
 *    Copyright (C) 2019 QuickLogic Corporation             
 *    All rights reserved                                   
 *    Use, duplication, or disclosure restricted            
 *                                                          
 *    File   : ble_pme_defs.h
 *    Purpose: 
 *                                                          
 *=========================================================*/

#ifndef _BLE_PME_DEFS_H_
#define _BLE_PME_DEFS_H_

#include <stdint.h>
#include <stdbool.h>
#ifdef SENSIML_RECOGNITION
#include "kb.h"
#include "kb_defines.h"

#define BLE_PME_MAX_FV_FRAME_SZ MAX_VECTOR_SIZE

#else
#define BLE_PME_MAX_FV_FRAME_SZ 2 //not for collection.
#endif
typedef struct
{
    uint16_t context;
    uint16_t classification;

    uint8_t fv_len; //Actual length to read
    uint8_t feature_vector[BLE_PME_MAX_FV_FRAME_SZ]; //Max features reporting out is 128

} ble_pme_result_w_fv_t;

typedef struct
{
    uint16_t context;
    uint16_t classification;
} ble_pme_result_t;


#endif //_BLE_PME_DEFS_H_