/**
  ******************************************************************************
  * @file    BLEFOTA\Inc\TargetFeatures.h 
  * @author  SRA - Central Labs
  * @version V1.1.0
  * @date    10-Dec-2019
  * @brief   Specification of the HW Features for each target platform
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2019 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/  
#ifndef _TARGET_FEATURES_H_
#define _TARGET_FEATURES_H_

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include "stm32l4xx_hal.h"

#include "STBOX1_config.h"
#include "SensorTile.box.h"
#include "SensorTile.box_bc.h"

#ifdef STBOX1_ENABLE_PRINTF
  #include "usbd_desc.h"
  #include "usbd_cdc.h"
  #include "usbd_cdc_interface.h"
#endif /* STBOX1_ENABLE_PRINTF */

/* Exported defines ------------------------------------------------------- */
/* Every second */
#define STBOX1_UPDATE_LED 10000
/* Every  second */
#define STBOX1_UPDATE_BATTERY 10000
/* Every 5 mSec */
#define STBOX1_UPDATE_VCOM 50

/* Exported macros ------------------------------------------------------- */
#define MCR_HEART_BIT()  \
{                        \
  BSP_LED_On(LED_BLUE);  \
  BSP_LED_On(LED_GREEN); \
  HAL_Delay(200);        \
  BSP_LED_Off(LED_BLUE); \
  BSP_LED_Off(LED_GREEN);\
  HAL_Delay(400);        \
  BSP_LED_On(LED_BLUE);  \
  BSP_LED_On(LED_GREEN); \
  HAL_Delay(200);        \
  BSP_LED_Off(LED_BLUE); \
  BSP_LED_Off(LED_GREEN);\
  HAL_Delay(1000);       \
}

#ifdef STBOX1_RESTART_DFU
  /* Board DFU Magic Number Position */
  extern uint32_t DFU_Var;

  /* Board  DFU Magic Number */
  #define DFU_MAGIC_NUM 0xABEDBABE
#endif /* STBOX1_RESTART_DFU */

/* Exported types ------------------------------------------------------- */

/* Exported variables ------------------------------------------------------- */
extern TIM_HandleTypeDef    TimCCHandle;
#ifdef STBOX1_ENABLE_PRINTF
  extern uint8_t VComBufferToWrite[];
  extern int32_t VComBytesToWrite;
#endif /* STBOX1_ENABLE_PRINTF */

/* Exported functions ------------------------------------------------------- */
extern void InitTargetPlatform(void);
extern void LedInitTargetPlatform(void);
extern void LedOnTargetPlatform(void);
extern void LedOffTargetPlatform(void);
extern void LedToggleTargetPlatform(void);

extern uint32_t GetPage(uint32_t Address);
extern uint32_t GetBank(uint32_t Address);

#ifdef STBOX1_ENABLE_PRINTF
  extern uint32_t VCOM_read(char *buffer, uint32_t len_max);
#endif /* define STBOX1_ENABLE_PRINTF */
#ifdef __cplusplus
}
#endif

#endif /* _TARGET_FEATURES_H_ */

/******************* (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

