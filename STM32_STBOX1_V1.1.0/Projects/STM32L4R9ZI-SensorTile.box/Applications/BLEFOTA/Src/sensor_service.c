/**
  ******************************************************************************
  * @file    BLEFOTA\Src\sensor_service.c
  * @author  SRA - Central Labs
  * @version V1.1.0
  * @date    10-Dec-2019
  * @brief   Add 4 bluetooth services using vendor specific profiles.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2019 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
#include <stdio.h>
#include "TargetFeatures.h"
#include "main.h"
#include "sensor_service.h"
#include "bluenrg1_l2cap_aci.h"

#include "uuid_ble_service.h"
#include "OTA.h"

/* Exported variables --------------------------------------------------------*/
uint8_t set_connectable      = TRUE;
uint32_t ConnectionBleStatus =0;
uint8_t AdvertisingFilter    = NO_WHITE_LIST_USE;

/* Imported Variables --------------------------------------------------------*/
extern uint8_t bdaddr[6];

extern volatile int PowerButtonPressed;
extern volatile int NeedToClearSecureDB;
extern  volatile int RebootBoard;
extern uint32_t Peripheral_Pass_Key;
extern char BoardName[8];

/* Private variables ---------------------------------------------------------*/
static uint16_t HWServW2STHandle;
static uint16_t BatteryFeaturesCharHandle;
static uint16_t ConfigServW2STHandle;
static uint16_t ConfigCharHandle;

static uint16_t ConsoleW2STHandle;
static uint16_t TermCharHandle;
static uint16_t StdErrCharHandle;

static uint8_t LastStderrBuffer[W2ST_MAX_CHAR_LEN];
static uint8_t LastStderrLen;
static uint8_t LastTermBuffer[W2ST_MAX_CHAR_LEN];
static uint8_t LastTermLen;
static uint32_t SizeOfUpdateBlueFW=0;

static uint16_t CurrentConnHandle = 0;

Service_UUID_t service_uuid;
Char_UUID_t char_uuid;

/* Private functions ---------------------------------------------------------*/
static uint32_t DebugConsoleCommandParsing(uint8_t * att_data, uint8_t data_length);

static void Read_Request_CB(uint16_t handle);
static void Attribute_Modified_Request_CB(uint16_t Connection_Handle, uint16_t attr_handle, uint16_t Offset, uint8_t data_length, uint8_t *att_data);

static void UpdateWhiteList(void);

/* Private define ------------------------------------------------------------*/


/**
 * @brief  Add the Config service using a vendor specific profile
 * @param  None
 * @retval tBleStatus Status
 */
tBleStatus Add_ConfigW2ST_Service(void)
{
  tBleStatus ret;

  uint8_t uuid[16];

  COPY_CONFIG_SERVICE_UUID(uuid);
  BLUENRG_memcpy(&service_uuid.Service_UUID_128, uuid, 16);
  ret = aci_gatt_add_service(UUID_TYPE_128,  &service_uuid, PRIMARY_SERVICE, 1+3,&ConfigServW2STHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_CONFIG_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(ConfigServW2STHandle, UUID_TYPE_128, &char_uuid, W2ST_MAX_CHAR_LEN,
                           CHAR_PROP_NOTIFY| CHAR_PROP_WRITE_WITHOUT_RESP,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &ConfigCharHandle);
EndLabel:
  return ret;
}


/**
 * @brief  Add the Console service using a vendor specific profile
 * @param  None
 * @retval tBleStatus Status
 */
tBleStatus Add_ConsoleW2ST_Service(void)
{
  tBleStatus ret;

  uint8_t uuid[16];

  COPY_CONSOLE_SERVICE_UUID(uuid);
  BLUENRG_memcpy(&service_uuid.Service_UUID_128, uuid, 16);
  ret = aci_gatt_add_service(UUID_TYPE_128,  &service_uuid, PRIMARY_SERVICE, 1+3*2,&ConsoleW2STHandle);
  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_TERM_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(ConsoleW2STHandle, UUID_TYPE_128, &char_uuid, W2ST_MAX_CHAR_LEN,
                           CHAR_PROP_NOTIFY| CHAR_PROP_WRITE_WITHOUT_RESP | CHAR_PROP_WRITE | CHAR_PROP_READ ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &TermCharHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_STDERR_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(ConsoleW2STHandle, UUID_TYPE_128, &char_uuid, W2ST_MAX_CHAR_LEN,
                           CHAR_PROP_NOTIFY | CHAR_PROP_READ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &StdErrCharHandle);

EndLabel:
  return ret;
}

/**
 * @brief  Update Stdout characteristic value (when lenght is <=W2ST_MAX_CHAR_LEN)
 * @param  uint8_t *data string to write
 * @param  uint8_t lenght lengt of string to write
 * @retval tBleStatus      Status
 */
tBleStatus UpdateTermStdOut(uint8_t *data,uint8_t length)
{
  if (aci_gatt_update_char_value(ConsoleW2STHandle, TermCharHandle, 0, length , data) != BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error Updating Stdout Char\r\n");
    return BLE_STATUS_ERROR;
  }
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief  Update Stderr characteristic value (when lenght is <=W2ST_MAX_CHAR_LEN)
 * @param  uint8_t *data string to write
 * @param  uint8_t lenght lengt of string to write
 * @retval tBleStatus      Status
 */
tBleStatus UpdateTermStdErr(uint8_t *data,uint8_t length)
{
  if (aci_gatt_update_char_value(ConsoleW2STHandle, StdErrCharHandle, 0, length , data) != BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error Updating Stdout Char\r\n");
    return BLE_STATUS_ERROR;
  } 
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief  Update Stderr characteristic value
 * @param  uint8_t *data string to write
 * @param  uint8_t lenght lengt of string to write
 * @retval tBleStatus      Status
 */
tBleStatus Stderr_Update(uint8_t *data,uint8_t length)
{
  uint8_t Offset;
  uint8_t DataToSend;
  /* Split the code in packages*/
  for(Offset =0; Offset<length; Offset +=W2ST_MAX_CHAR_LEN){
    DataToSend = (length-Offset);
    DataToSend = (DataToSend>W2ST_MAX_CHAR_LEN) ?  W2ST_MAX_CHAR_LEN : DataToSend;

    /* keep a copy */
    memcpy(LastStderrBuffer,data+Offset,DataToSend);
    LastStderrLen = DataToSend;
    
    if(UpdateTermStdErr(data+Offset,DataToSend)!=BLE_STATUS_SUCCESS) {
      return BLE_STATUS_ERROR;
    }
    HAL_Delay(20);
  }
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief  Update Terminal characteristic value
 * @param  uint8_t *data string to write
 * @param  uint8_t lenght lengt of string to write
 * @retval tBleStatus      Status
 */
tBleStatus Term_Update(uint8_t *data,uint8_t length)
{
  uint8_t   Offset;
  uint8_t   DataToSend;

  /* Split the code in packages */
  for(Offset =0; Offset<length; Offset +=W2ST_MAX_CHAR_LEN){
    DataToSend = (length-Offset);
    DataToSend = (DataToSend>W2ST_MAX_CHAR_LEN) ?  W2ST_MAX_CHAR_LEN : DataToSend;

    /* keep a copy */
    memcpy(LastTermBuffer,data+Offset,DataToSend);
    LastTermLen = DataToSend;

    if(UpdateTermStdOut(data+Offset,DataToSend)!=BLE_STATUS_SUCCESS) {
      return BLE_STATUS_ERROR;
    }
    HAL_Delay(20);
  }
  return BLE_STATUS_SUCCESS;
}


/**
 * @brief  Update Stderr characteristic value after a read request
 * @param None
 * @retval tBleStatus Status
 */
static tBleStatus Stderr_Update_AfterRead(void)
{
  tBleStatus ret;
  ret = aci_gatt_update_char_value(ConsoleW2STHandle, StdErrCharHandle, 0, LastStderrLen , LastStderrBuffer);
  return ret;
}


/**
 * @brief  Update Terminal characteristic value after a read request
 * @param None
 * @retval tBleStatus Status
 */
static tBleStatus Term_Update_AfterRead(void)
{
  tBleStatus ret;
  ret = aci_gatt_update_char_value(ConsoleW2STHandle, TermCharHandle, 0, LastTermLen , LastTermBuffer);
  if (ret != BLE_STATUS_SUCCESS) {
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Error Updating Stdout Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating Stdout Char\r\n");
    }
  }
  return ret;
}

/* @brief  Send a notification for answering to a configuration command
 * @param  uint32_t Feature Feature calibrated
 * @param  uint8_t Command Replay to this Command
 * @param  uint8_t data result to send back
 * @retval tBleStatus Status
 */
tBleStatus Config_Notify(uint32_t Feature,uint8_t Command,uint8_t data)
{
  tBleStatus ret;
  uint8_t buff[2+4+1+1];

  STORE_LE_16(buff  ,(HAL_GetTick()>>3));
  STORE_BE_32(buff+2,Feature);
  buff[6] = Command;
  buff[7] = data;

  ret = aci_gatt_update_char_value (ConfigServW2STHandle, ConfigCharHandle, 0, 8,buff);
  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Error Updating Configuration Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating Configuration Char\r\n");
    }
  }
  return ret;
}

/**
 * @brief  Add the HW Features service using a vendor specific profile
 * @param  None
 * @retval tBleStatus Status
 */
tBleStatus Add_HW_SW_ServW2ST_Service(void)
{
  tBleStatus ret;

  uint8_t uuid[16];

  COPY_HW_SENS_W2ST_SERVICE_UUID(uuid);
  BLUENRG_memcpy(&service_uuid.Service_UUID_128, uuid, 16);
  ret = aci_gatt_add_service(UUID_TYPE_128,  &service_uuid, PRIMARY_SERVICE, 1+3,&HWServW2STHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_BAT_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(HWServW2STHandle, UUID_TYPE_128, &char_uuid, 2+2+2+2+1,
                           CHAR_PROP_NOTIFY,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 0, &BatteryFeaturesCharHandle);
EndLabel:
  return ret;
}

/**
 * @brief  Update Battery characteristic
 * @param  int32_t BatteryLevel %Charge level
 * @param  uint32_t Voltage Battery Voltage
 * @param  uint32_t Status Charging/Discharging
 * @retval tBleStatus   Status
 */
tBleStatus Battery_Update(uint32_t BatteryLevel, uint32_t Voltage,uint32_t Status)
{  
  tBleStatus ret;

  uint8_t buff[2+2+2+2+1];

  STORE_LE_16(buff  ,(HAL_GetTick()>>3));
  STORE_LE_16(buff+2,BatteryLevel*10);
  STORE_LE_16(buff+4,Voltage);
  STORE_LE_16(buff+6,0x8000); /* No info for Current */
  buff[8] = Status;

  ret = aci_gatt_update_char_value(HWServW2STHandle, BatteryFeaturesCharHandle, 0, 2+2+2+2+1,buff);

  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite = sprintf((char *)BufferToWrite, "Error Updating Bat Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating Bat Char\r\n");
    }
  }
  return ret;
}

/**
 * @brief  Puts the device in connectable mode.
 * @param  None
 * @retval None
 */
void setConnectable(void)
{
  uint8_t local_name[8] = {AD_TYPE_COMPLETE_LOCAL_NAME,BoardName[0],BoardName[1],BoardName[2],BoardName[3],BoardName[4],BoardName[5],BoardName[6]};

  uint8_t manuf_data[26] = {
    2,0x0A,0x00 /* 0 dBm */, // Transmission Power
    8,0x09,BoardName[0],BoardName[1],BoardName[2],BoardName[3],BoardName[4],BoardName[5],BoardName[6], // Complete Name
    13,0xFF,0x01/*SKD version */,
    0x06, /* SensorTile.box */
    0x00 /* AudioSync+AudioData */,
    0x02 /* Battery Present */,
    0x00 /*  */,
    0x00 /*  */,
    0x00 /* BLE MAC start */,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00, /* BLE MAC stop */
  };

  tBleStatus RetStatus;

  /* BLE MAC */
  manuf_data[20] = bdaddr[5];
  manuf_data[21] = bdaddr[4];
  manuf_data[22] = bdaddr[3];
  manuf_data[23] = bdaddr[2];
  manuf_data[24] = bdaddr[1];
  manuf_data[25] = bdaddr[0];

  /* disable scan response */
  RetStatus = hci_le_set_scan_response_data(0,NULL);
  if(RetStatus !=BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error hci_le_set_scan_response_data [%x]\r\n",RetStatus);
    goto EndLabel;
  }

  /* Set the board discoverable */
  if(AdvertisingFilter == NO_WHITE_LIST_USE) {
    RetStatus = aci_gap_set_discoverable(ADV_IND, 0,0,
                             RANDOM_ADDR,
                             AdvertisingFilter,
                             sizeof(local_name), local_name, 0, NULL, 0, 0);
    if(RetStatus !=BLE_STATUS_SUCCESS) {
      STBOX1_PRINTF("Error aci_gap_set_discoverable [%x] Filter=%x\r\n",RetStatus,AdvertisingFilter);
      goto EndLabel;
    } else {
      STBOX1_PRINTF("aci_gap_set_discoverable OK Filter=%x\r\n",AdvertisingFilter);
    }
  } else {
    /* Advertising filter is enabled: enter in undirected connectable mode in order to use the advertising filter on bonded device */
    RetStatus = aci_gap_set_undirected_connectable(0,0,RANDOM_ADDR, AdvertisingFilter);
    if(RetStatus !=BLE_STATUS_SUCCESS) {
      STBOX1_PRINTF("Error aci_gap_set_undirected_connectable [%x] Filter=%x\r\n",RetStatus,AdvertisingFilter);
      goto EndLabel;
    } else {
      STBOX1_PRINTF("aci_gap_set_undirected_connectable OK Filter=%x\r\n",AdvertisingFilter);
    }
  }

  /* Send Advertising data */
  RetStatus = aci_gap_update_adv_data(26, manuf_data);
  if(RetStatus !=BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error aci_gap_update_adv_data [%x]\r\n",RetStatus);
  } else {
    STBOX1_PRINTF("aci_gap_update_adv_data OK\r\n");
  }
EndLabel:
  return;
}
/**
 * @brief  Exits the device from connectable mode.
 * @param  None
 * @retval None
 */
void setNotConnectable(void)
{
  aci_gap_set_non_discoverable();
}

/**
 * @brief  This function is called when there is a Bluetooth Read request
 * @param  uint16_t handle Handle of the attribute
 * @retval None
 */
void Read_Request_CB(uint16_t handle)
{
  if (handle == StdErrCharHandle + 1) {
    /* Send again the last packet for StdError */
    Stderr_Update_AfterRead();
  } else if (handle == TermCharHandle + 1) {
    /* Send again the last packet for Terminal */
    Term_Update_AfterRead();
  }

  if(CurrentConnHandle != 0)
    aci_gatt_allow_read(CurrentConnHandle);
}

/**
 * @brief  This function is called when there is a change on the gatt attribute
 * With this function it's possible to understand if one application
 * is subscribed or not to the one service
 * @param uint16_t Connection_Handle
 * @param uint16_t attr_handle Handle of the attribute
 * @param uint16_t Offset eventual Offset (not used)
 * @param uint8_t data_length length of the data
 * @param uint8_t *att_data attribute data
 * @retval None
 */
static void Attribute_Modified_Request_CB(uint16_t Connection_Handle, uint16_t attr_handle, uint16_t Offset, uint8_t data_length, uint8_t *att_data)
{
  if(attr_handle == BatteryFeaturesCharHandle + 2){
    if (att_data[0] == 01) {
      uint32_t uhCapture = __HAL_TIM_GET_COUNTER(&TimCCHandle);

      W2ST_ON_CONNECTION(W2ST_CONNECT_BAT_EVENT);
      BSP_BC_CmdSend(BATMS_ON);

      /* Start the TIM Base generation in interrupt mode */
      if(HAL_TIM_OC_Start_IT(&TimCCHandle, TIM_CHANNEL_2) != HAL_OK){
        /* Starting Error */
        Error_Handler();
      }

      /* Set the Capture Compare Register value */
      __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_2, (uhCapture + STBOX1_UPDATE_BATTERY));
    }else if (att_data[0] == 0){
      W2ST_OFF_CONNECTION(W2ST_CONNECT_BAT_EVENT);
      BSP_BC_CmdSend(BATMS_OFF);

      /* Stop the TIM Base generation in interrupt mode */
      if(HAL_TIM_OC_Stop_IT(&TimCCHandle, TIM_CHANNEL_2) != HAL_OK){
        /* Stopping Error */
        Error_Handler();
      }
   }
#ifdef STBOX1_DEBUG_CONNECTION
   if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
     BytesToWrite =sprintf((char *)BufferToWrite,"--->Bat=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_BAT_EVENT) ? " ON" : " OFF");
     Term_Update(BufferToWrite,BytesToWrite);
   } else {
     STBOX1_PRINTF("--->Bat=%s", W2ST_CHECK_CONNECTION(W2ST_CONNECT_BAT_EVENT) ? " ON\r\n" : " OFF\r\n");
   }
#endif /* STBOX1_DEBUG_CONNECTION */
   } else if(attr_handle == ConfigCharHandle + 2){
      if (att_data[0] == 01) {
        W2ST_ON_CONNECTION(W2ST_CONNECT_CONF_EVENT);
      } else if (att_data[0] == 0){
        W2ST_OFF_CONNECTION(W2ST_CONNECT_CONF_EVENT);
      }
#ifdef STBOX1_DEBUG_CONNECTION
      if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
        BytesToWrite =sprintf((char *)BufferToWrite,"--->Conf=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT) ? "ON" : "OFF");
       Term_Update(BufferToWrite,BytesToWrite);
      } else {
        STBOX1_PRINTF("--->Conf=%s\r\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT) ? "ON" : "OFF");
      }
#endif /* STBOX1_DEBUG_CONNECTION */
   } else if (attr_handle == ConfigCharHandle + 1) {
     /* Received one write command from Client on Configuration characteristc */
     /* Do Nothing */
  } else if(attr_handle == StdErrCharHandle + 2){
    if (att_data[0] == 01) {
      W2ST_ON_CONNECTION(W2ST_CONNECT_STD_ERR);
    } else if (att_data[0] == 0){
      W2ST_OFF_CONNECTION(W2ST_CONNECT_STD_ERR);
    }
  } else if(attr_handle == TermCharHandle + 2){
    if (att_data[0] == 01) {
      W2ST_ON_CONNECTION(W2ST_CONNECT_STD_TERM);
    } else if (att_data[0] == 0){
      W2ST_OFF_CONNECTION(W2ST_CONNECT_STD_TERM);
    }
  } else if (attr_handle == TermCharHandle + 1){
    uint32_t SendBackData =1; /* By default Answer with the same message received */
    if(SizeOfUpdateBlueFW!=0) {
      /* FP-AI-STBOX1 firwmare update */
      int8_t RetValue = UpdateFWBlueMS(&SizeOfUpdateBlueFW,att_data, data_length,1);
      if(RetValue!=0) {
        MCR_FAST_TERM_UPDATE_FOR_OTA(((uint8_t *)&RetValue));
        if(RetValue==1) {
          /* if OTA checked */
          STBOX1_PRINTF("%s will restart\r\n",STBOX1_PACKAGENAME);
          RebootBoard = 1;
        }
      }
      SendBackData=0;
    } else {
      /* Received one write from Client on Terminal characteristc */
      SendBackData = DebugConsoleCommandParsing(att_data,data_length);
    }

    /* Send it back if it's not recognized */
    if(SendBackData) {
      Term_Update(att_data,data_length);
    }
#ifndef STBOX1_BLE_SECURE_CONNECTION
  /* With a Bonded Device... it should not be necessary */
  } else if (attr_handle==(0x0002+2)) {
    /* Force one UUID rescan for FOTA */
    tBleStatus ret = BLE_STATUS_INSUFFICIENT_RESOURCES;
    uint8_t buff[4];

    /* Delete all the Handles from 0x0001 to 0xFFFF */
    STORE_LE_16(buff  ,0x0001);
    STORE_LE_16(buff+2,0xFFFF);

    ret = aci_gatt_update_char_value(0x0001,0x0002,0,4,buff);

    if (ret == BLE_STATUS_SUCCESS){
      STBOX1_PRINTF("UUID Rescan Forced\r\n");
    } else {
      STBOX1_PRINTF("Problem forcing UUID Rescan\r\n");
    }
#endif /* STBOX1_BLE_SECURE_CONNECTION */
  } else {   
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Notification UNKNOWN handle\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Notification UNKNOWN handle =%d\r\n",attr_handle);
    }
  }
}

/**
 * @brief  This function makes the parsing of the Debug Console Commands
 * @param uint8_t *att_data attribute data
 * @param uint8_t data_length length of the data
 * @retval uint32_t SendBackData true/false
 */
static uint32_t DebugConsoleCommandParsing(uint8_t * att_data, uint8_t data_length)
{
  uint32_t SendBackData = 1;

  /* Help Command */
  if(!strncmp("help",(char *)(att_data),4)) {
    /* Print Legend */
    SendBackData=0;

    BytesToWrite =sprintf((char *)BufferToWrite,
       "info\n"
       "clearDB\n"
#ifdef STBOX1_RESTART_DFU
       "DFU\n"
#endif /* STBOX1_RESTART_DFU */
       "Off\n");
    Term_Update(BufferToWrite,BytesToWrite);

  } else if(!strncmp("versionFw",(char *)(att_data),9)) {
    BytesToWrite =sprintf((char *)BufferToWrite,"%s_%s_%c.%c.%c\r\n",
                          "L4R9",
                          STBOX1_PACKAGENAME,
                          STBOX1_VERSION_MAJOR,
                          STBOX1_VERSION_MINOR,
                          STBOX1_VERSION_PATCH);
    Term_Update(BufferToWrite,BytesToWrite);
    SendBackData=0;
  } else if(!strncmp("info",(char *)(att_data),4)) {
    SendBackData=0;

    BytesToWrite =sprintf((char *)BufferToWrite,"\r\nSTMicroelectronics %s:\n"
       "\tVersion %c.%c.%c\n"
      "\tSTM32L4R9ZI-SensorTile.box board"
        "\n",
        STBOX1_PACKAGENAME,
        STBOX1_VERSION_MAJOR,STBOX1_VERSION_MINOR,STBOX1_VERSION_PATCH);
    Term_Update(BufferToWrite,BytesToWrite);

    BytesToWrite =sprintf((char *)BufferToWrite,"\t(HAL %ld.%ld.%ld_%ld)\n"
      "\tCompiled %s %s"
#if defined (__IAR_SYSTEMS_ICC__)
      " (IAR)\n",
#elif defined (__CC_ARM)
      " (KEIL)\n",
#elif defined (__GNUC__)
      " (STM32CubeIDE)\n",
#endif
        HAL_GetHalVersion() >>24,
        (HAL_GetHalVersion() >>16)&0xFF,
        (HAL_GetHalVersion() >> 8)&0xFF,
         HAL_GetHalVersion()      &0xFF,
         __DATE__,__TIME__);
    Term_Update(BufferToWrite,BytesToWrite);
  } else if(!strncmp("upgradeFw",(char *)(att_data),9)) {
    uint32_t uwCRCValue;
    uint8_t *PointerByte = (uint8_t*) &SizeOfUpdateBlueFW;

    SizeOfUpdateBlueFW=atoi((char *)(att_data+9));
    PointerByte[0]=att_data[ 9];
    PointerByte[1]=att_data[10];
    PointerByte[2]=att_data[11];
    PointerByte[3]=att_data[12];

    /* Check the Maximum Possible OTA size */
    if(SizeOfUpdateBlueFW>OTA_MAX_PROG_SIZE) {
      STBOX1_PRINTF("OTA %s SIZE=%ld > %d Max Allowed\r\n",STBOX1_PACKAGENAME,SizeOfUpdateBlueFW, OTA_MAX_PROG_SIZE);
      /* Answer with a wrong CRC value for signaling the problem to BlueMS application */
      PointerByte[0]= att_data[13];
      PointerByte[1]=(att_data[14]!=0) ? 0 : 1;/* In order to be sure to have a wrong CRC */
      PointerByte[2]= att_data[15];
      PointerByte[3]= att_data[16];
      BytesToWrite = 4;
      Term_Update(BufferToWrite,BytesToWrite);
    } else {
      PointerByte = (uint8_t*) &uwCRCValue;
      PointerByte[0]=att_data[13];
      PointerByte[1]=att_data[14];
      PointerByte[2]=att_data[15];
      PointerByte[3]=att_data[16];

      STBOX1_PRINTF("OTA %s SIZE=%ld uwCRCValue=%lx\r\n",STBOX1_PACKAGENAME,SizeOfUpdateBlueFW,uwCRCValue);

      /* Reset the Flash */
      StartUpdateFWBlueMS(SizeOfUpdateBlueFW,uwCRCValue);


      /* Reduce the connection interval */
      {
         int ret = aci_l2cap_connection_parameter_update_req(
                                                      CurrentConnHandle,
                                                      10 /* interval_min*/,
                                                      10 /* interval_max */,
                                                      0   /* slave_latency */,
                                                      400 /*timeout_multiplier*/);
        /* Go to infinite loop if there is one error */
        if (ret != BLE_STATUS_SUCCESS) {
          while (1) {
            STBOX1_PRINTF("Problem Changing the connection interval\r\n");
          }
        }
      }

      /* Signal that we are ready sending back the CRV value*/
      BufferToWrite[0] = PointerByte[0];
      BufferToWrite[1] = PointerByte[1];
      BufferToWrite[2] = PointerByte[2];
      BufferToWrite[3] = PointerByte[3];
      BytesToWrite = 4;
      Term_Update(BufferToWrite,BytesToWrite);
    }

    SendBackData=0;
#ifdef STBOX1_RESTART_DFU
  } else if(!strncmp("DFU",(char *)(att_data),3)) {
    SendBackData=0;
    BytesToWrite =sprintf((char *)BufferToWrite,"\n5 sec for restarting\n\tin DFU mode\n");
    Term_Update(BufferToWrite,BytesToWrite);
    HAL_Delay(5000);
    DFU_Var = DFU_MAGIC_NUM;
    HAL_NVIC_SystemReset();
#endif /* STBOX1_RESTART_DFU */
  }  else if(!strncmp("uid",(char *)(att_data),3)) {
    /* Write back the STM32 UID */
    uint8_t *uid = (uint8_t *)STM32_UUID;
    uint32_t MCU_ID = STM32_MCU_ID[0]&0xFFF;
    BytesToWrite =sprintf((char *)BufferToWrite,"%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X_%.3lX\n",
                          uid[ 3],uid[ 2],uid[ 1],uid[ 0],
                          uid[ 7],uid[ 6],uid[ 5],uid[ 4],
                          uid[11],uid[ 10],uid[9],uid[8],
                          MCU_ID);
    Term_Update(BufferToWrite,BytesToWrite);
    SendBackData=0;
  } else if(!strncmp("Off",(char *)(att_data),3)) {
    BytesToWrite =sprintf((char *)BufferToWrite,"\n2 sec for switching off\n");
    Term_Update(BufferToWrite,BytesToWrite);
    HAL_Delay(2000);
    PowerButtonPressed=1;
    SendBackData=0;
  } else  if(!strncmp("clearDB",(char *)(att_data),7)) {
    BytesToWrite =sprintf((char *)BufferToWrite,"\nThe Secure database will be cleared\n");
    Term_Update(BufferToWrite,BytesToWrite);
    NeedToClearSecureDB=1;
    SendBackData=0;
  }
  
  return SendBackData;
}

/**
 * @brief  This function Updates the White list for BLE Connection
 * @param None
 * @retval None
 */
static void UpdateWhiteList(void)
{
  tBleStatus RetStatus;
  uint8_t NumOfAddresses;
  Bonded_Device_Entry_t BondedDeviceEntry[STBOX1_MAX_BONDED_DEVICES];

  RetStatus =  aci_gap_get_bonded_devices(&NumOfAddresses, BondedDeviceEntry);

  if (RetStatus == BLE_STATUS_SUCCESS) {
    if (NumOfAddresses > 0) {
      STBOX1_PRINTF("Bonded with %d Device(s): \r\n", NumOfAddresses);
      RetStatus = aci_gap_configure_whitelist();
      if (RetStatus != BLE_STATUS_SUCCESS) {
        STBOX1_PRINTF("aci_gap_configure_whitelist() failed:0x%02x\r\n", RetStatus);
      } else {
        STBOX1_PRINTF("aci_gap_configure_whitelist --> SUCCESS\r\n");
      }
    }
  }
}

/* ***************** BlueNRG-1 Stack Callbacks ********************************/

/*******************************************************************************
 * Function Name  : aci_gap_bond_lost_event
 * Description    : This event is generated on the slave when a 
 *                  ACI_GAP_SLAVE_SECURITY_REQUEST is called to reestablish the bond.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gap_bond_lost_event(void) {
  aci_gap_allow_rebond(CurrentConnHandle);
  STBOX1_PRINTF("aci_gap_allow_rebond()\r\n");
}

/*******************************************************************************
 * Function Name  : aci_gap_pairing_complete_event
 * Description    : This event is generated when the pairing process has completed 
 *                  successfully or a pairing procedure timeout has occurred or 
 *                  the pairing has failed
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gap_pairing_complete_event(uint16_t Connection_Handle,
                                    uint8_t Status,
                                    uint8_t Reason)
{

  if(Status==0x00) {
    STBOX1_PRINTF("Pairing Completed\r\n");
  } else {
    STBOX1_PRINTF("Pairing Not Completed for [%s] with reason=%x\r\n",
                  (Status==0x01) ? "Timeout" : "Failed",Reason);
  }

  UpdateWhiteList();
  HAL_Delay(100);
}

/*******************************************************************************
 * Function Name  : aci_gap_pairing_complete_event
 * Description    : This command should be send by the host in response to
 *                  aci_gap_pass_key_req_event
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gap_pass_key_req_event(uint16_t Connection_Handle)
{
  tBleStatus status;

  status = aci_gap_pass_key_resp(CurrentConnHandle, Peripheral_Pass_Key);
  if (status != BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("aci_gap_pass_key_resp failed:0x%02x\r\n", status);
  } else {
    STBOX1_PRINTF("aci_gap_pass_key_resp OK\r\n");
  }

}

/*******************************************************************************
 * Function Name  : aci_gap_numeric_comparison_value_event
 * Description    : This event is sent only during SC v.4.2 Pairing
 *                  when Numeric Comparison Association model is selected
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gap_numeric_comparison_value_event(uint16_t Connection_Handle, uint32_t Numeric_Value)
{
  STBOX1_PRINTF("aci_gap_numeric_comparison_value_event Numeric_Value=%ld\r\n",Numeric_Value);

  /* Confirm Yes... without control of Numeric Value received from Master */
  aci_gap_numeric_comparison_value_confirm_yesno(Connection_Handle,0x01);
}

/*******************************************************************************
 * Function Name  : hci_encryption_change_event
 * Description    : It is used to indicate that the change of the encryption
 *                  mode has been completed
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_encryption_change_event(uint8_t Status,uint16_t Connection_Handle,uint8_t Encryption_Enabled)
{
  STBOX1_PRINTF("hci_encryption_change_event\r\n");  
}

/*******************************************************************************
 * Function Name  : hci_le_connection_complete_event
 * Description    : This event indicates that a new connection has been created.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_le_connection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Role,
                                      uint8_t Peer_Address_Type,
                                      uint8_t Peer_Address[6],
                                      uint16_t Conn_Interval,
                                      uint16_t Conn_Latency,
                                      uint16_t Supervision_Timeout,
                                      uint8_t Master_Clock_Accuracy)
{
#ifdef STBOX1_BLE_SECURE_CONNECTION
  tBleStatus RetStatus;
#endif /* STBOX1_BLE_SECURE_CONNECTION */  

  STBOX1_PRINTF(">>>>>>CONNECTED %x:%x:%x:%x:%x:%x\r\n",Peer_Address[5],Peer_Address[4],Peer_Address[3],Peer_Address[2],Peer_Address[1],Peer_Address[0]);

  /* Save Current device's information */
  CurrentConnHandle = Connection_Handle;

  ConnectionBleStatus=0;
#ifdef STBOX1_BLE_SECURE_CONNECTION
  /* Check if the device is already bonded */
  RetStatus = aci_gap_is_device_bonded(Peer_Address_Type,Peer_Address);
  if( RetStatus !=BLE_STATUS_SUCCESS) {
    /* Send a slave security request to the master */
    RetStatus = aci_gap_slave_security_req(Connection_Handle);
    if (RetStatus != BLE_STATUS_SUCCESS) {
      STBOX1_PRINTF("GAP Slave secury request failed %d\r\n",RetStatus);
    } else {
      STBOX1_PRINTF("GAP Slave secury request Done\r\n");
    }
  } else {
    STBOX1_PRINTF("Device already bounded\r\n");
  }
#endif /* STBOX1_BLE_SECURE_CONNECTION */

  /* Stop the TIM Base generation in interrupt mode for Led Blinking*/
  if(HAL_TIM_OC_Stop_IT(&TimCCHandle, TIM_CHANNEL_1) != HAL_OK){
    /* Stopping Error */
    Error_Handler();
  }

  /* Just in order to be sure to switch off the User Led */
  LedOffTargetPlatform();

  HAL_Delay(100);

}/* end hci_le_connection_complete_event() */

/*******************************************************************************
 * Function Name  : hci_disconnection_complete_event
 * Description    : This event occurs when a connection is terminated.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_disconnection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Reason)
{  
  /* No Device Connected */
  CurrentConnHandle =0;

  STBOX1_PRINTF("<<<<<<DISCONNECTED\r\n");

  /* Make the device connectable again. */
  set_connectable = TRUE;
  ConnectionBleStatus=0;

  /* Reset for any problem during FOTA update */
  SizeOfUpdateBlueFW = 0;

  /* We Need to understand why we need this delay... */
#ifdef STBOX1_BLE_SECURE_CONNECTION
  //AdvertisingFilter = WHITE_LIST_FOR_ALL;
#endif /* STBOX1_BLE_SECURE_CONNECTION */

  HAL_Delay(100);

}/* end hci_disconnection_complete_event() */

/*******************************************************************************
 * Function Name  : aci_gatt_read_permit_req_event
 * Description    : This event is given when a read request is received
 *                  by the server from the client.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_read_permit_req_event(uint16_t Connection_Handle,
                                    uint16_t Attribute_Handle,
                                    uint16_t Offset)
{
  Read_Request_CB(Attribute_Handle);    
}

/*******************************************************************************
 * Function Name  : aci_gatt_attribute_modified_event
 * Description    : This event is given when an attribute change his value.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_attribute_modified_event(uint16_t Connection_Handle,
                                       uint16_t Attr_Handle,
                                       uint16_t Offset,
                                       uint16_t Attr_Data_Length,
                                       uint8_t Attr_Data[])
{
  Attribute_Modified_Request_CB(Connection_Handle, Attr_Handle, Offset, Attr_Data_Length, Attr_Data);
}

/*******************************************************************************
 * Function Name  : aci_l2cap_connection_update_resp_event
 * Description    : This event is generated when the master responds to the connection
 *                  update request packet with a connection update response packet
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_l2cap_connection_update_resp_event(uint16_t Connection_Handle,
                                            uint16_t Result)
{
  STBOX1_PRINTF("aci_l2cap_connection_update_resp_event Result=%d\r\n",Result);
}

/*******************************************************************************
 * Function Name  : hci_le_connection_update_complete_event
 * Description    : It indicates that the Controller process to update the connection has completed
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_le_connection_update_complete_event(uint8_t Status,
                                             uint16_t Connection_Handle,
                                             uint16_t Conn_Interval,
                                             uint16_t Conn_Latency,
                                             uint16_t Supervision_Timeout)
{
  STBOX1_PRINTF("hci_le_connection_update_complete_event:\r\n");
  STBOX1_PRINTF("\tStatus=%d\r\n",Status);
  STBOX1_PRINTF("\tConn_Interval=%d\r\n",Conn_Interval);
  STBOX1_PRINTF("\tConn_Latency=%d\r\n",Conn_Latency);
  STBOX1_PRINTF("\tSupervision_Timeout=%d\r\n",Supervision_Timeout);
}

/*******************************************************************************
 * Function Name  : aci_att_exchange_mtu_resp_event
 * Description    : This event is generated in response to an Exchange MTU request
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_att_exchange_mtu_resp_event(uint16_t Connection_Handle,
                                     uint16_t Server_RX_MTU)
{
  STBOX1_PRINTF("aci_att_exchange_mtu_resp_event Server_RX_MTU=%d\r\n",Server_RX_MTU);
}

/*******************************************************************************
 * Function Name  : aci_gatt_proc_complete_event
 * Description    : This event is generated when a GATT client procedure completes
 *                  either with error or successfully
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_proc_complete_event(uint16_t Connection_Handle,
                                  uint8_t Error_Code)
{
  if(Error_Code==0) {
    STBOX1_PRINTF("aci_gatt_proc_complete_event Success\r\n");
  } else {
    STBOX1_PRINTF("aci_gatt_proc_complete_event Error Code=%d\r\n",Error_Code);
  }
}

/*******************************************************************************
 * Function Name  : hci_le_data_length_change_event
 * Description    : The LE Data Length Change event notifies the Host of a change
 *                  to either the maximum Payload length or the maximum transmission
 *                  time of Data Channel PDUs in either direction
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_le_data_length_change_event(uint16_t Connection_Handle,
                                     uint16_t MaxTxOctets,
                                     uint16_t MaxTxTime,
                                     uint16_t MaxRxOctets,
                                     uint16_t MaxRxTime)
{
  tBleStatus RetStatus;
  STBOX1_PRINTF("hci_le_data_length_change_event\r\n");
  RetStatus = aci_gatt_exchange_config(Connection_Handle);
  if( RetStatus !=BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("ACI GATT Exchange Config Failed\r\n");
  } else {
    STBOX1_PRINTF("ACI GATT Exchange Config Done\r\n");
  }
}
/******************* (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
