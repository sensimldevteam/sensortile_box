@echo off
set CUBE_PROG="C:\Program Files\STMicroelectronics\STM32Cube\STM32CubeProgrammer\bin\STM32_Programmer_CLI.exe"
set BINARY_NAME=BLEFOTA
set PORTNAME=usb1
set BOOTLOADER="..\..\..\..\..\Utilities\BootLoader\STM32L4R9ZI\BootLoaderL4R9.bin"
color 0F
echo                /******************************************/
echo                               Clean BLEFOTA
echo                /******************************************/
echo                              Full Chip Erase
echo                /******************************************/
%CUBE_PROG% -c port=%PORTNAME% -e all
echo                /******************************************/
echo                              Install BootLoader
echo                /******************************************/
%CUBE_PROG%  -c port=%PORTNAME% -d %BOOTLOADER% 0x08000000 -v
echo                /******************************************/
echo                            Install BLEFOTA
echo                /******************************************/
%CUBE_PROG% -c port=%PORTNAME% -d %BINARY_NAME%.bin 0x08004000 -v
echo                /******************************************/
echo                        Dump BLEFOTA + BootLoader
echo                /******************************************/
set offset_size=0x4000
for %%I in (%BINARY_NAME%.bin) do set application_size=%%~zI
echo %BINARY_NAME%.bin size is %application_size% bytes
set /a size=%offset_size%+%application_size%
echo Dumping %offset_size% + %application_size% = %size% bytes ...
echo ..........................
%CUBE_PROG%  -c port=%PORTNAME% -u 0x08000000 %size% %BINARY_NAME%_BL.bin
echo ..........................
echo Reset or power cycle the board to start the STM32 application.
if NOT "%1" == "SILENT" pause