@echo off
set STLINK_PATH="C:\Program Files (x86)\STMicroelectronics\STM32 ST-LINK Utility\ST-LINK Utility\"
set BINARY_NAME=STM32L4R9ZI-SensorTileBox\Debug\SensorTileBox-BLEFOTA
set BOOTLOADER="..\..\..\..\..\Utilities\BootLoader\STM32L4R9ZI\BootLoaderL4R9.bin"
color 0F
echo                /******************************************/
echo                           Clean BLEFOTA
echo                /******************************************/
echo                              Full Chip Erase
echo                /******************************************/
%STLINK_PATH%ST-LINK_CLI.exe -c UR -HardRst -ME
echo                /******************************************/
echo                              Install BootLoader
echo                /******************************************/
%STLINK_PATH%ST-LINK_CLI.exe -P %BOOTLOADER% 0x08000000 -V "after_programming"
echo                /******************************************/
echo                          Install BLEFOTA
echo                /******************************************/
%STLINK_PATH%ST-LINK_CLI.exe -P %BINARY_NAME%.bin 0x08004000 -V "after_programming"
echo                /******************************************/
echo                      Dump BLEFOTA + BootLoader
echo                /******************************************/
set offset_size=0x4000
for %%I in (%BINARY_NAME%.bin) do set application_size=%%~zI
echo %BINARY_NAME%.bin size is %application_size% bytes
set /a size=%offset_size%+%application_size%
echo Dumping %offset_size% + %application_size% = %size% bytes ...
echo ..........................
%STLINK_PATH%ST-LINK_CLI.exe -Dump 0x08000000 %size% %BINARY_NAME%_BL.bin
echo                /******************************************/
echo                                 Reset STM32
echo                /******************************************/
%STLINK_PATH%ST-LINK_CLI.exe -c UR -Rst
if NOT "%1" == "SILENT" pause