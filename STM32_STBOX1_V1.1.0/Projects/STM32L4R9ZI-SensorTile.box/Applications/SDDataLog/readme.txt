/**
  ******************** (C) COPYRIGHT STMicroelectronics ***********************
  * @file    readme.txt
  * @author  Central LAB
  * @version V1.1.0
  * @date    10-Dec-2019
  * @brief   Description of the SDDataLog application firmware
  ******************************************************************************
  * Attention
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

Application Description 

 This firmware package includes Components Device Drivers, Board Support Package
  and example application for the following STMicroelectronics elements:
  - STEVAL-MKSBOX1V1 (SensorTile.box) evaluation board that contains the following components:
      - MEMS sensor devices: HTS221, LPS22HH, LIS2MDL, LSM6DSOX
      - analog microphone 
  - FatFs generic FAT file system module provides access the storage devices 
   such as memory card and hard disk.
  - FreeRTOS Real Time Kernel/Scheduler that allows applications to be organized as a collection of independent threads of execution
   (under MIT open source license)

 The Example application initializes all the Components and pressing the User button is possible to 
 start/stop the recording off all board's sensors to SD-card.
 The program save 3 different files on SD-card for each log:
 - Sens000.csv where it stores the values of Acc/Gyro/Mag/Pressure/Temperature/Humidity
 - Mic000.wav where it stores the wave file for Analog Microphone at 16Khz
 - Rep000.txt where it stores the summary of used FreeRTOS queues and Max time for writing the Audio Buffer to the .wav file:
 
  Pool Queue:
    Max Size  = XXX
    Released  = XXX
    Allocated = XXX

  Message Queue:
    Max Size  = XXX
    Released  = XXX
    Allocated = XXX
  Max time for writing XXXXbytes for Audio =XXX mSec
 
 
 
@par Hardware and Software environment

  - This example runs on STEVAL-MKSBOX1V1 (SensorTile.box) evaluation board and it
    can be easily tailored to any other supported device and development board.

@par STM32Cube packages:
  - STM32L4xx drivers from STM32CubeL4 V1.14.0
@par STEVAL-MKSBOX1V1:
  - STEVAL-MKSBOX1V1 V1.3.1

@par How to use it ?

This package contains projects for 3 IDEs viz. IAR, �Vision and System Workbench.
In order to make the  program work, you must do the following:
 - WARNING: before opening the project with any toolchain be sure your folder
   installation path is not too in-depth since the toolchain may report errors
   after building.

For IAR:
 - Open IAR toolchain (this firmware has been successfully tested with Embedded Workbench V8.32.3).
 - Open the IAR project file EWARM\DataLog.eww
 - Rebuild all files and Flash the binary on STEVAL-MKSBOX1V1

For �Vision:
 - Open �Vision toolchain (this firmware has been successfully tested with MDK-ARM Professional Version: 5.27.1)
 - Open the �Vision project file MDK-ARM\Project.uvprojx
 - Rebuild all files and Flash the binary on STEVAL-MKSBOX1V1
		
For System STM32CubeIDE:
 Open STM32CubeIDE (this firmware has been successfully tested with Version 1.1.0)
 - Set the default workspace proposed by the IDE (please be sure that there are not spaces in the workspace path).
 - Press "File" -> "Import" -> "Existing Projects into Workspace"; press "Browse" in the "Select root directory" and choose the path where the System Workbench project is located (it should be STM32CubeIDE\). 
 - Rebuild all files and Flash the binary on STEVAL-MKSBOX1V1
		
 /******************* (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
