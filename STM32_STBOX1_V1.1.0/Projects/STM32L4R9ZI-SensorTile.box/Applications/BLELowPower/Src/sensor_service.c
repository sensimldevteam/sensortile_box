/**
  ******************************************************************************
  * @file    BLELowPower\Src\sensor_service.c
  * @author  SRA - Central Labs
  * @version V1.1.0
  * @date    10-Dec-2019
  * @brief   Add 4 bluetooth services using vendor specific profiles.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2019 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
#include <stdio.h>
#include "TargetFeatures.h"
#include "main.h"
#include "sensor_service.h"

#include "bluenrg1_l2cap_aci.h"

#include "uuid_ble_service.h"
#include "PowerControl.h"

#include <math.h>

/* Exported variables --------------------------------------------------------*/
uint8_t set_connectable = TRUE;

uint32_t ConnectionBleStatus  =0;

/* Imported Variables --------------------------------------------------------*/

extern volatile float RMS_Ch;
extern float dB_Value_Old;

extern uint8_t bdaddr[6];
extern char BoardName[8];

extern volatile int PowerButtonPressed;
extern volatile int NeedToRestartDFU;

/* Private variables ---------------------------------------------------------*/
static uint32_t FeatureMask;
static uint16_t HWServW2STHandle;
static uint16_t EnvironmentalCharHandle;
static uint16_t AccGyroMagCharHandle;
static uint16_t AccEventCharHandle;
static uint16_t AudioLevelCharHandle;

static uint16_t BatteryFeaturesCharHandle;

static uint16_t ConfigServW2STHandle;
static uint16_t ConfigCharHandle;

static uint16_t ConsoleW2STHandle;
static uint16_t TermCharHandle;
static uint16_t StdErrCharHandle;

static uint8_t LastStderrBuffer[W2ST_MAX_CHAR_LEN];
static uint8_t LastStderrLen;
static uint8_t LastTermBuffer[W2ST_MAX_CHAR_LEN];
static uint8_t LastTermLen;

static uint8_t  EnvironmentalCharSize = 2; /* Size for Environmental BLE characteristic */

static uint16_t connection_handle = 0;

Service_UUID_t service_uuid;
Char_UUID_t char_uuid;

/* Private functions ---------------------------------------------------------*/
static uint32_t DebugConsoleCommandParsing(uint8_t * att_data, uint8_t data_length);
static uint32_t ConfigCommandParsing(uint8_t * att_data, uint8_t data_length);

static void Read_Request_CB(uint16_t handle);
static void Attribute_Modified_Request_CB(uint16_t Connection_Handle, uint16_t attr_handle, uint16_t Offset, uint8_t data_length, uint8_t *att_data);

/* Private define ------------------------s------------------------------------*/

#ifdef ACC_BLUENRG_CONGESTION
#define ACI_GATT_UPDATE_CHAR_VALUE safe_aci_gatt_update_char_value
static int32_t breath;


/* @brief  Update the value of a characteristic avoiding (for a short time) to
 *         send the next updates if an error in the previous sending has
 *         occurred.
 * @param  servHandle The handle of the service
 * @param  charHandle The handle of the characteristic
 * @param  charValOffset The offset of the characteristic
 * @param  charValueLen The length of the characteristic
 * @param  charValue The pointer to the characteristic
 * @retval tBleStatus Status
 */
tBleStatus safe_aci_gatt_update_char_value(uint16_t servHandle,
                      uint16_t charHandle,
                      uint8_t charValOffset,
                      uint8_t charValueLen,
                      uint8_t *charValue)
{
  tBleStatus ret = BLE_STATUS_INSUFFICIENT_RESOURCES;

  if (breath > 0) {
    breath--;
  } else {
    ret = aci_gatt_update_char_value(servHandle,charHandle,charValOffset,charValueLen,charValue);

    if (ret != BLE_STATUS_SUCCESS){
      breath = ACC_BLUENRG_CONGESTION_SKIP;
    }
  }

  return ret;
}

#else /* ACC_BLUENRG_CONGESTION */
#define ACI_GATT_UPDATE_CHAR_VALUE aci_gatt_update_char_value
#endif /* ACC_BLUENRG_CONGESTION */


/**
 * @brief  Add the Config service using a vendor specific profile
 * @param  None
 * @retval tBleStatus Status
 */
tBleStatus Add_ConfigW2ST_Service(void)
{
  tBleStatus ret;

  uint8_t uuid[16];

  COPY_CONFIG_SERVICE_UUID(uuid);
  BLUENRG_memcpy(&service_uuid.Service_UUID_128, uuid, 16);
  ret = aci_gatt_add_service(UUID_TYPE_128,  &service_uuid, PRIMARY_SERVICE, 1+3,&ConfigServW2STHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_CONFIG_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(ConfigServW2STHandle, UUID_TYPE_128, &char_uuid, W2ST_MAX_CHAR_LEN,
                           CHAR_PROP_NOTIFY| CHAR_PROP_WRITE_WITHOUT_RESP,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &ConfigCharHandle);

EndLabel:
  return ret;
}


/**
 * @brief  Add the Console service using a vendor specific profile
 * @param  None
 * @retval tBleStatus Status
 */
tBleStatus Add_ConsoleW2ST_Service(void)
{
  tBleStatus ret;

  uint8_t uuid[16];

  COPY_CONSOLE_SERVICE_UUID(uuid);
  BLUENRG_memcpy(&service_uuid.Service_UUID_128, uuid, 16);
  ret = aci_gatt_add_service(UUID_TYPE_128,  &service_uuid, PRIMARY_SERVICE, 1+3*2,&ConsoleW2STHandle);
  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_TERM_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(ConsoleW2STHandle, UUID_TYPE_128, &char_uuid, W2ST_MAX_CHAR_LEN,
                           CHAR_PROP_NOTIFY| CHAR_PROP_WRITE_WITHOUT_RESP | CHAR_PROP_WRITE | CHAR_PROP_READ ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &TermCharHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_STDERR_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(ConsoleW2STHandle, UUID_TYPE_128, &char_uuid, W2ST_MAX_CHAR_LEN,
                           CHAR_PROP_NOTIFY | CHAR_PROP_READ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &StdErrCharHandle);

EndLabel:
  return ret;
}

/**
 * @brief  Update Stdout characteristic value (when lenght is <=W2ST_MAX_CHAR_LEN)
 * @param  uint8_t *data string to write
 * @param  uint8_t lenght lengt of string to write
 * @retval tBleStatus      Status
 */
tBleStatus UpdateTermStdOut(uint8_t *data,uint8_t length)
{
  if (aci_gatt_update_char_value(ConsoleW2STHandle, TermCharHandle, 0, length , data) != BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error Updating Stdout Char\r\n");
    return BLE_STATUS_ERROR;
  }
  osDelay(20);
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief  Update Stderr characteristic value (when lenght is <=W2ST_MAX_CHAR_LEN)
 * @param  uint8_t *data string to write
 * @param  uint8_t lenght lengt of string to write
 * @retval tBleStatus      Status
 */
tBleStatus UpdateTermStdErr(uint8_t *data,uint8_t length)
{
  if (aci_gatt_update_char_value(ConsoleW2STHandle, StdErrCharHandle, 0, length , data) != BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error Updating Stdout Char\r\n");
    return BLE_STATUS_ERROR;
  }
  osDelay(20);
  return BLE_STATUS_SUCCESS;
}


/**
 * @brief  Update Stderr characteristic value
 * @param  uint8_t *data string to write
 * @param  uint8_t lenght lengt of string to write
 * @retval tBleStatus      Status
 */
tBleStatus Stderr_Update(uint8_t *data,uint8_t length)
{
  uint8_t Offset;
  uint8_t DataToSend;
  msgData_t msg;
  msg.type        = TERM_STDERR;

  /* Split the code in packages*/
  for(Offset =0; Offset<length; Offset +=W2ST_MAX_CHAR_LEN){
    DataToSend = (length-Offset);
    DataToSend = (DataToSend>W2ST_MAX_CHAR_LEN) ?  W2ST_MAX_CHAR_LEN : DataToSend;

    /* keep a copy */
    memcpy(LastStderrBuffer,data+Offset,DataToSend);
    LastStderrLen = DataToSend;

    msg.term.length   =  DataToSend;
    memcpy(msg.term.data,data+Offset,DataToSend);
    SendMsgToHost(&msg);
  }
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief  Update Terminal characteristic value
 * @param  uint8_t *data string to write
 * @param  uint8_t lenght lengt of string to write
 * @retval tBleStatus      Status
 */
tBleStatus Term_Update(uint8_t *data,uint8_t length)
{
  uint8_t   Offset;
  uint8_t   DataToSend;
  msgData_t msg;
  msg.type = TERM_STDOUT;

  /* Split the code in packages */
  for(Offset =0; Offset<length; Offset +=W2ST_MAX_CHAR_LEN){
    DataToSend = (length-Offset);
    DataToSend = (DataToSend>W2ST_MAX_CHAR_LEN) ?  W2ST_MAX_CHAR_LEN : DataToSend;

    /* keep a copy */
    memcpy(LastTermBuffer,data+Offset,DataToSend);
    LastTermLen = DataToSend;

    msg.term.length   =  DataToSend;
    memcpy(msg.term.data,data+Offset,DataToSend);
    SendMsgToHost(&msg);
  }
  return BLE_STATUS_SUCCESS;
}


/**
 * @brief  Update Stderr characteristic value after a read request
 * @param None
 * @retval tBleStatus Status
 */
static tBleStatus Stderr_Update_AfterRead(void)
{
  msgData_t msg;
  msg.type        = TERM_STDERR;
  msg.term.length = LastStderrLen;
  memcpy(msg.term.data,LastStderrBuffer,LastStderrLen);
  SendMsgToHost(&msg);
  return BLE_STATUS_SUCCESS;
}


/**
 * @brief  Update Terminal characteristic value after a read request
 * @param None
 * @retval tBleStatus Status
 */
static tBleStatus Term_Update_AfterRead(void)
{
  msgData_t msg;
  msg.type        = TERM_STDOUT;
  msg.term.length = LastTermLen;
  memcpy(msg.term.data,LastTermBuffer,LastTermLen);
  SendMsgToHost(&msg);
  return BLE_STATUS_SUCCESS;
}

/* @brief  Send a message to host thread for answering to a configuration command for Accelerometer events
 * @param  uint32_t Feature Feature type
 * @param  uint8_t Command Replay to this Command
 * @param  uint8_t data result to send back
 * @retval tBleStatus Status
 */
tBleStatus Config_Notify(uint32_t Feature,uint8_t Command,uint8_t data)
{
  msgData_t msg;
  msg.type         = CONF_NOTIFY;
  msg.conf.feature = Feature;
  msg.conf.command = Command;
  msg.conf.data    = data;
  SendMsgToHost(&msg);
  return BLE_STATUS_SUCCESS;
}

/* @brief  Send a BLE notification for answering to a configuration command for Accelerometer events
 * @param  uint32_t Feature Feature type
 * @param  uint8_t Command Replay to this Command
 * @param  uint8_t data result to send back
 * @retval tBleStatus Status
 */
tBleStatus Config_NotifyBLE(uint32_t Feature,uint8_t Command,uint8_t data)
{
  tBleStatus ret=BLE_STATUS_SUCCESS;
  uint8_t buff[2+4+1+1];

  STORE_LE_16(buff  ,(HAL_GetTick()>>3));
  STORE_BE_32(buff+2,Feature);
  buff[6] = Command;
  buff[7] = data;

  ret = aci_gatt_update_char_value (ConfigServW2STHandle, ConfigCharHandle, 0, 8,buff);
  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Error Updating Configuration Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating Configuration Char\r\n");
    }
  }
  return ret;
}

/**
 * @brief  Send a BLE notification When the LSM6DSOX detects one Acceleration event
 * @param  Command to Send
 * @retval tBleStatus Status
 */
tBleStatus AccEvent_Notify(uint16_t Command, uint8_t dimByte)
{
  tBleStatus ret = BLE_STATUS_SUCCESS;
  uint8_t buff[2+3];

  STORE_LE_16(buff  ,(HAL_GetTick()>>3));

  if(dimByte==3) {
    buff[2]= 0;
    STORE_LE_16(buff+3,Command);
  } else {
    STORE_LE_16(buff+2,Command);
  }

  ret = aci_gatt_update_char_value(HWServW2STHandle, AccEventCharHandle, 0, 2+dimByte,buff);

  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Error Updating AccEvent_Notify Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating AccEvent_Notify Char\r\n");
    }
  }
  return ret;
}

/**
 * @brief  Add the HW Features service using a vendor specific profile
 * @param  None
 * @retval tBleStatus Status
 */
tBleStatus Add_HW_SW_ServW2ST_Service(void)
{
  tBleStatus ret;

  uint8_t uuid[16];

  /* default characteristics :
  1- HW_SENS_W2ST_SERVICE_UUID
  2- ENVIRONMENTAL_W2ST_CHAR_UUID
  3- ACC_GYRO_MAG_W2ST_CHAR_UUID
  4- ACC_EVENT_W2ST_CHAR_UUID
  5- MIC_W2ST_CHAR_UUID
  6- COPY_BAT_W2ST_CHAR_UUID
 */

  COPY_HW_SENS_W2ST_SERVICE_UUID(uuid);
  BLUENRG_memcpy(&service_uuid.Service_UUID_128, uuid, 16);
  ret = aci_gatt_add_service(UUID_TYPE_128,  &service_uuid, PRIMARY_SERVICE, 1+3*6 /* max_attr_records */,&HWServW2STHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  /* Fill the Environmental BLE Characteristc */
  COPY_ENVIRONMENTAL_W2ST_CHAR_UUID(uuid);

  if (TargetBoardFeatures.HandleTempSensor != STBOX1_SNS_NOT_VALID) {
    uuid[14] |= 0x04; /* One Temperature value*/
    EnvironmentalCharSize+=2;
  }
  
  if (TargetBoardFeatures.HandleHumSensor != STBOX1_SNS_NOT_VALID) {
    uuid[14] |= 0x08; /* Humidity */
    EnvironmentalCharSize+=2;
  }

  if (TargetBoardFeatures.HandlePressSensor != STBOX1_SNS_NOT_VALID) {
    uuid[14] |= 0x10; /* Pressure value*/
    EnvironmentalCharSize+=4;
  }

  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(HWServW2STHandle, UUID_TYPE_128, &char_uuid, EnvironmentalCharSize,
                           CHAR_PROP_NOTIFY|CHAR_PROP_READ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 0, &EnvironmentalCharHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_ACC_GYRO_MAG_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(HWServW2STHandle, UUID_TYPE_128, &char_uuid, 2+3*3*2,
                           CHAR_PROP_NOTIFY,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 0, &AccGyroMagCharHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_ACC_EVENT_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(HWServW2STHandle, UUID_TYPE_128, &char_uuid, 2+3,
                           CHAR_PROP_NOTIFY | CHAR_PROP_READ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &AccEventCharHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_MIC_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(HWServW2STHandle, UUID_TYPE_128, &char_uuid, 2+1,
                           CHAR_PROP_NOTIFY,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 0, &AudioLevelCharHandle);
  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_BAT_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(HWServW2STHandle, UUID_TYPE_128, &char_uuid, 2+2+2+2+1,
                           CHAR_PROP_NOTIFY,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 0, &BatteryFeaturesCharHandle);

EndLabel:
  return ret;
}

/**
 * @brief  Update acceleration/Gryoscope and Magneto characteristics value
 * @param  BSP_MOTION_SENSOR_Axes_t Acc Structure containing acceleration value in mg
 * @param  BSP_MOTION_SENSOR_Axes_t Gyro Structure containing Gyroscope value
 * @param  BSP_MOTION_SENSOR_Axes_t Mag Structure containing magneto value
 * @retval tBleStatus      Status
 */
tBleStatus AccGyroMag_Update(BSP_MOTION_SENSOR_Axes_t *Acc,BSP_MOTION_SENSOR_Axes_t *Gyro,BSP_MOTION_SENSOR_Axes_t *Mag)
{
  tBleStatus ret = BLE_STATUS_SUCCESS;
  uint8_t buff[2+3*3*2];

  STORE_LE_16(buff   ,(HAL_GetTick()>>3));

  STORE_LE_16(buff+2 ,Acc->x);
  STORE_LE_16(buff+4 ,Acc->y);
  STORE_LE_16(buff+6 ,Acc->z);

  Gyro->x/=100;
  Gyro->y/=100;
  Gyro->z/=100;

  STORE_LE_16(buff+8 ,Gyro->x);
  STORE_LE_16(buff+10,Gyro->y);
  STORE_LE_16(buff+12,Gyro->z);

  STORE_LE_16(buff+14, Mag->x);
  STORE_LE_16(buff+16, Mag->y);
  STORE_LE_16(buff+18, Mag->z);

  ret = ACI_GATT_UPDATE_CHAR_VALUE(HWServW2STHandle, AccGyroMagCharHandle, 0, 2+3*3*2, buff);

  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Error Updating Acc/Gyro/Mag Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating Acc/Gyro/Mag Char\r\n");
    }
  }
  return ret;
}


/**
 * @brief  Update Environmental characteristic value
 * @param  int32_t Press Pressure in mbar
 * @param  uint16_t Hum humidity RH (Relative Humidity) in thenths of %
 * @param  int16_t Temp1 Temperature in tenths of degree
 * @retval tBleStatus   Status
 */
tBleStatus Environmental_Update(int32_t Press,uint16_t Hum,int16_t Temp)
{
  tBleStatus ret=BLE_STATUS_SUCCESS;
  uint8_t BuffPos;

  uint8_t buff[2+4/*Press*/+2/*Hum*/+2/*Temp*/];

  STORE_LE_16(buff  ,(HAL_GetTick()>>3));
  BuffPos=2;

  if (TargetBoardFeatures.HandlePressSensor != STBOX1_SNS_NOT_VALID) {
    STORE_LE_32(buff+BuffPos,Press);
    BuffPos+=4;
  }

  if (TargetBoardFeatures.HandleHumSensor != STBOX1_SNS_NOT_VALID) {
    STORE_LE_16(buff+BuffPos,Hum);
    BuffPos+=2;
  }

  if (TargetBoardFeatures.HandleTempSensor != STBOX1_SNS_NOT_VALID) {
    STORE_LE_16(buff+BuffPos,Temp);
    BuffPos+=2;
  }

  ret = aci_gatt_update_char_value(HWServW2STHandle, EnvironmentalCharHandle, 0, EnvironmentalCharSize,buff);

  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Error Updating Environmental Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating Environmental Char\r\n");
    }
  }
  return ret;
}

/**
 * @brief  Update Microphones characteristic values
 * @param  uint16_t Mic SNR dB Microphone
 * @retval tBleStatus   Status
 */
tBleStatus AudioLevel_Update(uint16_t Mic)
{
  tBleStatus ret=BLE_STATUS_SUCCESS;
  uint8_t buff[2+1];

  STORE_LE_16(buff  ,(HAL_GetTick()>>3));
  buff[2]= Mic&0xFF;

  ret = ACI_GATT_UPDATE_CHAR_VALUE(HWServW2STHandle, AudioLevelCharHandle, 0, 2+1,buff);

  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite = sprintf((char *)BufferToWrite, "Error Updating Mic Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    }
  }
  return ret;
}

/**
 * @brief  Update Battery characteristic
 * @param  uint32_t soc Charging/Discharging
 * @param  uint32_t voltage battery voltage
 * @param  uint32_t BatteryLevel battery level
 * @retval tBleStatus   Status
 */
tBleStatus Battery_Update(uint32_t soc, uint32_t voltage,uint32_t BatteryLevel)
{  
  tBleStatus ret=BLE_STATUS_SUCCESS;
  uint8_t buff[2+2+2+2+1];

  STORE_LE_16(buff  ,(HAL_GetTick()>>3));
  STORE_LE_16(buff+2,BatteryLevel*10);
  STORE_LE_16(buff+4,voltage);
  STORE_LE_16(buff+6,0x8000); /* Current Info not available */

  buff[8] = soc; /* here it's already the status */

  ret = aci_gatt_update_char_value(HWServW2STHandle, BatteryFeaturesCharHandle, 0, 2+2+2+2+1,buff);

  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite = sprintf((char *)BufferToWrite, "Error Updating Bat Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating Bat Char\r\n");
    }
  }
  return ret;
}

/**
 * @brief  Puts the device in connectable mode.
 * @param  None
 * @retval None
 */
void setConnectable(void)
{
    uint8_t local_name[8] = {AD_TYPE_COMPLETE_LOCAL_NAME,BoardName[0],BoardName[1],BoardName[2],BoardName[3],BoardName[4],BoardName[5],BoardName[6]};

  uint8_t manuf_data[26] = {
    2,0x0A,0x00 /* 0 dBm */, // Transmission Power
    8,0x09,BoardName[0],BoardName[1],BoardName[2],BoardName[3],BoardName[4],BoardName[5],BoardName[6], // Complete Name
    13,0xFF,0x01/*SKD version */,
    0x06, /* SensorTile.box */
    0x00 /* AudioSync+AudioData */,
    0xE0 /* ACC+Gyro+Mag*/,
    0x00 /*  */,
    0x00, /*  */
    0x00, /* BLE MAC start */
    0x00,
    0x00,
    0x00,
    0x00,
    0x00, /* BLE MAC stop */
  };

  tBleStatus RetStatus;

  /* BLE MAC */
  manuf_data[20] = bdaddr[5];
  manuf_data[21] = bdaddr[4];
  manuf_data[22] = bdaddr[3];
  manuf_data[23] = bdaddr[2];
  manuf_data[24] = bdaddr[1];
  manuf_data[25] = bdaddr[0];

  manuf_data[17] |= 0x02; /* Battery Present */

  manuf_data[16] |= 0x04; /* Mic */

  if (TargetBoardFeatures.HandleTempSensor != STBOX1_SNS_NOT_VALID) {
    manuf_data[17] |= 0x04; /* One Temperature value*/
  }

  if (TargetBoardFeatures.HandleHumSensor != STBOX1_SNS_NOT_VALID) {
    manuf_data[17] |= 0x08; /* Humidity */
  }

  if (TargetBoardFeatures.HandlePressSensor != STBOX1_SNS_NOT_VALID) {
    manuf_data[17] |= 0x10; /* Pressure value*/
  }

  /* Accelerometer Events */
  manuf_data[18] |=0x04;

  /* disable scan response */
  RetStatus = hci_le_set_scan_response_data(0,NULL);
  if(RetStatus !=BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error hci_le_set_scan_response_data [%x]\r\n",RetStatus);
    goto EndLabel;
  }

  RetStatus = aci_gap_set_discoverable(ADV_IND, 0, 0,
                           RANDOM_ADDR,
                           NO_WHITE_LIST_USE,
                           sizeof(local_name), local_name, 0, NULL, 0, 0);
  if(RetStatus !=BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error aci_gap_set_discoverable [%x]\r\n",RetStatus);
  } else {
    STBOX1_PRINTF("aci_gap_set_discoverable OK\r\n");
  }

  /* Send Advertising data */
  RetStatus = aci_gap_update_adv_data(26, manuf_data);
  if(RetStatus !=BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error aci_gap_update_adv_data [%x]\r\n",RetStatus);
  } else {
    STBOX1_PRINTF("aci_gap_update_adv_data OK\r\n");
  }

EndLabel:
  return;
}
/**
 * @brief  Exits the device from connectable mode.
 * @param  None
 * @retval None
 */
void setNotConnectable(void)
{
  aci_gap_set_non_discoverable();
}

/**
 * @brief  This function is called when there is a Bluetooth Read request
 * @param  uint16_t handle Handle of the attribute
 * @retval None
 */
void Read_Request_CB(uint16_t handle)
{
  if(handle == EnvironmentalCharHandle + 1){
    /* Read Request for Pressure,Humidity, and Temperatures*/
    int32_t PressToSend;
    uint16_t HumToSend;
    int16_t TempToSend;

    /* Read all the Environmental Sensors */
    ReadEnvironmentalData(&PressToSend,&HumToSend, &TempToSend);

    /* Send the Data with BLE */
    Environmental_Update(PressToSend,HumToSend,TempToSend);

  } else if(handle == AccEventCharHandle +1) {
    {
      uint16_t StepCount;
      if(W2ST_CHECK_HW_FEATURE(W2ST_HWF_PEDOMETER)) {
        StepCount = GetStepHWPedometer();
      } else {
        StepCount = 0;
      }
      AccEvent_Notify(StepCount, 2);
    }
  } else if (handle == StdErrCharHandle + 1) {
    /* Send again the last packet for StdError */
    Stderr_Update_AfterRead();
  } else if (handle == TermCharHandle + 1) {
    /* Send again the last packet for Terminal */
    Term_Update_AfterRead();
  }

  if(connection_handle != 0)
    aci_gatt_allow_read(connection_handle);
}

/**
 * @brief  This function is called when there is a change on the gatt attribute
 * With this function it's possible to understand if one application
 * is subscribed or not to the one service
 * @param uint16_t Connection_Handle
 * @param uint16_t attr_handle Handle of the attribute
 * @param uint16_t Offset eventual Offset (not used)
 * @param uint8_t data_length length of the data
 * @param uint8_t *att_data attribute data
 * @retval None
 */
static void Attribute_Modified_Request_CB(uint16_t Connection_Handle, uint16_t attr_handle, uint16_t Offset, uint8_t data_length, uint8_t *att_data)
{
  if(attr_handle == BatteryFeaturesCharHandle + 2){
    if (att_data[0] == 01) {
      W2ST_ON_CONNECTION(W2ST_CONNECT_BAT_EVENT);
      BSP_BC_CmdSend(BATMS_ON);
      startProc(BATTERY_INFO,ENV_UPDATE_MS);
    }else if (att_data[0] == 0){
      W2ST_OFF_CONNECTION(W2ST_CONNECT_BAT_EVENT);
      BSP_BC_CmdSend(BATMS_OFF);
      stopProc(BATTERY_INFO);
    }
#ifdef STBOX1_DEBUG_CONNECTION
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
      BytesToWrite =sprintf((char *)BufferToWrite,"--->Battery=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_BAT_EVENT) ? "ON" : "OFF");
     Term_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("--->Battery=%s\r\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_BAT_EVENT) ? "ON" : "OFF");
    }
#endif /* STBOX1_DEBUG_CONNECTION */
   } else if(attr_handle == ConfigCharHandle + 2){
      if (att_data[0] == 01) {
        W2ST_ON_CONNECTION(W2ST_CONNECT_CONF_EVENT);
      } else if (att_data[0] == 0){
        W2ST_OFF_CONNECTION(W2ST_CONNECT_CONF_EVENT);
      }
#ifdef STBOX1_DEBUG_CONNECTION
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
      BytesToWrite =sprintf((char *)BufferToWrite,"--->Conf=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT) ? "ON" : "OFF");
     Term_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("--->Conf=%s\r\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT) ? "ON" : "OFF");
    }
#endif /* STBOX1_DEBUG_CONNECTION */
    } else if(attr_handle == AccGyroMagCharHandle + 2) {
     if (att_data[0] == 01) {
       W2ST_ON_CONNECTION(W2ST_CONNECT_ACC_GYRO_MAG);
       startProc(MOTION, INERTIAL_UPDATE_MS );
    } else if (att_data[0] == 0) {
       W2ST_OFF_CONNECTION(W2ST_CONNECT_ACC_GYRO_MAG);
       stopProc(MOTION);
    }
#ifdef STBOX1_DEBUG_CONNECTION
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
      BytesToWrite =sprintf((char *)BufferToWrite,"--->Acc/Gyro/Mag=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_ACC_GYRO_MAG) ? "ON" : "OFF");
     Term_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("--->Acc/Gyro/Mag=%s\r\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_ACC_GYRO_MAG) ? "ON" : "OFF");
    }
#endif /* STBOX1_DEBUG_CONNECTION */
  } else if(attr_handle == AccEventCharHandle + 2) {
    if (att_data[0] == 01) {
      W2ST_ON_CONNECTION(W2ST_CONNECT_ACC_EVENT);
      EnableHWOrientation6D();
      if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)) {
        Config_Notify(FEATURE_MASK_ACC_EVENTS,'o',1);
      }
    } else if (att_data[0] == 0) {
      W2ST_OFF_CONNECTION(W2ST_CONNECT_ACC_EVENT);
      DisableHWFeatures();
    }
#ifdef STBOX1_DEBUG_CONNECTION
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
      BytesToWrite =sprintf((char *)BufferToWrite,"--->Acc HW Events=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT) ? "ON" : "OFF");
     Term_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("--->Acc HW Events=%s\r\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT) ? "ON" : "OFF");
    }
#endif /* STBOX1_DEBUG_CONNECTION */
  } else if(attr_handle == EnvironmentalCharHandle + 2){
    if (att_data[0] == 01) {
      W2ST_ON_CONNECTION(W2ST_CONNECT_ENV);
      startProc(ENV,ENV_UPDATE_MS);
    } else if (att_data[0] == 0){
      W2ST_OFF_CONNECTION(W2ST_CONNECT_ENV);
      stopProc(ENV);
    }
#ifdef STBOX1_DEBUG_CONNECTION
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
      BytesToWrite =sprintf((char *)BufferToWrite,"--->Env=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_ENV) ? "ON" : "OFF");
     Term_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("--->Evn=%s\r\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_ENV) ? "ON" : "OFF");
    }
#endif /* STBOX1_DEBUG_CONNECTION */
  } else if(attr_handle == StdErrCharHandle + 2){
    if (att_data[0] == 01) {
      W2ST_ON_CONNECTION(W2ST_CONNECT_STD_ERR);
    } else if (att_data[0] == 0){
      W2ST_OFF_CONNECTION(W2ST_CONNECT_STD_ERR);
    }
  } else if(attr_handle == TermCharHandle + 2){
    if (att_data[0] == 01) {
      W2ST_ON_CONNECTION(W2ST_CONNECT_STD_TERM);
    } else if (att_data[0] == 0){
      W2ST_OFF_CONNECTION(W2ST_CONNECT_STD_TERM);
    }
  } else if (attr_handle == TermCharHandle + 1){
    uint32_t SendBackData =1; /* By default Answer with the same message received */
    /* Received one write from Client on Terminal characteristc */
    SendBackData = DebugConsoleCommandParsing(att_data,data_length);

    /* Send it back if it's not recognized */
    if(SendBackData) {
      Term_Update(att_data,data_length);
    }
  } else if (attr_handle == AudioLevelCharHandle + 2) {
    if (att_data[0] == 01) {

      W2ST_ON_CONNECTION(W2ST_CONNECT_AUDIO_LEVEL);

      InitMics(AUDIO_SAMPLING_FREQUENCY);
      RMS_Ch=0;
      dB_Value_Old =0;

      startProc(AUDIO_LEV,MICS_DB_UPDATE_MS);

    } else if (att_data[0] == 0) {
      W2ST_OFF_CONNECTION(W2ST_CONNECT_AUDIO_LEVEL);

      DeInitMics();
      stopProc(AUDIO_LEV);
    }
#ifdef STBOX1_DEBUG_CONNECTION
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
      BytesToWrite =sprintf((char *)BufferToWrite,"--->Audio dB=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_AUDIO_LEVEL) ? "ON" : "OFF");
     Term_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("--->Audio dB=%s\r\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_AUDIO_LEVEL) ? "ON" : "OFF");
    }
#endif /* STBOX1_DEBUG_CONNECTION */
  } else if (attr_handle == ConfigCharHandle + 1) {
    /* Received one write command from Client on Configuration characteristc */
    ConfigCommandParsing(att_data, data_length);
#ifdef BLE_FORCE_RESCAN
  }  else if (attr_handle==(0x0002+2)) {
    /* Force one UUID rescan */
    tBleStatus ret = BLE_STATUS_INSUFFICIENT_RESOURCES;
    uint8_t buff[4];

    /* Delete all the Handles from 0x0001 to 0xFFFF */
    STORE_LE_16(buff  ,0x0001);
    STORE_LE_16(buff+2,0xFFFF);

    ret = aci_gatt_update_char_value(0x0001,0x0002,0,4,buff);

    if (ret == BLE_STATUS_SUCCESS){
      STBOX1_PRINTF("UUID Rescan Forced\r\n");
    } else {
      STBOX1_PRINTF("Problem forcing UUID Rescan\r\n");
    }
#endif /* BLE_FORCE_RESCAN */
  }else {   
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Notification UNKNOWN handle\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Notification UNKNOWN handle =%d\r\n",attr_handle);
    }
  }
}

/**
 * @brief  This function makes the parsing of the Debug Console Commands
 * @param uint8_t *att_data attribute data
 * @param uint8_t data_length length of the data
 * @retval uint32_t SendBackData true/false
 */
static uint32_t DebugConsoleCommandParsing(uint8_t * att_data, uint8_t data_length)
{
  uint32_t SendBackData = 1;

    /* Help Command */
    if(!strncmp("help",(char *)(att_data),4)) {
      /* Print Legend */
      SendBackData=0;

      BytesToWrite =sprintf((char *)BufferToWrite,
         "\ninfo\n"
#ifdef STBOX1_RESTART_DFU
         "DFU\n"
#endif /* STBOX1_RESTART_DFU */
         "Off\n");
      Term_Update(BufferToWrite,BytesToWrite);

      BytesToWrite =sprintf((char *)BufferToWrite,
         "setMicVol Val (0-64)\n");
      Term_Update(BufferToWrite,BytesToWrite);

    } else if(!strncmp("info",(char *)(att_data),4)) {
      SendBackData=0;

      BytesToWrite =sprintf((char *)BufferToWrite,"\r\nSTMicroelectronics %s:\n"
         "\tVersion %c.%c.%c\n"
        "\tSTM32L4R9ZI-SensorTile.box board\n"
#ifndef DISABLE_PM
          "\tLow Power Enabled\n"
#endif /* DISABLE_PM */
          ,STBOX1_PACKAGENAME,
          STBOX1_VERSION_MAJOR,STBOX1_VERSION_MINOR,STBOX1_VERSION_PATCH);
      Term_Update(BufferToWrite,BytesToWrite);

      BytesToWrite =sprintf((char *)BufferToWrite,"\t(HAL %ld.%ld.%ld_%ld)\n"
        "\tCompiled %s %s"
#if defined (__IAR_SYSTEMS_ICC__)
        " (IAR)\n",
#elif defined (__CC_ARM)
        " (KEIL)\n",
#elif defined (__GNUC__)
        " (STM32CubeIDE)\n",
#endif
          HAL_GetHalVersion() >>24,
          (HAL_GetHalVersion() >>16)&0xFF,
          (HAL_GetHalVersion() >> 8)&0xFF,
           HAL_GetHalVersion()      &0xFF,
           __DATE__,__TIME__);
      Term_Update(BufferToWrite,BytesToWrite);
#ifdef STBOX1_RESTART_DFU
    } else if(!strncmp("DFU",(char *)(att_data),3)) {
      SendBackData=0;
      BytesToWrite =sprintf((char *)BufferToWrite,"\r\n5 sec for restarting\r\n\tin DFU mode\r\n");
      Term_Update(BufferToWrite,BytesToWrite);
      NeedToRestartDFU=1;
#endif /* STBOX1_RESTART_DFU */
    } else if(!strncmp("uid",(char *)(att_data),3)) {
      /* Write back the STM32 UID */
      uint8_t *uid = (uint8_t *)STM32_UUID;
      uint32_t MCU_ID = STM32_MCU_ID[0]&0xFFF;
      BytesToWrite =sprintf((char *)BufferToWrite,"%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X_%.3lX\n",
                            uid[ 3],uid[ 2],uid[ 1],uid[ 0],
                            uid[ 7],uid[ 6],uid[ 5],uid[ 4],
                            uid[11],uid[ 10],uid[9],uid[8],
                            MCU_ID);
      Term_Update(BufferToWrite,BytesToWrite);
      SendBackData=0;
    } else if(!strncmp("Off",(char *)(att_data),3)) {
      PowerButtonPressed=1;
      SendBackData=0;
    } else if(!strncmp("setMicVol",(char *)(att_data),9)) {
      int32_t AudioVolume = atoi((char *)(att_data+9));

      if((AudioVolume<0) & (AudioVolume>64)) {
        BytesToWrite =sprintf((char *)BufferToWrite,"setMicVol Not Correct\n");
      } else {
        TargetBoardFeatures.AudioVolume = AudioVolume;
        BytesToWrite =sprintf((char *)BufferToWrite,"setMicVol Correct =%ld\n",
                            TargetBoardFeatures.AudioVolume);
      }

      Term_Update(BufferToWrite,BytesToWrite);
      SendBackData=0;
    }
  return SendBackData;
}

/**
 * @brief  This function makes the parsing of the Configuration Commands
 * @param uint8_t *att_data attribute data
 * @param uint8_t data_length length of the data
 * @retval uint32_t SendItBack true/false
 */
static uint32_t ConfigCommandParsing(uint8_t * att_data, uint8_t data_length)
{
  uint32_t SendItBack = 1;

  FeatureMask = (att_data[3]) | (att_data[2]<<8) | (att_data[1]<<16) | (att_data[0]<<24);
  uint8_t Command = att_data[4];
  uint8_t Data    = att_data[5];

  switch (FeatureMask) {
  case FEATURE_MASK_ACC_EVENTS:
    /* Acc events */
    if (Data == 1 ) {
      BSP_MOTION_SENSOR_Enable(TargetBoardFeatures.HandleAccSensor, MOTION_ACCELERO);
    }
    switch(Command) {
      case 'f':
        /* FreeFall */
        switch(Data) {
          case 1:
            EnableHWFreeFall();
            if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)){
              Config_Notify(FEATURE_MASK_ACC_EVENTS,Command,Data);
            }
            break;
          case 0:
            DisableHWFreeFall();
            if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)) {
              Config_Notify(FEATURE_MASK_ACC_EVENTS,Command,Data);
            }
            break;
         }
      break;
      case 'd':
        /* Double Tap */
        switch(Data) {
          case 1:
            EnableHWDoubleTap();
            if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)) {
              Config_Notify(FEATURE_MASK_ACC_EVENTS,Command,Data);
            }
            break;
          case 0:
            DisableHWDoubleTap();
            if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)) {
              Config_Notify(FEATURE_MASK_ACC_EVENTS,Command,Data);
            }
            break;
        }
      break;
      case 's':
        /* Single Tap */
        switch(Data) {
          case 1:
            EnableHWSingleTap();
            if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)) {
              Config_Notify(FEATURE_MASK_ACC_EVENTS,Command,Data);
            }
            break;
          case 0:
            DisableHWSingleTap();
            if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)) {
              Config_Notify(FEATURE_MASK_ACC_EVENTS,Command,Data);
            }
            break;
        }
      break;
      case 'w':
        /* Wake UP */
        switch(Data) {
          case 1:
            EnableHWWakeUp();
            if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)) {
              Config_Notify(FEATURE_MASK_ACC_EVENTS,Command,Data);
            }
            break;
          case 0:
            DisableHWWakeUp();
            if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)) {
              Config_Notify(FEATURE_MASK_ACC_EVENTS,Command,Data);
            }
            break;
        }
       break;
       case 't':
         /* Tilt */
        switch(Data) {
          case 1:
            EnableHWTilt();
            if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)) {
              Config_Notify(FEATURE_MASK_ACC_EVENTS,Command,Data);
            }
            break;
          case 0:
            DisableHWTilt();
            if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)) {
              Config_Notify(FEATURE_MASK_ACC_EVENTS,Command,Data);
            }
            break;
        }
      break;
      case 'o' :
        /* Tilt */
        switch(Data) {
        case 1:
          EnableHWOrientation6D();
          if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)) {
            Config_Notify(FEATURE_MASK_ACC_EVENTS,Command,Data);
          }
          break;
        case 0:
          DisableHWOrientation6D();
          if (W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT)) {
            Config_Notify(FEATURE_MASK_ACC_EVENTS,Command,Data);
          }
          break;
        }
      break;
    }
    if (Data == 0 ) {
      BSP_MOTION_SENSOR_Disable(TargetBoardFeatures.HandleAccSensor, MOTION_ACCELERO);
    }
    break;
  }
  return SendItBack;
}

/* ***************** BlueNRG-1 Stack Callbacks ********************************/

/*******************************************************************************
 * Function Name  : hci_le_connection_complete_event.
 * Description    : This event indicates that a new connection has been created.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_le_connection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Role,
                                      uint8_t Peer_Address_Type,
                                      uint8_t Peer_Address[6],
                                      uint16_t Conn_Interval,
                                      uint16_t Conn_Latency,
                                      uint16_t Supervision_Timeout,
                                      uint8_t Master_Clock_Accuracy)
{ 
  connection_handle = Connection_Handle;

  STBOX1_PRINTF(">>>>>>CONNECTED %x:%x:%x:%x:%x:%x\r\n",Peer_Address[5],Peer_Address[4],Peer_Address[3],Peer_Address[2],Peer_Address[1],Peer_Address[0]);

  ConnectionBleStatus=0;
  DisableHWFeatures();
  LedBlinkStop();
  osDelay(100);
}/* end hci_le_connection_complete_event() */

/*******************************************************************************
 * Function Name  : hci_disconnection_complete_event.
 * Description    : This event occurs when a connection is terminated.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_disconnection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Reason)
{  
  /* No Device Connected */
  connection_handle =0;
  
  STBOX1_PRINTF("<<<<<<DISCONNECTED\r\n");

  /* Make the device connectable again. */
  set_connectable = TRUE;
  
  /************************/
  /* Stops all the Timers */
  /************************/

  /* Stop Timer For Acc/Gyro/Mag */
  if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_ACC_GYRO_MAG)) {
    stopProc(MOTION);
  }

  /* Stop Timer For Environmental */
  if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_ENV)) {
    stopProc(ENV);
  }

  /* Stop Timer For Audio Level*/
  if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_AUDIO_LEVEL)) {
    stopProc(AUDIO_LEV);
  }

  /* Stop Timer Battery Level*/
  if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_BAT_EVENT)) {
    stopProc(BATTERY_INFO);
  }

  ConnectionBleStatus=0;

  DisableHWFeatures();

  osDelay(100);

}/* end hci_disconnection_complete_event() */

/*******************************************************************************
 * Function Name  : aci_gatt_read_permit_req_event.
 * Description    : This event is given when a read request is received
 *                  by the server from the client.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_read_permit_req_event(uint16_t Connection_Handle,
                                    uint16_t Attribute_Handle,
                                    uint16_t Offset)
{
  Read_Request_CB(Attribute_Handle);    
}

/*******************************************************************************
 * Function Name  : aci_gatt_attribute_modified_event.
 * Description    : This event is given when an attribute change his value.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_attribute_modified_event(uint16_t Connection_Handle,
                                       uint16_t Attr_Handle,
                                       uint16_t Offset,
                                       uint16_t Attr_Data_Length,
                                       uint8_t Attr_Data[])
{
  Attribute_Modified_Request_CB(Connection_Handle, Attr_Handle, Offset, Attr_Data_Length, Attr_Data);
}
/******************* (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
