/**
  ******************************************************************************
  * @file    BLEMLC\Inc\sensor_service.h 
  * @author  SRA - Central Labs
  * @version V1.1.0
  * @date    10-Dec-2019
  * @brief   Sensors services APIs
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2019 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/  
#ifndef _SENSOR_SERVICE_H_
#define _SENSOR_SERVICE_H_

#ifdef __cplusplus
 extern "C" {
#endif 

/* Includes ------------------------------------------------------------------*/
#include "TargetFeatures.h"
#include "bluenrg1_hal_aci.h"
#include "bluenrg1_gatt_aci.h"
#include "bluenrg1_gap_aci.h"
#include "bluenrg1_hci_le.h"

#include <stdlib.h>

/* Exported Defines --------------------------------------------------------*/

/* For enabling the capability to handle BlueNRG Congestion */
#define ACC_BLUENRG_CONGESTION

#ifdef ACC_BLUENRG_CONGESTION
/* For defining how many events skip when there is a congestion */
#define ACC_BLUENRG_CONGESTION_SKIP 30
#endif /* ACC_BLUENRG_CONGESTION */

/*************** Don't Change the following defines *************/

/* Define the Max dimesion of the Bluetooth characteristics for each packet  */
#define W2ST_MAX_CHAR_LEN 20

/* BLE Characteristic connection control */
   
/* BLE Characteristic connection control */
/* Standard Terminal */
#define W2ST_CONNECT_STD_TERM  (1   )

/* Standard Error */
#define W2ST_CONNECT_STD_ERR   (1<<1)

/* Battery Feature */
#define W2ST_CONNECT_BAT_EVENT  (1<<2)

/* Configuration Feature */
#define W2ST_CONNECT_CONF_EVENT (1<<3)

/* Activity Recognition */
#define W2ST_CONNECT_AR_EVENT   (1<<4)

/* Acceleration/Gyroscope */
#define W2ST_CONNECT_ACC_GYRO   (1<<5)

/* Machine Learning Core */
#define W2ST_CONNECT_MLC        (1<<6)

/* Finite State Machine */
#define W2ST_CONNECT_FSM        (1<<7)

#define W2ST_CHECK_CONNECTION(BleChar) ((ConnectionBleStatus&(BleChar)) ? 1 : 0)
#define W2ST_ON_CONNECTION(BleChar)    (ConnectionBleStatus|=(BleChar))
#define W2ST_OFF_CONNECTION(BleChar)   (ConnectionBleStatus&=(~BleChar))

/* Exported Types ------------------------------------------------------- */

/* Exported Variables ------------------------------------------------------- */
extern uint32_t ConnectionBleStatus;

/* Exported functions ------------------------------------------------------- */
extern tBleStatus Add_HW_SW_ServW2ST_Service(void);
extern tBleStatus Battery_Update(uint32_t BatteryLevel, uint32_t Voltage,uint32_t Status);
extern tBleStatus Add_ConsoleW2ST_Service(void);
extern tBleStatus Stderr_Update(uint8_t *data,uint8_t length);
extern tBleStatus Term_Update(uint8_t *data,uint8_t length);
extern tBleStatus UpdateTermStdOut(uint8_t *data,uint8_t length);
extern tBleStatus UpdateTermStdErr(uint8_t *data,uint8_t length);
extern tBleStatus Add_ConfigW2ST_Service(void);
extern void       setConnectable(void);
extern void       setNotConnectable(void);
extern void       setConnectionParameters(int min , int max, int latency , int timeout );
extern void       HCI_Event_CB(void *pckt);
extern tBleStatus ActivityRec_Update(HAR_output_t ActivityCode);
extern tBleStatus AccGyro_Update(BSP_MOTION_SENSOR_Axes_t *Acc,BSP_MOTION_SENSOR_Axes_t *Gyro);
extern tBleStatus MLC_Update(uint8_t *mlc_out,uint8_t *mlc_status_mainpage);
extern tBleStatus FSM_Update(lsm6dsox_fsm_out_t *fsm_out,uint8_t *fsm_status_a_mainpage,uint8_t *fsm_status_b_mainpage);

#ifdef __cplusplus
}
#endif

#endif /* _SENSOR_SERVICE_H_ */

/******************* (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
