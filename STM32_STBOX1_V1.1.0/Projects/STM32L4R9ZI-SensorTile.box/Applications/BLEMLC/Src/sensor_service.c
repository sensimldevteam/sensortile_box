/**
  ******************************************************************************
  * @file    BLEMLC\Src\sensor_service.c
  * @author  SRA - Central Labs
  * @version V1.1.0
  * @date    10-Dec-2019
  * @brief   Add 4 bluetooth services using vendor specific profiles.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2019 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
#include <stdio.h>
#include "TargetFeatures.h"
#include "main.h"
#include "sensor_service.h"
#include "bluenrg1_l2cap_aci.h"

#include "uuid_ble_service.h"

/* Exported variables --------------------------------------------------------*/
uint8_t set_connectable = TRUE;

uint32_t ConnectionBleStatus  =0;

/* Imported Variables --------------------------------------------------------*/
extern uint8_t bdaddr[6];
extern char BoardName[8];

extern volatile int PowerOff;
extern  volatile int RebootBoard;

extern HAR_output_t ActivityCode;

/* Private variables ---------------------------------------------------------*/
static uint16_t HWServW2STHandle;
static uint16_t BatteryFeaturesCharHandle;
static uint16_t ConfigServW2STHandle;
static uint16_t ConfigCharHandle;
static uint16_t ActivityRecCharHandle;
static uint16_t MLCCharHandle;
static uint16_t FSMCharHandle;

static uint16_t AccGyroCharHandle;

static uint16_t ConsoleW2STHandle;
static uint16_t TermCharHandle;
static uint16_t StdErrCharHandle;

static uint8_t LastStderrBuffer[W2ST_MAX_CHAR_LEN];
static uint8_t LastStderrLen;
static uint8_t LastTermBuffer[W2ST_MAX_CHAR_LEN];
static uint8_t LastTermLen;
static uint16_t connection_handle = 0;

Service_UUID_t service_uuid;
Char_UUID_t char_uuid;

/* Private functions ---------------------------------------------------------*/
static uint32_t DebugConsoleCommandParsing(uint8_t * att_data, uint8_t data_length);

static void Read_Request_CB(uint16_t handle);
static void Attribute_Modified_Request_CB(uint16_t Connection_Handle, uint16_t attr_handle, uint16_t Offset, uint8_t data_length, uint8_t *att_data);

/* Private define ------------------------------------------------------------*/

#ifdef ACC_BLUENRG_CONGESTION
#define ACI_GATT_UPDATE_CHAR_VALUE safe_aci_gatt_update_char_value
static int32_t breath;


/* @brief  Update the value of a characteristic avoiding (for a short time) to
 *         send the next updates if an error in the previous sending has
 *         occurred.
 * @param  servHandle The handle of the service
 * @param  charHandle The handle of the characteristic
 * @param  charValOffset The offset of the characteristic
 * @param  charValueLen The length of the characteristic
 * @param  charValue The pointer to the characteristic
 * @retval tBleStatus Status
 */
tBleStatus safe_aci_gatt_update_char_value(uint16_t servHandle,
                      uint16_t charHandle,
                      uint8_t charValOffset,
                      uint8_t charValueLen,
                      uint8_t *charValue)
{
  tBleStatus ret = BLE_STATUS_INSUFFICIENT_RESOURCES;

  if (breath > 0) {
    breath--;
  } else {
    ret = aci_gatt_update_char_value(servHandle,charHandle,charValOffset,charValueLen,charValue);

    if (ret != BLE_STATUS_SUCCESS){
      breath = ACC_BLUENRG_CONGESTION_SKIP;
    }
  }

  return ret;
}

#else /* ACC_BLUENRG_CONGESTION */
#define ACI_GATT_UPDATE_CHAR_VALUE aci_gatt_update_char_value
#endif /* ACC_BLUENRG_CONGESTION */


/**
 * @brief  Add the Config service using a vendor specific profile
 * @param  None
 * @retval tBleStatus Status
 */
tBleStatus Add_ConfigW2ST_Service(void)
{
  tBleStatus ret;

  uint8_t uuid[16];

  COPY_CONFIG_SERVICE_UUID(uuid);
  BLUENRG_memcpy(&service_uuid.Service_UUID_128, uuid, 16);
  ret = aci_gatt_add_service(UUID_TYPE_128,  &service_uuid, PRIMARY_SERVICE, 1+3,&ConfigServW2STHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_CONFIG_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(ConfigServW2STHandle, UUID_TYPE_128, &char_uuid, W2ST_MAX_CHAR_LEN,
                           CHAR_PROP_NOTIFY| CHAR_PROP_WRITE_WITHOUT_RESP,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &ConfigCharHandle);
EndLabel:
  return ret;
}


/**
 * @brief  Add the Console service using a vendor specific profile
 * @param  None
 * @retval tBleStatus Status
 */
tBleStatus Add_ConsoleW2ST_Service(void)
{
  tBleStatus ret;

  uint8_t uuid[16];

  COPY_CONSOLE_SERVICE_UUID(uuid);
  BLUENRG_memcpy(&service_uuid.Service_UUID_128, uuid, 16);
  ret = aci_gatt_add_service(UUID_TYPE_128,  &service_uuid, PRIMARY_SERVICE, 1+3*2,&ConsoleW2STHandle);
  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_TERM_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(ConsoleW2STHandle, UUID_TYPE_128, &char_uuid, W2ST_MAX_CHAR_LEN,
                           CHAR_PROP_NOTIFY| CHAR_PROP_WRITE_WITHOUT_RESP | CHAR_PROP_WRITE | CHAR_PROP_READ ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &TermCharHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_STDERR_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(ConsoleW2STHandle, UUID_TYPE_128, &char_uuid, W2ST_MAX_CHAR_LEN,
                           CHAR_PROP_NOTIFY | CHAR_PROP_READ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 1, &StdErrCharHandle);
EndLabel:
  return ret;
}

/**
 * @brief  Update Stdout characteristic value (when lenght is <=W2ST_MAX_CHAR_LEN)
 * @param  uint8_t *data string to write
 * @param  uint8_t lenght lengt of string to write
 * @retval tBleStatus      Status
 */
tBleStatus UpdateTermStdOut(uint8_t *data,uint8_t length)
{
  if (aci_gatt_update_char_value(ConsoleW2STHandle, TermCharHandle, 0, length , data) != BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error Updating Stdout Char\r\n");
    return BLE_STATUS_ERROR;
  }
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief  Update Stderr characteristic value (when lenght is <=W2ST_MAX_CHAR_LEN)
 * @param  uint8_t *data string to write
 * @param  uint8_t lenght lengt of string to write
 * @retval tBleStatus      Status
 */
tBleStatus UpdateTermStdErr(uint8_t *data,uint8_t length)
{
  if (aci_gatt_update_char_value(ConsoleW2STHandle, StdErrCharHandle, 0, length , data) != BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error Updating Stdout Char\r\n");
    return BLE_STATUS_ERROR;
  } 
  return BLE_STATUS_SUCCESS;
}


/**
 * @brief  Update Stderr characteristic value
 * @param  uint8_t *data string to write
 * @param  uint8_t lenght lengt of string to write
 * @retval tBleStatus      Status
 */
tBleStatus Stderr_Update(uint8_t *data,uint8_t length)
{
  uint8_t Offset;
  uint8_t DataToSend;
  /* Split the code in packages*/
  for(Offset =0; Offset<length; Offset +=W2ST_MAX_CHAR_LEN){
    DataToSend = (length-Offset);
    DataToSend = (DataToSend>W2ST_MAX_CHAR_LEN) ?  W2ST_MAX_CHAR_LEN : DataToSend;

    /* keep a copy */
    memcpy(LastStderrBuffer,data+Offset,DataToSend);
    LastStderrLen = DataToSend;
    
    if(UpdateTermStdErr(data+Offset,DataToSend) != BLE_STATUS_SUCCESS) {
      return BLE_STATUS_ERROR;
    }
    HAL_Delay(20);
  }
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief  Update Terminal characteristic value
 * @param  uint8_t *data string to write
 * @param  uint8_t lenght lengt of string to write
 * @retval tBleStatus      Status
 */
tBleStatus Term_Update(uint8_t *data,uint8_t length)
{
  uint8_t   Offset;
  uint8_t   DataToSend;

  /* Split the code in packages */
  for(Offset =0; Offset<length; Offset +=W2ST_MAX_CHAR_LEN){
    DataToSend = (length-Offset);
    DataToSend = (DataToSend>W2ST_MAX_CHAR_LEN) ?  W2ST_MAX_CHAR_LEN : DataToSend;

    /* keep a copy */
    memcpy(LastTermBuffer,data+Offset,DataToSend);
    LastTermLen = DataToSend;

    if(UpdateTermStdOut(data+Offset,DataToSend)!= BLE_STATUS_SUCCESS) {
      return BLE_STATUS_ERROR;
    }
    HAL_Delay(20);
  }
  return BLE_STATUS_SUCCESS;
}


/**
 * @brief  Update Stderr characteristic value after a read request
 * @param None
 * @retval tBleStatus Status
 */
static tBleStatus Stderr_Update_AfterRead(void)
{
  tBleStatus ret;
  ret = aci_gatt_update_char_value(ConsoleW2STHandle, StdErrCharHandle, 0, LastStderrLen , LastStderrBuffer);
  return ret;
}


/**
 * @brief  Update Terminal characteristic value after a read request
 * @param None
 * @retval tBleStatus Status
 */
static tBleStatus Term_Update_AfterRead(void)
{
  tBleStatus ret;
  ret = aci_gatt_update_char_value(ConsoleW2STHandle, TermCharHandle, 0, LastTermLen , LastTermBuffer);
  if (ret != BLE_STATUS_SUCCESS) {
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Error Updating Stdout Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating Stdout Char\r\n");
    }
  }
  return ret;
}

/**
 * @brief  Add the HW Features service using a vendor specific profile
 * @param  None
 * @retval tBleStatus Status
 */
tBleStatus Add_HW_SW_ServW2ST_Service(void)
{
  tBleStatus ret;

  uint8_t uuid[16];

  COPY_HW_SENS_W2ST_SERVICE_UUID(uuid);
  BLUENRG_memcpy(&service_uuid.Service_UUID_128, uuid, 16);
  ret = aci_gatt_add_service(UUID_TYPE_128,  &service_uuid, PRIMARY_SERVICE, 1+5*3 /* Batt/ActivityRec/AccGyro/MLC/FSM */,&HWServW2STHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_BAT_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(HWServW2STHandle, UUID_TYPE_128, &char_uuid, 2+2+2+2+1,
                           CHAR_PROP_NOTIFY,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 0, &BatteryFeaturesCharHandle);
  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }
  
  COPY_ACC_GYRO_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(HWServW2STHandle, UUID_TYPE_128, &char_uuid, 2+2*3*2,
                           CHAR_PROP_NOTIFY,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 0, &AccGyroCharHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_ACTIVITY_REC_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(HWServW2STHandle, UUID_TYPE_128, &char_uuid, 2+1/* 2 byte timestamp, 1 byte action */,
                           CHAR_PROP_NOTIFY | CHAR_PROP_READ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 0, &ActivityRecCharHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_MLC_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(HWServW2STHandle, UUID_TYPE_128, &char_uuid, 2+8+1/* 2 byte timestamp, 8 MLC registers output, 1 MCL output state */,
                           CHAR_PROP_NOTIFY| CHAR_PROP_WRITE_WITHOUT_RESP | CHAR_PROP_WRITE | CHAR_PROP_READ ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 0, &MLCCharHandle);

  if (ret != BLE_STATUS_SUCCESS) {
    goto EndLabel;
  }

  COPY_FSM_W2ST_CHAR_UUID(uuid);
  BLUENRG_memcpy(&char_uuid.Char_UUID_128, uuid, 16);
  ret =  aci_gatt_add_char(HWServW2STHandle, UUID_TYPE_128, &char_uuid, 2+16+2/* 2 byte timestamp, 16 FSM registers outputs, 2 FSM outputs state */,
                           CHAR_PROP_NOTIFY| CHAR_PROP_WRITE_WITHOUT_RESP | CHAR_PROP_WRITE | CHAR_PROP_READ ,
                           ATTR_PERMISSION_NONE,
                           GATT_NOTIFY_ATTRIBUTE_WRITE | GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP,
                           16, 0, &FSMCharHandle);
EndLabel:
  return ret;
}

/**
 * @brief  Update acceleration and Gryoscope characteristics value
 * @param  BSP_MOTION_SENSOR_Axes_t Acc Structure containing acceleration value in mg
 * @param  BSP_MOTION_SENSOR_Axes_t Gyro Structure containing Gyroscope value
 * @retval tBleStatus      Status
 */
tBleStatus AccGyro_Update(BSP_MOTION_SENSOR_Axes_t *Acc,BSP_MOTION_SENSOR_Axes_t *Gyro)
{
  tBleStatus ret = BLE_STATUS_SUCCESS;

  uint8_t buff[2+2*3*2];

  STORE_LE_16(buff   ,(HAL_GetTick()>>3));

  STORE_LE_16(buff+2 ,Acc->x);
  STORE_LE_16(buff+4 ,Acc->y);
  STORE_LE_16(buff+6 ,Acc->z);

  Gyro->x/=100;
  Gyro->y/=100;
  Gyro->z/=100;

  STORE_LE_16(buff+8 ,Gyro->x);
  STORE_LE_16(buff+10,Gyro->y);
  STORE_LE_16(buff+12,Gyro->z);

  ret = ACI_GATT_UPDATE_CHAR_VALUE(HWServW2STHandle, AccGyroCharHandle, 0, 2+2*3*2, buff);

  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Error Updating Acc/Gyro Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating Acc/Gyro Char\r\n");
    }
  }
  return ret;
}

/**
 * @brief  Update Battery characteristic
 * @param  int32_t BatteryLevel %Charge level
 * @param  uint32_t Voltage Battery Voltage
 * @param  uint32_t Status Charging/Discharging
 * @retval tBleStatus   Status
 */
tBleStatus Battery_Update(uint32_t BatteryLevel, uint32_t Voltage,uint32_t Status)
{  
  tBleStatus ret;

  uint8_t buff[2+2+2+2+1];

  STORE_LE_16(buff  ,(HAL_GetTick()>>3));
  STORE_LE_16(buff+2,BatteryLevel*10);
  STORE_LE_16(buff+4,Voltage);
  STORE_LE_16(buff+6,0x8000); /* No info for Current */
  buff[8] = Status;

  ret = aci_gatt_update_char_value(HWServW2STHandle, BatteryFeaturesCharHandle, 0, 2+2+2+2+1,buff);

  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite = sprintf((char *)BufferToWrite, "Error Updating Bat Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating Bat Char\r\n");
    }
  }
  return ret;
}

/**
 * @brief  Update Activity Recognition value
 * @param  HAR_output_t ActivityCode Activity Recognized
 * @retval tBleStatus      Status
 */
tBleStatus ActivityRec_Update(HAR_output_t ActivityCode)
{
  tBleStatus ret;
  uint8_t buff[2+1+1];

  STORE_LE_16(buff  ,(HAL_GetTick()>>3));
  buff[2] = ActivityCode;

  ret = aci_gatt_update_char_value(HWServW2STHandle, ActivityRecCharHandle, 0, 2+1, buff);

  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Error Updating ActivityRec Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating ActivityRec Char\r\n");
    }
  }
  return ret;
}


/**
 * @brief  Update Machine Learning Core output registers
 * @param  uint8_t *mlc_out pointers to 8 MLC register
 * @param  uint8_t *mlc_status_mainpage pointer to the MLC status bits from 1 to 8
 * @retval tBleStatus Status
 */
tBleStatus MLC_Update(uint8_t *mlc_out,uint8_t *mlc_status_mainpage)
{
  tBleStatus ret;
  uint8_t buff[2+8+1];

  /* TimeStamp */
  STORE_LE_16(buff  ,(HAL_GetTick()>>3));
  /* MLC outputs registers */
  memcpy(buff+2,mlc_out, 8*sizeof(uint8_t));
  /* Status bit for MLC from 1 to 8   */
  buff[10] = *mlc_status_mainpage;

  ret = aci_gatt_update_char_value(HWServW2STHandle, MLCCharHandle, 0, 2+8+1, buff);

  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Error Updating MLC Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating MLC Char\r\n");
    }
  }
  return ret;
}


/**
 * @brief  Update Finite State Machine output registers
 * @param  lsm6dsox_fsm_out_t *fsm_out pointer to 16 FSM output registers
 * @param  uint8_t *fsm_status_a_mainpage pointer to the FSM status bits from 1 to 8
 * @param  uint8_t *fsm_status_a_mainpage pointer to the FSM status bits from 9 to 16
 * @retval tBleStatus Status
 */
tBleStatus FSM_Update(lsm6dsox_fsm_out_t *fsm_out,uint8_t *fsm_status_a_mainpage,uint8_t *fsm_status_b_mainpage)
{
  tBleStatus ret;
  uint8_t buff[2+16+2];

  /* TimeStamp */
  STORE_LE_16(buff  ,(HAL_GetTick()>>3));
  /* FSM outputs registers */
  memcpy(buff+2,(uint8_t *)fsm_out, 16*sizeof(uint8_t));
  /* Status bits for FSM from 1 to 8 */
  buff[18] = *fsm_status_a_mainpage;
  /* Status bit for FSM from 9 to 16 */
  buff[19] = *fsm_status_b_mainpage;

  ret = aci_gatt_update_char_value(HWServW2STHandle, FSMCharHandle, 0, 2+16+2, buff);

  if (ret != BLE_STATUS_SUCCESS){
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Error Updating FSM Char\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Error Updating FSM Char\r\n");
    }
  }
  return ret;
}
/**
 * @brief  Puts the device in connectable mode.
 * @param  None
 * @retval None
 */
void setConnectable(void)
{
   uint8_t local_name[8] = {AD_TYPE_COMPLETE_LOCAL_NAME,BoardName[0],BoardName[1],BoardName[2],BoardName[3],BoardName[4],BoardName[5],BoardName[6]};

  uint8_t manuf_data[26] = {
    2,0x0A,0x00 /* 0 dBm */, // Transmission Power
    8,0x09,BoardName[0],BoardName[1],BoardName[2],BoardName[3],BoardName[4],BoardName[5],BoardName[6], // Complete Name
    13,0xFF,0x01/*SKD version */,
    0x06, /* SensorTile.box */
    0x00 /* AudioSync+AudioData */,
    0xC0 /* ACC+Gyro*/ |
      0x02 /* Battery Present */,
    0x00 /*  */,
    0x00, /*  */
    0x00, /* BLE MAC start */
    0x00,
    0x00,
    0x00,
    0x00,
    0x00, /* BLE MAC stop */
  };

  tBleStatus RetStatus;
  
  /* Activity Recognition */
  manuf_data[19] |= 0x10;

  /* BLE MAC */
  manuf_data[20] = bdaddr[5];
  manuf_data[21] = bdaddr[4];
  manuf_data[22] = bdaddr[3];
  manuf_data[23] = bdaddr[2];
  manuf_data[24] = bdaddr[1];
  manuf_data[25] = bdaddr[0];

  /* disable scan response */
  RetStatus = hci_le_set_scan_response_data(0,NULL);
  if(RetStatus !=BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error hci_le_set_scan_response_data [%x]\r\n",RetStatus);
    goto EndLabel;
  }

  RetStatus = aci_gap_set_discoverable(ADV_IND, 0, 0,
                           RANDOM_ADDR,
                           NO_WHITE_LIST_USE,
                           sizeof(local_name), local_name, 0, NULL, 0, 0);
  if(RetStatus !=BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error aci_gap_set_discoverable [%x]\r\n",RetStatus);
  } else {
    STBOX1_PRINTF("aci_gap_set_discoverable OK\r\n");
  }

  /* Send Advertising data */
  RetStatus = aci_gap_update_adv_data(26, manuf_data);
  if(RetStatus !=BLE_STATUS_SUCCESS) {
    STBOX1_PRINTF("Error aci_gap_update_adv_data [%x]\r\n",RetStatus);
  } else {
    STBOX1_PRINTF("aci_gap_update_adv_data OK\r\n");
  }

EndLabel:
  return;
}
/**
 * @brief  Exits the device from connectable mode.
 * @param  None
 * @retval None
 */
void setNotConnectable(void)
{
  aci_gap_set_non_discoverable();
}

/**
 * @brief  This function is called when there is a Bluetooth Read request
 * @param  uint16_t handle Handle of the attribute
 * @retval None
 */
void Read_Request_CB(uint16_t handle)
{
  if (handle == StdErrCharHandle + 1) {

    /* Send again the last packet for StdError */
    Stderr_Update_AfterRead();

  } else if (handle == TermCharHandle + 1) {

    /* Send again the last packet for Terminal */
    Term_Update_AfterRead();

  } else if (handle == ActivityRecCharHandle + 1) {
    lsm6dsox_all_sources_t  status;
    uint8_t mlc_out[8];
    lsm6dsox_all_sources_get(LSM6DSOX_Contex, &status);
    lsm6dsox_mlc_out_get(LSM6DSOX_Contex, mlc_out);
    ActivityCode = MappingToHAR_ouput_t[mlc_out[0]];
    ActivityRec_Update(ActivityCode);
  } else if (handle == MLCCharHandle +1) {

    lsm6dsox_all_sources_t  status;
    uint8_t mlc_out[8];

    STBOX1_PRINTF("Read On MLC\r\n");

    lsm6dsox_all_sources_get(LSM6DSOX_Contex, &status);
    lsm6dsox_mlc_out_get(LSM6DSOX_Contex, mlc_out);

    MLC_Update(mlc_out,(uint8_t *)&(status.mlc_status));

  } else if (handle == FSMCharHandle +1) {

    lsm6dsox_all_sources_t  status;
    lsm6dsox_fsm_out_t      fsm_out;

    STBOX1_PRINTF("Read On FSM\r\n");

    lsm6dsox_all_sources_get(LSM6DSOX_Contex, &status);
    lsm6dsox_fsm_out_get(LSM6DSOX_Contex, &fsm_out);

    FSM_Update(&fsm_out,(uint8_t *)&(status.fsm_status_a),(uint8_t *)&(status.fsm_status_b));

  }

  if(connection_handle != 0)
    aci_gatt_allow_read(connection_handle);
}

/**
 * @brief  This function is called when there is a change on the gatt attribute
 * With this function it's possible to understand if one application
 * is subscribed or not to the one service
 * @param uint16_t Connection_Handle
 * @param uint16_t attr_handle Handle of the attribute
 * @param uint16_t Offset eventual Offset (not used)
 * @param uint8_t data_length length of the data
 * @param uint8_t *att_data attribute data
 * @retval None
 */
static void Attribute_Modified_Request_CB(uint16_t Connection_Handle, uint16_t attr_handle, uint16_t Offset, uint8_t data_length, uint8_t *att_data)
{
  if(attr_handle == BatteryFeaturesCharHandle + 2){

    if (att_data[0] == 01) {
      uint32_t uhCapture = __HAL_TIM_GET_COUNTER(&TimCCHandle);

      W2ST_ON_CONNECTION(W2ST_CONNECT_BAT_EVENT);
      BSP_BC_CmdSend(BATMS_ON);

      /* Start the TIM Base generation in interrupt mode */
      if(HAL_TIM_OC_Start_IT(&TimCCHandle, TIM_CHANNEL_2) != HAL_OK){
        /* Starting Error */
        Error_Handler();
      }

      /* Set the Capture Compare Register value */
      __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_2, (uhCapture + STBOX1_UPDATE_BATTERY));
    } else if (att_data[0] == 0){
      W2ST_OFF_CONNECTION(W2ST_CONNECT_BAT_EVENT);
      BSP_BC_CmdSend(BATMS_OFF);

      /* Stop the TIM Base generation in interrupt mode */
      if(HAL_TIM_OC_Stop_IT(&TimCCHandle, TIM_CHANNEL_2) != HAL_OK){
        /* Stopping Error */
        Error_Handler();
      }
   }
#ifdef STBOX1_DEBUG_CONNECTION
   if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
     BytesToWrite =sprintf((char *)BufferToWrite,"--->Bat=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_BAT_EVENT) ? " ON" : " OFF");
     Term_Update(BufferToWrite,BytesToWrite);
   } else {
     STBOX1_PRINTF("--->Bat=%s", W2ST_CHECK_CONNECTION(W2ST_CONNECT_BAT_EVENT) ? " ON\r\n" : " OFF\r\n");
   }
#endif /* STBOX1_DEBUG_CONNECTION */

   } else if(attr_handle == ConfigCharHandle + 2){

      if (att_data[0] == 01) {
        W2ST_ON_CONNECTION(W2ST_CONNECT_CONF_EVENT);
      } else if (att_data[0] == 0){
        W2ST_OFF_CONNECTION(W2ST_CONNECT_CONF_EVENT);
      }
#ifdef STBOX1_DEBUG_CONNECTION
      if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
        BytesToWrite =sprintf((char *)BufferToWrite,"--->Conf=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT) ? "ON" : "OFF");
       Term_Update(BufferToWrite,BytesToWrite);
      } else {
        STBOX1_PRINTF("--->Conf=%s\r\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_CONF_EVENT) ? "ON" : "OFF");
      }
#endif /* STBOX1_DEBUG_CONNECTION */

  } else if (attr_handle == ConfigCharHandle + 1) {

     /* Received one write command from Client on Configuration characteristc */
     /* Do Nothing */

  } else if(attr_handle == MLCCharHandle + 2){

      if (att_data[0] == 01) {
        W2ST_ON_CONNECTION(W2ST_CONNECT_MLC);
         /* Start the LSM6DSOX MLC */
         Init_MEMS_MLC();
      } else if (att_data[0] == 0){
        W2ST_OFF_CONNECTION(W2ST_CONNECT_MLC);
        /* Stop the LSM6DSOX MLC */
        DeInit_MEMS();
      }
#ifdef STBOX1_DEBUG_CONNECTION
      if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
        BytesToWrite =sprintf((char *)BufferToWrite,"--->MLC=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_MLC) ? "ON" : "OFF");
       Term_Update(BufferToWrite,BytesToWrite);
      } else {
        STBOX1_PRINTF("--->MLC=%s\r\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_MLC) ? "ON" : "OFF");
      }
#endif /* STBOX1_DEBUG_CONNECTION */

  } else if (attr_handle == MLCCharHandle + 1) {

     /* Received one write command from Client on Machine Learning Core characteristc */
     STBOX1_PRINTF("Write On MLC=%s\r\n", att_data);

   } else if(attr_handle == FSMCharHandle + 2){

      if (att_data[0] == 01) {
        W2ST_ON_CONNECTION(W2ST_CONNECT_FSM);
        /* Start the LSM6DSOX FSM */
        Init_MEMS_FSM();
      } else if (att_data[0] == 0){

        W2ST_OFF_CONNECTION(W2ST_CONNECT_FSM);
        /* Stop the LSM6DSOX FSM */
        DeInit_MEMS();
      }
#ifdef STBOX1_DEBUG_CONNECTION
      if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
        BytesToWrite =sprintf((char *)BufferToWrite,"--->FSM=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_FSM) ? "ON" : "OFF");
       Term_Update(BufferToWrite,BytesToWrite);
      } else {
        STBOX1_PRINTF("--->FSM=%s\r\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_FSM) ? "ON" : "OFF");
      }
#endif /* STBOX1_DEBUG_CONNECTION */

  } else if (attr_handle == FSMCharHandle + 1) {

     /* Received one write command from Client on Finite State Machine characteristc */
     STBOX1_PRINTF("Write On FSM=%s\r\n", att_data);

  } else if(attr_handle == AccGyroCharHandle + 2) {

     if (att_data[0] == 01) {
       uint32_t uhCapture = __HAL_TIM_GET_COUNTER(&TimCCHandle);

       W2ST_ON_CONNECTION(W2ST_CONNECT_ACC_GYRO);

       /* Init the LSM6DSOX Acc/Gyro */
       Init_MEMS();

       /* Start the TIM Base generation in interrupt mode */
      if(HAL_TIM_OC_Start_IT(&TimCCHandle, TIM_CHANNEL_4) != HAL_OK){
        /* Starting Error */
        Error_Handler();
      }

      /* Set the Capture Compare Register value */
      __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_4, (uhCapture + STBOX1_UPDATE_INV));
    } else if (att_data[0] == 0) {
       W2ST_OFF_CONNECTION(W2ST_CONNECT_ACC_GYRO);

      /* Stop the TIM Base generation in interrupt mode */
      if(HAL_TIM_OC_Stop_IT(&TimCCHandle, TIM_CHANNEL_4) != HAL_OK){
        /* Stopping Error */
        Error_Handler();
      }

      /* Stop the LSM6DSOX Acc/Gyro */
      DeInit_MEMS();

    }
#ifdef STBOX1_DEBUG_CONNECTION
      if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
        BytesToWrite =sprintf((char *)BufferToWrite,"--->Acc/Gyro=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_ACC_GYRO) ? "ON" : "OFF");
       Term_Update(BufferToWrite,BytesToWrite);
      } else {
        STBOX1_PRINTF("--->Acc/Gyro=%s\r\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_ACC_GYRO) ? "ON" : "OFF");
      }
#endif /* STBOX1_DEBUG_CONNECTION */

  } else if(attr_handle == StdErrCharHandle + 2){

    if (att_data[0] == 01) {
      W2ST_ON_CONNECTION(W2ST_CONNECT_STD_ERR);
    } else if (att_data[0] == 0){
      W2ST_OFF_CONNECTION(W2ST_CONNECT_STD_ERR);
    }

  } else if(attr_handle == TermCharHandle + 2){

    if (att_data[0] == 01) {
      W2ST_ON_CONNECTION(W2ST_CONNECT_STD_TERM);
    } else if (att_data[0] == 0){
      W2ST_OFF_CONNECTION(W2ST_CONNECT_STD_TERM);
    }

  } else if (attr_handle == TermCharHandle + 1){

    uint32_t SendBackData =1; /* By default Answer with the same message received */
    /* Received one write from Client on Terminal characteristc */
    SendBackData = DebugConsoleCommandParsing(att_data,data_length);

    /* Send it back if it's not recognized */
    if(SendBackData) {
      Term_Update(att_data,data_length);
    }

  } else if (attr_handle == ActivityRecCharHandle + 2) {

    if (att_data[0] == 01) {
      W2ST_ON_CONNECTION(W2ST_CONNECT_AR_EVENT);
      /* Start the Activity Recognition of LSM6DSOX MLC */
      Init_MEMS_MLC();
    } else if (att_data[0] == 0) {
      W2ST_OFF_CONNECTION(W2ST_CONNECT_AR_EVENT);
      /* Stop the LSM6DSOX MLC */
      DeInit_MEMS();
    }
#ifdef STBOX1_DEBUG_CONNECTION
   if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
     BytesToWrite =sprintf((char *)BufferToWrite,"--->AR=%s\n", W2ST_CHECK_CONNECTION(W2ST_CONNECT_AR_EVENT) ? " ON" : " OFF");
     Term_Update(BufferToWrite,BytesToWrite);
   } else {
     STBOX1_PRINTF("--->AR=%s", W2ST_CHECK_CONNECTION(W2ST_CONNECT_AR_EVENT) ? " ON\r\n" : " OFF\r\n");
   }
#endif /* STBOX1_DEBUG_CONNECTION */
#ifdef BLE_FORCE_RESCAN

  }  else if (attr_handle==(0x0002+2)) {

    /* Force one UUID rescan */
    tBleStatus ret = BLE_STATUS_INSUFFICIENT_RESOURCES;
    uint8_t buff[4];

    /* Delete all the Handles from 0x0001 to 0xFFFF */
    STORE_LE_16(buff  ,0x0001);
    STORE_LE_16(buff+2,0xFFFF);

    ret = aci_gatt_update_char_value(0x0001,0x0002,0,4,buff);

    if (ret == BLE_STATUS_SUCCESS){
      STBOX1_PRINTF("UUID Rescan Forced\r\n");
    } else {
      STBOX1_PRINTF("Problem forcing UUID Rescan\r\n");
    }
#endif /* BLE_FORCE_RESCAN */

  } else {

    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_ERR)){
      BytesToWrite =sprintf((char *)BufferToWrite, "Notification UNKNOWN handle\n");
      Stderr_Update(BufferToWrite,BytesToWrite);
    } else {
      STBOX1_PRINTF("Notification UNKNOWN handle =%d\r\n",attr_handle);
    }
  }

}

/**
 * @brief  This function makes the parsing of the Debug Console Commands
 * @param uint8_t *att_data attribute data
 * @param uint8_t data_length length of the data
 * @retval uint32_t SendItBack true/false
 */
static uint32_t DebugConsoleCommandParsing(uint8_t * att_data, uint8_t data_length)
{
  uint32_t SendBackData = 1;

    /* Help Command */
    if(!strncmp("help",(char *)(att_data),4)) {
      /* Print Legend */
      SendBackData=0;

      BytesToWrite =sprintf((char *)BufferToWrite,
         "info\n"
         "getMLCLabels\n"
         "getFSMLabels\n"
#ifdef STBOX1_RESTART_DFU
         "DFU\n"
#endif /* STBOX1_RESTART_DFU */
         "Off\n");
      Term_Update(BufferToWrite,BytesToWrite);

    } else if(!strncmp("info",(char *)(att_data),4)) {
      SendBackData=0;

      BytesToWrite =sprintf((char *)BufferToWrite,"\r\nSTMicroelectronics %s:\n"
         "\tVersion %c.%c.%c\n"
        "\tSTM32L4R9ZI-SensorTile.box board"
          "\n",
          STBOX1_PACKAGENAME,
          STBOX1_VERSION_MAJOR,STBOX1_VERSION_MINOR,STBOX1_VERSION_PATCH);
      Term_Update(BufferToWrite,BytesToWrite);

      BytesToWrite =sprintf((char *)BufferToWrite,"\t(HAL %ld.%ld.%ld_%ld)\n"
        "\tCompiled %s %s"
#if defined (__IAR_SYSTEMS_ICC__)
        " (IAR)\n",
#elif defined (__CC_ARM)
        " (KEIL)\n",
#elif defined (__GNUC__)
        " (STM32CubeIDE)\n",
#endif
          HAL_GetHalVersion() >>24,
          (HAL_GetHalVersion() >>16)&0xFF,
          (HAL_GetHalVersion() >> 8)&0xFF,
           HAL_GetHalVersion()      &0xFF,
           __DATE__,__TIME__);
      Term_Update(BufferToWrite,BytesToWrite);
#ifdef STBOX1_RESTART_DFU
    } else if(!strncmp("DFU",(char *)(att_data),3)) {
      SendBackData=0;
      BytesToWrite =sprintf((char *)BufferToWrite,"\r\n5 sec for restarting\r\n\tin DFU mode\r\n");
      Term_Update(BufferToWrite,BytesToWrite);
      HAL_Delay(5000);
      DFU_Var = DFU_MAGIC_NUM;
      HAL_NVIC_SystemReset();
#endif /* STBOX1_RESTART_DFU */
    } else if(!strncmp("uid",(char *)(att_data),3)) {
      /* Write back the STM32 UID */
      uint8_t *uid = (uint8_t *)STM32_UUID;
      uint32_t MCU_ID = STM32_MCU_ID[0]&0xFFF;
      BytesToWrite =sprintf((char *)BufferToWrite,"%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X_%.3lX\n",
                            uid[ 3],uid[ 2],uid[ 1],uid[ 0],
                            uid[ 7],uid[ 6],uid[ 5],uid[ 4],
                            uid[11],uid[ 10],uid[9],uid[8],
                            MCU_ID);
      Term_Update(BufferToWrite,BytesToWrite);
      SendBackData=0;
    } else if(!strncmp("Off",(char *)(att_data),3)) {
      PowerOff=1;
      SendBackData=0;
    } else if(!strncmp("getMLCLabels",(char *)(att_data),12)) {
      BytesToWrite =sprintf((char *)BufferToWrite,"<MLC0_SRC>Activity Recognition,0='Stationary',1='Walking',4='Jogging',8='Biking',12='Driving';\n");
      Term_Update(BufferToWrite,BytesToWrite);
      SendBackData=0;
    } else if(!strncmp("getFSMLabels",(char *)(att_data),12)) {
      BytesToWrite =sprintf((char *)BufferToWrite,"<FSM_OUTS0>4D PosRec,0='N/A',16='Portrait Down',32='Portrait Up',64='Landscape Right',128='Landscape Left';\n");
      Term_Update(BufferToWrite,BytesToWrite);
      SendBackData=0;
    }
  return SendBackData;
}

/* ***************** BlueNRG-1 Stack Callbacks ********************************/

/*******************************************************************************
 * Function Name  : hci_le_connection_complete_event.
 * Description    : This event indicates that a new connection has been created.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_le_connection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Role,
                                      uint8_t Peer_Address_Type,
                                      uint8_t Peer_Address[6],
                                      uint16_t Conn_Interval,
                                      uint16_t Conn_Latency,
                                      uint16_t Supervision_Timeout,
                                      uint8_t Master_Clock_Accuracy)
{ 
  connection_handle = Connection_Handle;

  STBOX1_PRINTF(">>>>>>CONNECTED %x:%x:%x:%x:%x:%x\r\n",Peer_Address[5],Peer_Address[4],Peer_Address[3],Peer_Address[2],Peer_Address[1],Peer_Address[0]);

  ConnectionBleStatus=0;

  /* Stop the TIM Base generation in interrupt mode for Led Blinking*/
  if(HAL_TIM_OC_Stop_IT(&TimCCHandle, TIM_CHANNEL_1) != HAL_OK){
    /* Stopping Error */
    Error_Handler();
  }

  /* Just in order to be sure to switch off the User Led */
  LedOffTargetPlatform();

  HAL_Delay(100);

}/* end hci_le_connection_complete_event() */

/*******************************************************************************
 * Function Name  : hci_disconnection_complete_event.
 * Description    : This event occurs when a connection is terminated.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void hci_disconnection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Reason)
{  
  /* No Device Connected */
  connection_handle =0;
  
  STBOX1_PRINTF("<<<<<<DISCONNECTED\r\n");

  /* Make the device connectable again. */
  set_connectable = TRUE;
  ConnectionBleStatus=0;

  HAL_Delay(100);

}/* end hci_disconnection_complete_event() */

/*******************************************************************************
 * Function Name  : aci_gatt_read_permit_req_event.
 * Description    : This event is given when a read request is received
 *                  by the server from the client.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_read_permit_req_event(uint16_t Connection_Handle,
                                    uint16_t Attribute_Handle,
                                    uint16_t Offset)
{
  Read_Request_CB(Attribute_Handle);    
}

/*******************************************************************************
 * Function Name  : aci_gatt_attribute_modified_event.
 * Description    : This event is given when an attribute change his value.
 * Input          : See file bluenrg1_events.h
 * Output         : See file bluenrg1_events.h
 * Return         : See file bluenrg1_events.h
 *******************************************************************************/
void aci_gatt_attribute_modified_event(uint16_t Connection_Handle,
                                       uint16_t Attr_Handle,
                                       uint16_t Offset,
                                       uint16_t Attr_Data_Length,
                                       uint8_t Attr_Data[])
{
  Attribute_Modified_Request_CB(Connection_Handle, Attr_Handle, Offset, Attr_Data_Length, Attr_Data);
}
/******************* (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
