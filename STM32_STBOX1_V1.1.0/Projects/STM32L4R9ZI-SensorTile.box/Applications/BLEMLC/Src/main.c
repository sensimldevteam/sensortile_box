/**
  ******************************************************************************
  * @file    BLEMLC\Src\main.c
  * @author  SRA - Central Labs
  * @version V1.1.0
  * @date    10-Dec-2019
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2019 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/** 
 *
 * @page BLEMLC Example on how to program the Machine Learning Core and Finite State Machine
 * 
 * @image html st_logo.png
 *
 * <b>Introduction</b>
 *
 * This firmware package includes Components Device Drivers, Board Support Package
 * and example application for the following STMicroelectronics elements:
 * - STEVAL-MKSBOX1V1 (SensorTile.box) evaluation board that contains the following components:
 *     - MEMS sensor devices: HTS221, LPS22HH, LIS2MDL, LSM6DSOX
 *     - analog microphone
 *
 *
 * <b>Example Application</b>
 *
 * The Example application initializes all the Components, Loading the program on Machine Learning Core (MLC)
 * or Finite State Machine (FSM), initializes the Library creating some Custom Bluetooth services:
 * - The first service exposes all the HW/SW characteristics:
 *    - LSM6DSOX MLC results: Activity Recognition
 *    - LSM6DS0X accelerometer and Gyroscope values
 *    - LSM6DSOX MLC output registers (Only available starting from V4.6.0 of ST BLE Sensor Application)
 *    - LSM6DSOX FSM output registers (Only available starting from V4.6.0 of ST BLE Sensor Application)
 *    - Battery status
 * - The second Service exposes the console services where we have stdin/stdout and stderr capabilities
 * - The last Service is used for configuration purpose
 *
 * This example must be used with the related ST BLE Sensor Android/iOS application available on Play/itune store (Version 4.6.0 or higher),
 * in order to read the sent information by Bluetooth Low Energy protocol
 * 
 */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <math.h>
#include <limits.h>

#include "TargetFeatures.h"
#include "main.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Imported Variables --------------------------------------------------------*/
extern uint8_t set_connectable;

/* Exported Variables --------------------------------------------------------*/

uint8_t BufferToWrite[256];
int32_t BytesToWrite;

uint8_t bdaddr[6];
char BoardName[8]={NAME_BLUEMS,0};

HAR_output_t ActivityCode = HAR_ERROR;

#ifdef STBOX1_RESTART_DFU
  #if defined (__GNUC__)
    uint32_t DFU_Var __attribute__ ((section (".noinit")));
  #elif defined (__CC_ARM)
    uint32_t DFU_Var  __attribute__((at(0x2009FFF0)));
  #elif defined (__IAR_SYSTEMS_ICC__)
    __no_init uint32_t DFU_Var;
  #endif /* defined (__GNUC__) */
#endif /* STBOX1_RESTART_DFU */

/* Private variables ---------------------------------------------------------*/
static volatile int ButtonPressed   = 0;
static volatile int SendBatteryInfo = 0;
volatile        int PowerOff        = 0;
volatile        int RebootBoard     = 0;
volatile        int hci_event       = 0;
static volatile int BlinkLed        = 0;
static volatile int MEMSInterrupt   = 0;
static volatile int SendAccGyro     = 0;

/* Private function prototypes -----------------------------------------------*/

static void Init_BlueNRG_Custom_Services(void);
static void Init_BlueNRG_Stack(void);

static void ButtonCallback(void);
static void MEMSCallback(void);
static void SystemClock_Config(void);
static void SendBatteryInfoData(void);
static void AccGyro_Read(BSP_MOTION_SENSOR_Axes_t *ACC_Value,BSP_MOTION_SENSOR_Axes_t *GYR_Value);

void APP_UserEvtRx(void *pData);

#ifdef STBOX1_ENABLE_PRINTF
static void DisplayFirmwareInfo(void);
#endif /* STBOX1_ENABLE_PRINTF */

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{

#ifdef STBOX1_RESTART_DFU
  /* Check if we need to go to DFU */
  if(DFU_Var == DFU_MAGIC_NUM) {
    typedef void (*pFunction)(void);
    pFunction JumpToDFU;

    /* Reset the Magic Number */
    DFU_Var = 0x00000000;
    HAL_RCC_DeInit();
    SysTick->CTRL = 0;
    SysTick->LOAD = 0;
    SysTick->VAL = 0;
    __disable_irq();
    __HAL_SYSCFG_REMAPMEMORY_SYSTEMFLASH();
    JumpToDFU = (void (*)(void)) (*((uint32_t *)(0x1FFF0000 + 4)));
    /* Initialize user application's Stack Pointer */
    __set_MSP(*(__IO uint32_t*) 0x1FFF0000);
    JumpToDFU();
  }
#endif /* STBOX1_RESTART_DFU */

  /* HAL_Init */
  HAL_Init();

  /* Configure the System clock */
  SystemClock_Config();

  InitTargetPlatform();

#ifdef STBOX1_ENABLE_PRINTF
  DisplayFirmwareInfo();
#endif /* STBOX1_ENABLE_PRINTF */

  /* Initialize the BlueNRG */
  Init_BlueNRG_Stack();

  /* Initialize the BlueNRG Custom services */
  Init_BlueNRG_Custom_Services();

  while (1){
    if(set_connectable){
      uint32_t uhCapture = __HAL_TIM_GET_COUNTER(&TimCCHandle);
      set_connectable =0;
      setConnectable();

      /* Start the TIM Base generation in interrupt mode */
      if(HAL_TIM_OC_Start_IT(&TimCCHandle, TIM_CHANNEL_1) != HAL_OK){
        /* Starting Error */
        Error_Handler();
      }

      /* Set the Capture Compare Register value */
      __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_1, (uhCapture + STBOX1_UPDATE_LED));
    }

    /* Handle BLE event */
    if(hci_event) {
      hci_event =0;
      hci_user_evt_proc();
    }

    /* Handle user button */
    if(ButtonPressed) {
      ButtonCallback();
      ButtonPressed=0;
    }

    /* Power Off the SensorTile.box */
    if(PowerOff){
      BSP_BC_CmdSend(SHIPPING_MODE_ON);
      PowerOff =0;
    }

    /* Interrupt from LSM6DSOX */
    if(MEMSInterrupt) {
      MEMSCallback();
      MEMSInterrupt =0;
    }

    /* Reboot the Board */
    if(RebootBoard) {
      RebootBoard=0;
      HAL_NVIC_SystemReset();
    }

    /* Handle Acc/Gyro */
    if(SendAccGyro) {
      BSP_MOTION_SENSOR_Axes_t Acc,Gyro;
      SendAccGyro =0;
      AccGyro_Read(&Acc,&Gyro);
      AccGyro_Update(&Acc,&Gyro);
    }

    /* Battery Info Data */
    if(SendBatteryInfo) {
      SendBatteryInfo=0;
      SendBatteryInfoData();
    }

    /* Blinking the Led */
    if(BlinkLed) {
      BlinkLed = 0;
      LedToggleTargetPlatform();
    }

    /* Wait next event */
    __WFI();
  }
}

#ifdef STBOX1_ENABLE_PRINTF
/**
  * @brief Display all the Firmware Information
  * @param  None
  * @retval None
  */
static void DisplayFirmwareInfo(void)
{
    STBOX1_PRINTF("\r\n------------------------------------------------------------\r\n");
    STBOX1_PRINTF("STMicroelectronics %s\r\n"
         "\tVersion %c.%c.%c\r\n"
        "\tSTM32L4R9ZI-SensorTile.box board\r\n",
          STBOX1_PACKAGENAME,
          STBOX1_VERSION_MAJOR,STBOX1_VERSION_MINOR,STBOX1_VERSION_PATCH);

    STBOX1_PRINTF("\t(HAL %ld.%ld.%ld_%ld)\r\n\tCompiled %s %s"
#if defined (__IAR_SYSTEMS_ICC__)
        " (IAR)\r\n",
#elif defined (__CC_ARM)
        " (KEIL)\r\n",
#elif defined (__GNUC__)
        " (STM32CubeIDE)\r\n",
#endif
           HAL_GetHalVersion() >>24,
          (HAL_GetHalVersion() >>16)&0xFF,
          (HAL_GetHalVersion() >> 8)&0xFF,
           HAL_GetHalVersion()      &0xFF,
           __DATE__,__TIME__);
  STBOX1_PRINTF("------------------------------------------------------------\r\n");
}
#endif /* STBOX1_ENABLE_PRINTF */

/**
  * @brief  Read The Inertial Data (Acc/Gyro)
  * @param  BSP_MOTION_SENSOR_Axes_t ACC_Value Structure containing acceleration value in mg
  * @param  BSP_MOTION_SENSOR_Axes_t GYR_Value Structure containing Gyroscope value
  * @retval None
 */
static void AccGyro_Read(BSP_MOTION_SENSOR_Axes_t *ACC_Value,BSP_MOTION_SENSOR_Axes_t *GYR_Value)
{
  /* Read the Acc values */
  BSP_MOTION_SENSOR_GetAxes(LSM6DSOX_0,MOTION_ACCELERO,ACC_Value);

  /* Read the Gyro values */
  BSP_MOTION_SENSOR_GetAxes(LSM6DSOX_0,MOTION_GYRO,GYR_Value);
}

/**
  * @brief  Callback for user button
  * @param  None
  * @retval None
  */
static void ButtonCallback(void)
{
  STBOX1_PRINTF("User button pressed\r\n");
  STBOX1_PRINTF("The board will be switched off\r\n");
  PowerOff =1;
}

/**
  * @brief  Callback for LSM6DS0X interrupt
  * @param  None
  * @retval None
  */
static void MEMSCallback(void)
{
  lsm6dsox_all_sources_t      status;
  lsm6dsox_all_sources_get(LSM6DSOX_Contex, &status);
  if(*((uint8_t *)&(status.mlc_status))) {
    uint8_t mlc_out[8];
    lsm6dsox_mlc_out_get(LSM6DSOX_Contex, mlc_out);

    /* Check if we need to update the MLC BLE char */
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_MLC)) {
      MLC_Update(mlc_out,(uint8_t *)&(status.mlc_status));
    }

    /* Check if we need to update the Activity Recognition BLE char */
    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_AR_EVENT)) {
      if (status.mlc_status.is_mlc1) {
        ActivityCode = MappingToHAR_ouput_t[mlc_out[0]];
        if(ActivityCode!=HAR_ERROR) {
          ActivityRec_Update(ActivityCode);
          if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
            BytesToWrite =sprintf((char *)BufferToWrite,"Rec ActivityCode %02X [%02X]\n",ActivityCode,mlc_out[0]);
            Term_Update(BufferToWrite,BytesToWrite);
          } else {
            STBOX1_PRINTF("Rec ActivityCode %02X [%02X]\r\n",ActivityCode,mlc_out[0]);
          }
        } else {
          if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_STD_TERM)) {
            BytesToWrite =sprintf((char *)BufferToWrite,"Wrong ActivityCode %02X [%02X]\n",ActivityCode,mlc_out[0]);
            Term_Update(BufferToWrite,BytesToWrite);
          } else {
            STBOX1_PRINTF("Wrong ActivityCode %02X [%02X]\r\n",ActivityCode,mlc_out[0]);
          }
        }
      }
    }
  } else  if((*((uint8_t *)&(status.fsm_status_a))) |
             (*((uint8_t *)&(status.fsm_status_b)))) {
    lsm6dsox_fsm_out_t fsm_out;
    lsm6dsox_fsm_out_get(LSM6DSOX_Contex, &fsm_out);

    if(W2ST_CHECK_CONNECTION(W2ST_CONNECT_FSM)) {
      FSM_Update(&fsm_out,(uint8_t *)&(status.fsm_status_a),(uint8_t *)&(status.fsm_status_b));
    }
  }
}

/**
  * @brief  Send Battery Info Data (Voltage/Current/Soc) to BLE
  * @param  None
  * @retval None
  */
static void SendBatteryInfoData(void)
{
  uint32_t BatteryLevel;
  uint32_t Voltage;

  /* Read the Battery Charger voltage and Level values */
  BSP_BC_GetVoltageAndLevel(&Voltage,&BatteryLevel);

  Battery_Update(BatteryLevel,Voltage,0x04 /* Unknown */);
}


/** @brief Initialize the BlueNRG Stack
 * @param None
 * @retval None
 */
static void Init_BlueNRG_Stack(void)
{
  uint16_t service_handle, dev_name_char_handle, appearance_char_handle;
  tBleStatus ret;
  uint8_t data_len_out;

  /* Initialize the BlueNRG HCI */
  hci_init(APP_UserEvtRx, NULL);

  /* we will let the BLE chip to use its Random MAC address */
#define CONFIG_DATA_RANDOM_ADDRESS          (0x80) /**< Stored static random address. Read-only. */
  ret = aci_hal_read_config_data(CONFIG_DATA_RANDOM_ADDRESS, &data_len_out, bdaddr);

  if(ret != BLE_STATUS_SUCCESS){
    STBOX1_PRINTF("\r\nReading  Random BD_ADDR failed\r\n");
    goto fail;
  }

  ret = aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET,
                                  CONFIG_DATA_PUBADDR_LEN,
                                  bdaddr);

  if(ret != BLE_STATUS_SUCCESS){
     STBOX1_PRINTF("\r\nSetting Public BD_ADDR failed\r\n");
     goto fail;
  }

  ret = aci_gatt_init();
  if(ret != BLE_STATUS_SUCCESS){
    STBOX1_PRINTF("\r\nGATT_Init failed\r\n");
    goto fail;
  }

  ret = aci_gap_init(GAP_PERIPHERAL_ROLE, 0, strlen(BoardName), &service_handle, &dev_name_char_handle, &appearance_char_handle);
  

  if(ret != BLE_STATUS_SUCCESS){
     STBOX1_PRINTF("\r\nGAP_Init failed\r\n");
     goto fail;
  }

#ifdef STBOX1_ENABLE_PRINTF
  #ifdef STBOX1_RESTART_DFU
    /* Ask if we want to boot in DFU mode */
    {
      int32_t Answer =0;
      char Buffer[16];
      uint32_t InitTick;

      STBOX1_PRINTF("Do you want to reboot in DFU mode (y/n) [5sec]?");
      InitTick = HAL_GetTick();
      while(Answer==0) {
        if(VCOM_read(Buffer, 16)) {
          if(Buffer[0]=='y') {
            DFU_Var = DFU_MAGIC_NUM;
            HAL_NVIC_SystemReset();
          } else {
            STBOX1_PRINTF("\r\n\n");
            Answer=1;
          }
        } else {
          HAL_Delay(10);
          if((HAL_GetTick()-InitTick)>5000) {
            STBOX1_PRINTF("\r\n\tTimeOut\r\n");
            Answer=1;
          }
        }
      }
    }
  #endif /* STBOX1_RESTART_DFU */

  /* Ask if we want to change the board Name (timeout of 5 seconds )*/
  {
    int32_t Answer =0;
    char Buffer[16];
    uint32_t InitTick;

    STBOX1_PRINTF("Default BLE board Name [%s]\r\n",BoardName);
    STBOX1_PRINTF("\tDo you want change it (y/n) [5sec]?");
    InitTick = HAL_GetTick();
    while(Answer==0) {
      if(VCOM_read(Buffer, 16)) {
        if(Buffer[0]=='y') {
          int32_t NumBytes=0;
          STBOX1_PRINTF("\r\n\tWrite the Name (7 Chars):");
          while(NumBytes!=7) {
            if(VCOM_read(Buffer, 16)) {
              BoardName[NumBytes]=Buffer[0];
              NumBytes++;
            }
          }
          STBOX1_PRINTF("\r\n\tNew Name=[%s]\r\n\n",BoardName);
          Answer=1;
        } else {
          STBOX1_PRINTF("\r\n\n");
          Answer=1;
        }
      } else {
        HAL_Delay(10);
        if((HAL_GetTick()-InitTick)>5000) {
          STBOX1_PRINTF("\r\n\tTimeOut\r\n");
          Answer=1;
        }
      }
    }
  }
#endif /* STBOX1_ENABLE_PRINTF */

  ret = aci_gatt_update_char_value(service_handle, dev_name_char_handle, 0,
                                   strlen(BoardName), (uint8_t *)BoardName);

  if(ret != BLE_STATUS_SUCCESS){
     STBOX1_PRINTF("\r\naci_gatt_update_char_value failed\r\n");
    while(1);
  }

  ret = aci_gap_set_authentication_requirement(BONDING,
                                               MITM_PROTECTION_REQUIRED,
                                               SC_IS_SUPPORTED,
                                               KEYPRESS_IS_NOT_SUPPORTED,
                                               7, 
                                               16,
                                               USE_FIXED_PIN_FOR_PAIRING,
                                               123456,
                                               0x00);
  if (ret != BLE_STATUS_SUCCESS) {
     STBOX1_PRINTF("\r\nGAP setting Authentication failed\r\n");
     goto fail;
  }

  STBOX1_PRINTF("\r\nSERVER: BLE Stack Initialized \r\n"
         "\t\tBoardName= %s\r\n"
         "\t\tBoardMAC = %x:%x:%x:%x:%x:%x\r\n",
         BoardName,
         bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]);

  /* Set output power level */
  aci_hal_set_tx_power_level(1,4); /* -2,1 dBm */

  return;

fail:
  return;
}

/** @brief HCI Transport layer user function
  * @param void *pData pointer to HCI event data
  * @retval None
  */
void APP_UserEvtRx(void *pData)
{
  uint32_t i;

  hci_spi_pckt *hci_pckt = (hci_spi_pckt *)pData;

  if(hci_pckt->type == HCI_EVENT_PKT) {
    hci_event_pckt *event_pckt = (hci_event_pckt*)hci_pckt->data;

    if(event_pckt->evt == EVT_LE_META_EVENT) {
      evt_le_meta_event *evt = (void *)event_pckt->data;

      for (i = 0; i < (sizeof(hci_le_meta_events_table)/sizeof(hci_le_meta_events_table_type)); i++) {
        if (evt->subevent == hci_le_meta_events_table[i].evt_code) {
          hci_le_meta_events_table[i].process((void *)evt->data);
        }
      }
    } else if(event_pckt->evt == EVT_VENDOR) {
      evt_blue_aci *blue_evt = (void*)event_pckt->data;        

      for (i = 0; i < (sizeof(hci_vendor_specific_events_table)/sizeof(hci_vendor_specific_events_table_type)); i++) {
        if (blue_evt->ecode == hci_vendor_specific_events_table[i].evt_code) {
          hci_vendor_specific_events_table[i].process((void *)blue_evt->data);
        }
      }
    } else {
      for (i = 0; i < (sizeof(hci_events_table)/sizeof(hci_events_table_type)); i++) {
        if (event_pckt->evt == hci_events_table[i].evt_code) {
          hci_events_table[i].process((void *)event_pckt->data);
        }
      }
    }
  }
}

/** @brief Initialize all the Custom BlueNRG services
 * @param None
 * @retval None
 */
static void Init_BlueNRG_Custom_Services(void)
{
  int ret;

  ret = Add_HW_SW_ServW2ST_Service();
  if(ret == BLE_STATUS_SUCCESS) {
     STBOX1_PRINTF("HW & SW Service W2ST added successfully\r\n");
  } else {
     STBOX1_PRINTF("\r\nError while adding HW & SW Service W2ST\r\n");
  }

  ret = Add_ConsoleW2ST_Service();
  if(ret == BLE_STATUS_SUCCESS) {
     STBOX1_PRINTF("Console Service W2ST added successfully\r\n");
  } else {
     STBOX1_PRINTF("\r\nError while adding Console Service W2ST\r\n");
  }

  ret = Add_ConfigW2ST_Service();
  if(ret == BLE_STATUS_SUCCESS) {
     STBOX1_PRINTF("Config  Service W2ST added successfully\r\n");
  } else {
     STBOX1_PRINTF("\r\nError while adding Config Service W2ST\r\n");
  }
}

/**
  * @brief  System Clock tree configuration
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST) != HAL_OK) {
    /* Initialization Error */
    while(1);
  }
  
   /**Configure LSE Drive Capability 
    */
  HAL_PWR_EnableBkUpAccess();

  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|
                                     RCC_OSCILLATORTYPE_LSE  |
                                     RCC_OSCILLATORTYPE_HSE  |
                                     RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_11;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 60;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
    /* Initialization Error */
    while(1);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK   |
                                RCC_CLOCKTYPE_SYSCLK |
                                RCC_CLOCKTYPE_PCLK1  |
                                RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK) {
    /* Initialization Error */
    while(1);
  }
  
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_SAI1   |
                                      RCC_PERIPHCLK_DFSDM1 |
                                      RCC_PERIPHCLK_USB    |
                                      RCC_PERIPHCLK_RTC    |
                                      RCC_PERIPHCLK_SDMMC1 |
                                      RCC_PERIPHCLK_ADC;

  PeriphClkInit.Sai1ClockSelection = RCC_SAI1CLKSOURCE_PLLSAI1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;  
  PeriphClkInit.Dfsdm1ClockSelection = RCC_DFSDM1CLKSOURCE_PCLK2;  
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.Sdmmc1ClockSelection = RCC_SDMMC1CLKSOURCE_PLLP;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSE;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 5;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 96;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV25;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV4;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV4;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK|RCC_PLLSAI1_SAI1CLK;  
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
    /* Initialization Error */
    while(1);
  }
}

/**
  * @brief This function provides accurate delay (in milliseconds) based
  *        on variable incremented.
  * @note This is a user implementation using WFI state
  * @param Delay: specifies the delay time length, in milliseconds.
  * @retval None
  */
void HAL_Delay(__IO uint32_t Delay)
{
  uint32_t tickstart = 0;
  tickstart = HAL_GetTick();
  while((HAL_GetTick() - tickstart) < Delay){
    __WFI();
  }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  STBOX1_PRINTF("Error_Handler\r\n");
  /* User may add here some code to deal with this error */
  while(1){
  }
}

/**
 * @brief  EXTI line detection callback.
 * @param  uint16_t GPIO_Pin Specifies the pin connected EXTI line
 * @retval None
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  switch(GPIO_Pin) {
    case HCI_TL_SPI_EXTI_PIN:
      hci_tl_lowlevel_isr();
      hci_event=1;
    break;
    case USER_BUTTON_PIN:
      ButtonPressed = 1;    
    break;
    case GPIO_PIN_2:
    case GPIO_PIN_3:
      MEMSInterrupt = 1;
    break;
  }
}


/**
  * @brief  Output Compare callback in non blocking mode
  * @param  TIM_HandleTypeDef *htim TIM OC handle
  * @retval None
  */
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
  uint32_t uhCapture=0;
  /* TIM1_CH1 toggling with frequency = 1Hz */
  if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1) {
    uhCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);
    /* Set the Capture Compare Register value */
    __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_1, (uhCapture + STBOX1_UPDATE_LED));

    BlinkLed =1;
  }

  /* TIM1_CH2 toggling with frequency = 0.5Hz */
  if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2) {
    uhCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);
    /* Set the Capture Compare Register value */
    __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_2, (uhCapture + STBOX1_UPDATE_BATTERY));
    SendBatteryInfo=1;
  }

#ifdef STBOX1_ENABLE_PRINTF
  if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_3) {
    uhCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3);
    /* Set the Capture Compare Register value */
    __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_3, (uhCapture + STBOX1_UPDATE_VCOM));
    CDC_PeriodElapsedCallback();
  }
#endif /* STBOX1_ENABLE_PRINTF */

  /* TIM1_CH4 toggling with frequency = 0.5Hz */
  if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_4) {
    uhCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4);
    /* Set the Capture Compare Register value */
    __HAL_TIM_SET_COMPARE(&TimCCHandle, TIM_CHANNEL_4, (uhCapture + STBOX1_UPDATE_INV));
    SendAccGyro=1;
  }
}


#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: STBOX1_PRINTF("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1){
  }
}
#endif

/******************* (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
