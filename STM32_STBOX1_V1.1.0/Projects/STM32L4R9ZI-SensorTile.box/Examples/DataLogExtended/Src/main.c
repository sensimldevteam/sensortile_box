/**
  ******************************************************************************
  * @file    DataLogExtended\Src\main.c
  * @author  MEMS Application Team
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2019 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/**
 *
 * @page DataLogExtended For visualizing Sensor's data with Unicleo-GUI
 *
 * @image html st_logo.png
 *
 * <b>Introduction</b>
 *
 * Main function is to show how to use SensorTile.box to send data using USB
 * to a connected PC or Desktop and display it on specific application Unicleo-Gui,
 * which is developed by STMicroelectronics and provided separately, not in binary with this package.
 * After connection has been established:
 * - the user can view the data from various on-board environment sensors like Temperature,
 * Humidity, and Pressure.
 * - the user can also view data from various on-board MEMS sensors as well like Accelerometer,
 * Gyroscope, and Magnetometer.
 * - the user can also visualize this data as graphs using Unicleo application.
 *
 * 
 */

/* Includes ------------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>
#include <math.h>
#include "main.h"
#include "vcom.h"
#include "DemoSerial.h"

/* Private typedef -----------------------------------------------------------*/

typedef struct displayFloatToInt_s 
{
  int8_t   sign;     /* 0 means positive, 1 means negative*/
  uint32_t out_int;
  uint32_t out_dec;
} displayFloatToInt_t;

/* Private define ------------------------------------------------------------*/
#define MAX_BUF_SIZE 256

#define MCR_HEART_BIT()  \
{                        \
  BSP_LED_On(LED_BLUE);  \
  BSP_LED_On(LED_GREEN); \
  HAL_Delay(200);        \
  BSP_LED_Off(LED_BLUE); \
  BSP_LED_Off(LED_GREEN);\
  HAL_Delay(400);        \
  BSP_LED_On(LED_BLUE);  \
  BSP_LED_On(LED_GREEN); \
  HAL_Delay(200);        \
  BSP_LED_Off(LED_BLUE); \
  BSP_LED_Off(LED_GREEN);\
  HAL_Delay(1000);       \
}

/* Extern variables ----------------------------------------------------------*/
extern volatile uint8_t DataLoggerActive; /*!< DataLogger Flag */

extern uint32_t AccInstance;
extern uint32_t GyrInstance;
extern uint32_t MagInstance;
extern uint32_t HumInstance;
extern uint32_t TmpInstance;
extern uint32_t PrsInstance;

extern volatile uint32_t SensorsEnabled; /* This "redundant" line is here to fulfil MISRA C-2012 rule 8.4 */
volatile uint32_t SensorsEnabled = 0;    /*!< Enable Sensor Flag */
volatile uint8_t IntStatus = 0;

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static uint8_t  TerminalActive           = 0;  /*!< COM port terminal active */
static uint32_t Previous_Sensors_Enabled = 0;  /*!< Previously Stored Sensors Enabled/Disabled Flag */

static int RtcSynchPrediv;
static char DataOut[MAX_BUF_SIZE];              /*!< DataOut Frame */
static RTC_HandleTypeDef RtcHandle;             /*!< RTC HANDLE */
static volatile uint8_t AutoInit = 0;           /*!< Auto Init */
static uint8_t NewData = 0;
static uint8_t NewDataFlags = 0;

/* Private function prototypes -----------------------------------------------*/
static void GPIO_Init(void);

static void RTC_Config(void);
static void RTC_TimeStampConfig(void);

static void Initialize_All_Sensors(void);
static void Enable_Disable_Sensors(void);
static void Float_To_Int(float In, displayFloatToInt_t *OutValue, int32_t DecPrec);

static void RTC_Handler(TMsg *Msg);
static void Accelero_Sensor_Handler(TMsg *Msg, uint32_t Instance);
static void Gyro_Sensor_Handler(TMsg *Msg, uint32_t Instance);
static void Magneto_Sensor_Handler(TMsg *Msg, uint32_t Instance);
static void Press_Sensor_Handler(TMsg *Msg, uint32_t Instance);
static void Hum_Sensor_Handler(TMsg *Msg, uint32_t Instance);
static void Temp_Sensor_Handler(TMsg *Msg, uint32_t Instance);
static void Sensors_Interrupt_Handler(TMsg *Msg);
static void MLC_Handler(TMsg *Msg);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main function is to show how to SensorTile.box to send data
  *         using a connected PC or Desktop and display it on generic applications like
  *         TeraTerm and specific application like Unicleo GUI, which is developed by STMicroelectronics
  *         and provided with a separated package.
  *
  *         After connection has been established:
  *         - the user can view the data from various on-board environment sensors like Temperature, Humidity, and Pressure
  *         - the user can also view data from various on-board MEMS sensors as well like Accelerometer, Gyrometer, and Magnetometer
  *         - the user can also visualize this data as graphs using Sensors_DataLog application provided with this package
  *
  * @param  None
  * @retval None
  */
int main(void)
{
  TMsg msg_dat;
  TMsg msg_cmd;

  /* STM32L4xx HAL library initialization:
  - Configure the Flash prefetch, instruction and Data caches
  - Configure the Systick to generate an interrupt each 1 msec
  - Set NVIC Group Priority to 4
  - Global MSP (MCU Support Package) initialization
   */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize LEDs */
  BSP_LED_Init(LED_BLUE);
  BSP_LED_Init(LED_GREEN);

  MCR_HEART_BIT();

  BSP_LED_On(LED_GREEN);

  /* Initialize GPIO */
  GPIO_Init();

  /* Initialize VCOM (Virtual COM Port) */
  VCOM_init();

  /* Initialize RTC */
  RTC_Config();
  RTC_TimeStampConfig();

  /* Initialize sensors */
  Initialize_All_Sensors();

  while (1)
  {
    if (VCOM_receive_MSG(&msg_cmd, TMsg_MaxLen) == OK)
    {
      if (msg_cmd.Data[0] == DEV_ADDR)
      {
        HandleMSG((TMsg *)&msg_cmd);
        if (DataLoggerActive)
        {
          TerminalActive = 0;
        }
      }
    }

    if (Previous_Sensors_Enabled != SensorsEnabled)
    {
      Previous_Sensors_Enabled = SensorsEnabled;
      Enable_Disable_Sensors();
    }

    RTC_Handler(&msg_dat);

    if ((SensorsEnabled & ACCELEROMETER_SENSOR) == ACCELEROMETER_SENSOR)
    {
      Accelero_Sensor_Handler(&msg_dat, AccInstance);
    }
    if ((SensorsEnabled & GYROSCOPE_SENSOR) == GYROSCOPE_SENSOR)
    {
      Gyro_Sensor_Handler(&msg_dat, GyrInstance);
    }
    if ((SensorsEnabled & MAGNETIC_SENSOR) == MAGNETIC_SENSOR)
    {
      Magneto_Sensor_Handler(&msg_dat, MagInstance);
    }
    if ((SensorsEnabled & HUMIDITY_SENSOR) == HUMIDITY_SENSOR)
    {
      Hum_Sensor_Handler(&msg_dat, HumInstance);
    }
    if ((SensorsEnabled & TEMPERATURE_SENSOR) == TEMPERATURE_SENSOR)
    {
      Temp_Sensor_Handler(&msg_dat, TmpInstance);
    }
    if ((SensorsEnabled & PRESSURE_SENSOR) == PRESSURE_SENSOR)
    {
      Press_Sensor_Handler(&msg_dat, PrsInstance);
    }
    
    Sensors_Interrupt_Handler(&msg_dat);
    
    if (DataLoggerActive != 0U)
    {
      MLC_Handler(&msg_dat);
    }

    if (DataLoggerActive || TerminalActive)
    {
      BSP_LED_Toggle(LED_BLUE);
    }
    else
    {
      BSP_LED_Off(LED_BLUE);
    }

    if(DataLoggerActive)
    {
      if (NewData != 0)
      {
        INIT_STREAMING_HEADER(&msg_dat);
        msg_dat.Data[55] = NewDataFlags;
        msg_dat.Len = STREAMING_MSG_LENGTH;
        VCOM_send_MSG(&msg_dat);
        NewData = 0;
        NewDataFlags = 0;
      }
    }

    if (TerminalActive)
    {
      HAL_Delay(500);
    }
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  
  /* Interrupt pins SensorTile.Box */
  // LSM6DSOX INT1                      PA2
  // LSM6DSOX INT2                      PE3
  // LPS22HH INT                        PD15
  // LIS2DW12 INT1                      PC5
  // LIS2DW12 INT2                      PD14
  // LIS3DHH INT1                       PC13
  // LIS3DHH INT2                       PE6
  // LIS2MDL INT                        PD12
  // HTS221                             PD13
  // STTS751                            PG5
  
  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /* Configure GPIO pins : PA2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  
  /* Configure GPIO pins : PC5 PC13*/
  GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
  
  /* Configure GPIO pins : PD12 PD13 PD14 PD15 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
  
  /* Configure GPIO pins : PE3 PE6 */
  GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
  
  /* Configure GPIO pins : PG5 */
  GPIO_InitStruct.Pin = GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);
}

/**
  * @brief  Initialize all sensors
  * @param  None
  * @retval None
  */
static void Initialize_All_Sensors(void)
{
  (void)BSP_MOTION_SENSOR_Init(AccInstance, MOTION_ACCELERO);
  (void)BSP_MOTION_SENSOR_Init(GyrInstance, MOTION_GYRO);
  (void)BSP_MOTION_SENSOR_Init(MagInstance, MOTION_MAGNETO);
  (void)BSP_ENV_SENSOR_Init(HumInstance, ENV_HUMIDITY);
  (void)BSP_ENV_SENSOR_Init(TmpInstance, ENV_TEMPERATURE);
  (void)BSP_ENV_SENSOR_Init(PrsInstance, ENV_PRESSURE);
}

/**
 * @brief  Enable/disable desired sensors
 * @param  None
 * @retval None
 */
static void Enable_Disable_Sensors(void)
{
  if ((SensorsEnabled & ACCELEROMETER_SENSOR) == ACCELEROMETER_SENSOR)
  {
    (void)BSP_MOTION_SENSOR_Enable(AccInstance, MOTION_ACCELERO);
  }
  else
  {
    (void)BSP_MOTION_SENSOR_Disable(AccInstance, MOTION_ACCELERO);
  }
  
  if ((SensorsEnabled & GYROSCOPE_SENSOR) == GYROSCOPE_SENSOR)
  {
    (void)BSP_MOTION_SENSOR_Enable(GyrInstance, MOTION_GYRO);
  }
  else
  {
    (void)BSP_MOTION_SENSOR_Disable(GyrInstance, MOTION_GYRO);
  }
  
  if ((SensorsEnabled & MAGNETIC_SENSOR) == MAGNETIC_SENSOR)
  {
    (void)BSP_MOTION_SENSOR_Enable(MagInstance, MOTION_MAGNETO);
  }
  else
  {
    (void)BSP_MOTION_SENSOR_Disable(MagInstance, MOTION_MAGNETO);
  }
  
  if ((SensorsEnabled & HUMIDITY_SENSOR) == HUMIDITY_SENSOR)
  {
    (void)BSP_ENV_SENSOR_Enable(HumInstance, ENV_HUMIDITY);
  }
  else
  {
    (void)BSP_ENV_SENSOR_Disable(HumInstance, ENV_HUMIDITY);
  }
  
  if ((SensorsEnabled & TEMPERATURE_SENSOR) == TEMPERATURE_SENSOR)
  {
    (void)BSP_ENV_SENSOR_Enable(TmpInstance, ENV_TEMPERATURE);
  }
  else
  {
    (void)BSP_ENV_SENSOR_Disable(TmpInstance, ENV_TEMPERATURE);
  }
  
  if ((SensorsEnabled & PRESSURE_SENSOR) == PRESSURE_SENSOR)
  {
    (void)BSP_ENV_SENSOR_Enable(PrsInstance, ENV_PRESSURE);
  }
  else
  {
    (void)BSP_ENV_SENSOR_Disable(PrsInstance, ENV_PRESSURE);
  }
}

/**
 * @brief  Handles the time+date getting/sending
 * @param  Msg the time+date part of the stream
 * @retval None
 */
static void RTC_Handler(TMsg *Msg)
{
  uint8_t sub_sec;
  uint32_t ans_uint32;
  int32_t ans_int32;
  RTC_DateTypeDef sdatestructureget;
  RTC_TimeTypeDef stimestructure;

  if (DataLoggerActive != 0U)
  {
    (void)HAL_RTC_GetTime(&RtcHandle, &stimestructure, FORMAT_BIN);
    (void)HAL_RTC_GetDate(&RtcHandle, &sdatestructureget, FORMAT_BIN);

    /* To be MISRA C-2012 compliant the original calculation:
       sub_sec = ((((((int)RtcSynchPrediv) - ((int)stimestructure.SubSeconds)) * 100) / (RtcSynchPrediv + 1)) & 0xFF);
       has been split to separate expressions */
    ans_int32 = (RtcSynchPrediv - (int32_t)stimestructure.SubSeconds) * 100;
    ans_int32 /= RtcSynchPrediv + 1;
    ans_uint32 = (uint32_t)ans_int32 & 0xFFU;
    sub_sec = (uint8_t)ans_uint32;

    Msg->Data[3] = (uint8_t)stimestructure.Hours;
    Msg->Data[4] = (uint8_t)stimestructure.Minutes;
    Msg->Data[5] = (uint8_t)stimestructure.Seconds;
    Msg->Data[6] = sub_sec;
  }
  else if (AutoInit != 0U)
  {
    (void)HAL_RTC_GetTime(&RtcHandle, &stimestructure, FORMAT_BIN);
    (void)HAL_RTC_GetDate(&RtcHandle, &sdatestructureget, FORMAT_BIN);

    /* To be MISRA C-2012 compliant the original calculation:
       sub_sec = ((((((int)RtcSynchPrediv) - ((int)stimestructure.SubSeconds)) * 100) / (RtcSynchPrediv + 1)) & 0xFF);
       has been split to separate expressions */
    ans_int32 = (RtcSynchPrediv - (int32_t)stimestructure.SubSeconds) * 100;
    ans_int32 /= RtcSynchPrediv + 1;
    ans_uint32 = (uint32_t)ans_int32 & 0xFFU;
    sub_sec = (uint8_t)ans_uint32;

    (void)snprintf(DataOut, MAX_BUF_SIZE, "TimeStamp: %d:%d:%d.%d\r\n", stimestructure.Hours, stimestructure.Minutes,
                   stimestructure.Seconds, sub_sec);
    (void)VCOM_write(DataOut, (uint16_t)strlen(DataOut));
  }
  else
  {
    /* Nothing to do */
  }
}

/**
 * @brief  Handles the ACCELERO axes data getting/sending
 * @param  Msg the ACCELERO part of the stream
 * @param  Instance the device instance
 * @retval None
 */
static void Accelero_Sensor_Handler(TMsg *Msg, uint32_t Instance)
{
  int32_t data[6];
  BSP_MOTION_SENSOR_Axes_t acceleration;
  uint8_t status = 0;

  if (BSP_MOTION_SENSOR_Get_DRDY_Status(Instance, MOTION_ACCELERO, &status) == BSP_ERROR_NONE && status == 1U)
  {
    NewData++;
    NewDataFlags |= 1U;

    (void)BSP_MOTION_SENSOR_GetAxes(Instance, MOTION_ACCELERO, &acceleration);

    if (DataLoggerActive != 0U)
    {
      Serialize_s32(&Msg->Data[19], acceleration.x, 4);
      Serialize_s32(&Msg->Data[23], acceleration.y, 4);
      Serialize_s32(&Msg->Data[27], acceleration.z, 4);
    }
    else if (AutoInit != 0U)
    {
      data[0] = acceleration.x;
      data[1] = acceleration.y;
      data[2] = acceleration.z;

      (void)snprintf(DataOut, MAX_BUF_SIZE, "ACC_X: %d, ACC_Y: %d, ACC_Z: %d\r\n", (int)data[0], (int)data[1], (int)data[2]);
      (void)VCOM_write(DataOut, (uint16_t)strlen(DataOut));
    }
    else
    {
      /* Nothing to do */
    }
  }
}

/**
 * @brief  Handles the GYRO axes data getting/sending
 * @param  Msg the GYRO part of the stream
 * @param  Instance the device instance
 * @retval None
 */
static void Gyro_Sensor_Handler(TMsg *Msg, uint32_t Instance)
{
  int32_t data[6];
  BSP_MOTION_SENSOR_Axes_t angular_velocity;
  uint8_t status = 0;

  if (BSP_MOTION_SENSOR_Get_DRDY_Status(Instance, MOTION_GYRO, &status) == BSP_ERROR_NONE && status == 1U)
  {
    NewData++;
    NewDataFlags |= 2U;

    (void)BSP_MOTION_SENSOR_GetAxes(Instance, MOTION_GYRO, &angular_velocity);

    if (DataLoggerActive != 0U)
    {
      Serialize_s32(&Msg->Data[31], angular_velocity.x, 4);
      Serialize_s32(&Msg->Data[35], angular_velocity.y, 4);
      Serialize_s32(&Msg->Data[39], angular_velocity.z, 4);
    }
    else if (AutoInit != 0U)
    {
      data[0] = angular_velocity.x;
      data[1] = angular_velocity.y;
      data[2] = angular_velocity.z;

      (void)snprintf(DataOut, MAX_BUF_SIZE, "GYR_X: %d, GYR_Y: %d, GYR_Z: %d\r\n", (int)data[0], (int)data[1], (int)data[2]);
      (void)VCOM_write(DataOut, (uint16_t)strlen(DataOut));
    }
    else
    {
      /* Nothing to do */
    }
  }
}

/**
 * @brief  Handles the MAGNETO axes data getting/sending
 * @param  Msg the MAGNETO part of the stream
 * @param  Instance the device instance
 * @retval None
 */
static void Magneto_Sensor_Handler(TMsg *Msg, uint32_t Instance)
{
  int32_t data[3];
  BSP_MOTION_SENSOR_Axes_t magnetic_field;
  uint8_t status = 0;

  if (BSP_MOTION_SENSOR_Get_DRDY_Status(Instance, MOTION_MAGNETO, &status) == BSP_ERROR_NONE && status == 1U)
  {
    NewData++;
    NewDataFlags |= 4U;

    (void)BSP_MOTION_SENSOR_GetAxes(Instance, MOTION_MAGNETO, &magnetic_field);

    if (DataLoggerActive != 0U)
    {
      Serialize_s32(&Msg->Data[43], (int32_t)magnetic_field.x, 4);
      Serialize_s32(&Msg->Data[47], (int32_t)magnetic_field.y, 4);
      Serialize_s32(&Msg->Data[51], (int32_t)magnetic_field.z, 4);
    }
    else if (AutoInit != 0U)
    {
      data[0] = magnetic_field.x;
      data[1] = magnetic_field.y;
      data[2] = magnetic_field.z;

      (void)snprintf(DataOut, MAX_BUF_SIZE, "MAG_X: %d, MAG_Y: %d, MAG_Z: %d\r\n", (int)data[0], (int)data[1], (int)data[2]);
      (void)VCOM_write(DataOut, (uint16_t)strlen(DataOut));
    }
    else
    {
      /* Nothing to do */
    }
  }
}


/**
 * @brief  Handles the PRESSURE sensor data getting/sending
 * @param  Msg the PRESSURE part of the stream
 * @param  Instance the device instance
 * @retval None
 */
static void Press_Sensor_Handler(TMsg *Msg, uint32_t Instance)
{
  float pressure;
  uint8_t status = 0;

  if (BSP_ENV_SENSOR_Get_DRDY_Status(Instance, ENV_PRESSURE, &status) == BSP_ERROR_NONE && status == 1U)
  {
    NewData++;
    NewDataFlags |= 8U;

    (void)BSP_ENV_SENSOR_GetValue(Instance, ENV_PRESSURE, &pressure);

    if (DataLoggerActive != 0U)
    {
      (void)memcpy(&Msg->Data[7], (void *)&pressure, sizeof(float));
    }
    else if (AutoInit != 0U)
    {
      displayFloatToInt_t out_value;
      Float_To_Int(pressure, &out_value, 2);
      (void)snprintf(DataOut, MAX_BUF_SIZE, "PRESS: %d.%02d\r\n", (int)out_value.out_int, (int)out_value.out_dec);
      (void)VCOM_write(DataOut, (uint16_t)strlen(DataOut));
    }
    else
    {
      /* Nothing to do */
    }
  }
}

/**
 * @brief  Handles the HUMIDITY sensor data getting/sending
 * @param  Msg the HUMIDITY part of the stream
 * @param  Instance the device instance
 * @retval None
 */
static void Hum_Sensor_Handler(TMsg *Msg, uint32_t Instance)
{
  float humidity;
  uint8_t status = 0;

  if (BSP_ENV_SENSOR_Get_DRDY_Status(Instance, ENV_HUMIDITY, &status) == BSP_ERROR_NONE && status == 1U)
  {
    NewData++;
    NewDataFlags |= 16U;

    (void)BSP_ENV_SENSOR_GetValue(Instance, ENV_HUMIDITY, &humidity);

    if (DataLoggerActive != 0U)
    {
      (void)memcpy(&Msg->Data[15], (void *)&humidity, sizeof(float));
    }
    else if (AutoInit != 0U)
    {
      displayFloatToInt_t out_value;
      Float_To_Int(humidity, &out_value, 2);
      (void)snprintf(DataOut, MAX_BUF_SIZE, "HUM: %d.%02d\r\n", (int)out_value.out_int, (int)out_value.out_dec);
      (void)VCOM_write(DataOut, (uint16_t)strlen(DataOut));
    }
    else
    {
      /* Nothing to do */
    }
  }
}

/**
 * @brief  Handles the TEMPERATURE sensor data getting/sending
 * @param  Msg the TEMPERATURE part of the stream
 * @param  Instance the device instance
 * @retval None
 */
static void Temp_Sensor_Handler(TMsg *Msg, uint32_t Instance)
{
  float temperature;
  uint8_t status = 0;
  uint8_t drdy = 0;
  static uint8_t stts751_is_busy = 0;

  if (Instance == STTS751_0)
  {
    if (BSP_ENV_SENSOR_Get_DRDY_Status(Instance, ENV_TEMPERATURE, &status) == BSP_ERROR_NONE)
    {
      if (status == 0)
      {
        stts751_is_busy = 1;
        drdy = 0;
      }
      else
      {
        if (stts751_is_busy == 1)
        {
          stts751_is_busy = 0;
          drdy = 1;
        }
      }
    }
  }
  else
  {
    if (BSP_ENV_SENSOR_Get_DRDY_Status(Instance, ENV_TEMPERATURE, &status) == BSP_ERROR_NONE && status == 1U)
    {
      drdy = 1;
    }
    else
    {
      drdy = 0;
    }
  }

  if (drdy == 1)
  {
    NewData++;
    NewDataFlags |= 32U;

    (void)BSP_ENV_SENSOR_GetValue(Instance, ENV_TEMPERATURE, &temperature);

    if (DataLoggerActive != 0U)
    {
      (void)memcpy(&Msg->Data[11], (void *)&temperature, sizeof(float));
    }
    else if (AutoInit != 0U)
    {
      displayFloatToInt_t out_value;
      Float_To_Int(temperature, &out_value, 2);
      (void)snprintf(DataOut, MAX_BUF_SIZE, "TEMP: %c%d.%02d\r\n", ((out_value.sign != 0) ? '-' : '+'),
                     (int)out_value.out_int, (int)out_value.out_dec);
      (void)VCOM_write(DataOut, (uint16_t)strlen(DataOut));
    }
    else
    {
      /* Nothing to do */
    }
  }
}

/**
 * @brief  Handles the sensors interrupts
 * @param  Msg the INTERRUPT part of the stream
 * @retval None
 */
static void Sensors_Interrupt_Handler(TMsg *Msg)
{
  static uint8_t mem_int_status = 0;

  /* Interrupt pins SensorTile.Box */
  // Bit 0: LSM6DSOX INT1 (PA2)
  // Bit 1: LSM6DSOX INT2 (PE3)
  // Bit 2: LPS22HH INT (PD15)
  // Bit 3: LIS2DW12 INT1 (PC5) / LIS3DHH INT1 (PC13)
  // Bit 4: LIS2DW12 INT2 (PD14) / LIS3DHH INT2 (PE6)
  // Bit 5: LIS2MDL INT (PD12)
  // Bit 6: HTS221 (PD13)
  // Bit 7: STTS751 (PG5)
  
  if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_2) == GPIO_PIN_SET) IntStatus |= (1 << 0); else IntStatus &= ~(1 << 0);
  if (HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_3) == GPIO_PIN_SET) IntStatus |= (1 << 1); else IntStatus &= ~(1 << 1);
  if (HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_15) == GPIO_PIN_SET) IntStatus |= (1 << 2); else IntStatus &= ~(1 << 2);
  
  if (AccInstance == LIS2DW12_0)
  {
    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_5) == GPIO_PIN_SET) IntStatus |= (1 << 3); else IntStatus &= ~(1 << 3);
    if (HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_14) == GPIO_PIN_SET) IntStatus |= (1 << 4); else IntStatus &= ~(1 << 4);
  }
  else if (AccInstance == LIS3DHH_0)
  {
    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13) == GPIO_PIN_SET) IntStatus |= (1 << 3); else IntStatus &= ~(1 << 3);
    if (HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_6) == GPIO_PIN_SET) IntStatus |= (1 << 4); else IntStatus &= ~(1 << 4);
  }
  else
  {
    IntStatus &= ~(1 << 3);
    IntStatus &= ~(1 << 4);
  }
  
  if (HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_12) == GPIO_PIN_SET) IntStatus |= (1 << 5); else IntStatus &= ~(1 << 5);
  if (HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_13) == GPIO_PIN_SET) IntStatus |= (1 << 6); else IntStatus &= ~(1 << 6);
  if (HAL_GPIO_ReadPin(GPIOG, GPIO_PIN_5) == GPIO_PIN_SET) IntStatus |= (1 << 7); else IntStatus &= ~(1 << 7);

  if (mem_int_status != IntStatus)
  {
    NewData++;
    NewDataFlags |= 64U;
    Msg->Data[56] = IntStatus;
    mem_int_status = IntStatus;
  }
}

/**
 * @brief  Handles the MLC status data
 * @param  Msg the MLC part of the stream
 * @retval None
 */
static void MLC_Handler(TMsg *Msg)
{
  if ((AccInstance == LSM6DSOX_0) && (GyrInstance == LSM6DSOX_0))
  {
    (void)BSP_MOTION_SENSOR_Write_Register(LSM6DSOX_0, LSM6DSOX_FUNC_CFG_ACCESS, LSM6DSOX_EMBEDDED_FUNC_BANK << 6);

    (void)BSP_MOTION_SENSOR_Read_Register(LSM6DSOX_0, LSM6DSOX_MLC0_SRC, &Msg->Data[57]);
    (void)BSP_MOTION_SENSOR_Read_Register(LSM6DSOX_0, LSM6DSOX_MLC1_SRC, &Msg->Data[58]);
    (void)BSP_MOTION_SENSOR_Read_Register(LSM6DSOX_0, LSM6DSOX_MLC2_SRC, &Msg->Data[59]);
    (void)BSP_MOTION_SENSOR_Read_Register(LSM6DSOX_0, LSM6DSOX_MLC3_SRC, &Msg->Data[60]);
    (void)BSP_MOTION_SENSOR_Read_Register(LSM6DSOX_0, LSM6DSOX_MLC4_SRC, &Msg->Data[61]);
    (void)BSP_MOTION_SENSOR_Read_Register(LSM6DSOX_0, LSM6DSOX_MLC5_SRC, &Msg->Data[62]);
    (void)BSP_MOTION_SENSOR_Read_Register(LSM6DSOX_0, LSM6DSOX_MLC6_SRC, &Msg->Data[63]);
    (void)BSP_MOTION_SENSOR_Read_Register(LSM6DSOX_0, LSM6DSOX_MLC7_SRC, &Msg->Data[64]);

    (void)BSP_MOTION_SENSOR_Write_Register(LSM6DSOX_0, LSM6DSOX_FUNC_CFG_ACCESS, LSM6DSOX_USER_BANK << 6);
  }
}

/**
  * @brief  Configures the RTC
  * @param  None
  * @retval None
  */
static void RTC_Config(void)
{
  RtcHandle.Instance = RTC;

  /* Configure RTC prescaler and RTC data registers */
  /* RTC configured as follow:
    - Hour Format    = Format 12
    - Asynch Prediv  = Value according to source clock
    - Synch Prediv   = Value according to source clock
    - OutPut         = Output Disable
    - OutPutPolarity = High Polarity
    - OutPutType     = Open Drain
   */
  RtcHandle.Init.HourFormat     = RTC_HOURFORMAT_12;
  RtcHandle.Init.AsynchPrediv   = RTC_ASYNCH_PREDIV_LSE;
  RtcHandle.Init.SynchPrediv    = RTC_SYNCH_PREDIV_LSE;
  RtcHandle.Init.OutPut         = RTC_OUTPUT_DISABLE;
  RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  RtcHandle.Init.OutPutType     = RTC_OUTPUT_TYPE_OPENDRAIN;

  RtcSynchPrediv = RTC_SYNCH_PREDIV_LSE;

  if (HAL_RTC_Init(&RtcHandle) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
}

/**
  * @brief  Configures the current time and date
  * @param  None
  * @retval None
  */
static void RTC_TimeStampConfig(void)
{
  RTC_DateTypeDef sdatestructure;
  RTC_TimeTypeDef stimestructure;

  /* Configure the Date using BCD format */
  /* Set Date: Monday January 1st 2000 */
  sdatestructure.Year    = 0x00;
  sdatestructure.Month   = RTC_MONTH_JANUARY;
  sdatestructure.Date    = 0x01;
  sdatestructure.WeekDay = RTC_WEEKDAY_MONDAY;

  if (HAL_RTC_SetDate(&RtcHandle, &sdatestructure, FORMAT_BCD) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /* Configure the Time using BCD format */
  /* Set Time: 00:00:00 */
  stimestructure.Hours          = 0x00;
  stimestructure.Minutes        = 0x00;
  stimestructure.Seconds        = 0x00;
  stimestructure.TimeFormat     = RTC_HOURFORMAT12_AM;
  stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE ;
  stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;

  if (HAL_RTC_SetTime(&RtcHandle, &stimestructure, FORMAT_BCD) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
}

/**
  * @brief  Configures the current date
  * @param  y the year value to be set
  * @param  m the month value to be set
  * @param  d the day value to be set
  * @param  dw the day-week value to be set
  * @retval None
  */
void RTC_DateRegulate(uint8_t y, uint8_t m, uint8_t d, uint8_t dw)
{
  RTC_DateTypeDef sdatestructure;

  sdatestructure.Year    = y;
  sdatestructure.Month   = m;
  sdatestructure.Date    = d;
  sdatestructure.WeekDay = dw;

  if (HAL_RTC_SetDate(&RtcHandle, &sdatestructure,FORMAT_BIN) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
}

/**
  * @brief  Configures the current time and date
  * @param  hh the hour value to be set
  * @param  mm the minute value to be set
  * @param  ss the second value to be set
  * @retval None
  */
void RTC_TimeRegulate(uint8_t hh, uint8_t mm, uint8_t ss)
{

  RTC_TimeTypeDef stimestructure;

  stimestructure.TimeFormat     = RTC_HOURFORMAT12_AM;
  stimestructure.Hours          = hh;
  stimestructure.Minutes        = mm;
  stimestructure.Seconds        = ss;
  stimestructure.SubSeconds     = 0;
  stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;

  if (HAL_RTC_SetTime(&RtcHandle, &stimestructure, FORMAT_BIN) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
}

/**
  * @brief  TIM period elapsed callback
  * @param  htim: TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    CDC_TIM_PeriodElapsedCallback(htim);
}

/**
  * @brief  This function is executed in case of error occurrence
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  while (1)
  {}
}

/**
  * @brief  Splits a float into two integer values.
  * @param  in the float value as input
  * @param  out_value the pointer to the output integer structure
  * @param  dec_prec the decimal precision to be used
  * @retval None
  */
static void Float_To_Int(float in, displayFloatToInt_t *out_value, int32_t dec_prec)
{
  if(in >= 0.0f)
  {
    out_value->sign = 0;
  }
  else
  {
    out_value->sign = 1;
    in = -in;
  }

  out_value->out_int = (int32_t)in;
  in = in - (float)(out_value->out_int);
  out_value->out_dec = (int32_t)trunc(in * pow(10, dec_prec));
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\n", file, line) */

  /* Infinite loop */
  while (1)
  {}
}

#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
