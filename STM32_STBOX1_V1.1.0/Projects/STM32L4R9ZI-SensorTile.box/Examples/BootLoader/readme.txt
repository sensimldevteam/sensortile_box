/**
  ******************** (C) COPYRIGHT STMicroelectronics ***********************
  * @file    readme.txt
  * @author  Central LAB
  * @version V1.1.0
  * @date    10-Dec-2019
  * @brief   Description of the BootLoader application firmware
  ******************************************************************************
  * Attention
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

Application Description 

 This firmware package includes Components Device Drivers, Board Support Package
  and example application for the following STMicroelectronics elements:
  - STEVAL-MKSBOX1V1 (SensorTile.box) evaluation board that contains the following components:
      - MEMS sensor devices: HTS221, LPS22HH, LIS2MDL, LSM6DSOX
      - analog microphone 
 

 
 The Example application is a BootLoader that allow Full/Partial Firmware Update of one program.
 Inside this package, this program must be used like Boot Loader for the BLEFOTA application.
 If there is Firmware Update, it makes the Update or, if there is not the Firwamre Update,
 the program works like a trompoline for executing the real program.
 A pre-compiled version of this program is placed on Utilities\BootLoader\STM32L4R9ZI\BootLoaderL4R9.bin

                              --------------------
                              |  VERY IMPORTANT  |
                              -------------------- 
 This example must run starting at the beginning of the FLASH 0x08000000 in memory and works
 in collaboration with another program saved on the LASH at address 0x08004000

@par STM32Cube packages:
  - STM32L4xx drivers from STM32CubeL4 V1.14.0
@par STEVAL-MKSBOX1V1:
  - STEVAL-MKSBOX1V1 V1.3.1

@par How to use it ?

This package contains projects for 3 IDEs viz. IAR, �Vision and System Workbench.
In order to make the  program work, you must do the following:
 - WARNING: before opening the project with any toolchain be sure your folder
   installation path is not too in-depth since the toolchain may report errors
   after building.

For IAR:
 - Open IAR toolchain (this firmware has been successfully tested with Embedded Workbench V8.32.3).
 - Open the IAR project file EWARM\BootLoader.eww
 - Rebuild all files and load your image into target memory.

For �Vision:
 - Open �Vision toolchain (this firmware has been successfully tested with MDK-ARM Professional Version: 5.27.1)
 - Open the �Vision project file MDK-ARM\Project.uvprojx
 - Rebuild all files and load your image into target memory.
		
For System STM32CubeIDE:
- Open STM32CubeIDE (this firmware has been successfully tested with Version 1.1.0)
 - Set the default workspace proposed by the IDE (please be sure that there are not spaces in the workspace path).
 - Press "File" -> "Import" -> "Existing Projects into Workspace"; press "Browse" in the "Select root directory" and choose the path where the System Workbench project is located (it should be STM32CubeIDE\). 
 - Rebuild all files and load your image into target memory.
		
 /******************* (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
